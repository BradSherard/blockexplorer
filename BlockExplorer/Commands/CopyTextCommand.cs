﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace BlockExplorer.Commands
{
    public class CopyTextCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;// dont bother raisingCanExecuteChanged updates, by the time it can be acted upon the copy command should work
        }

        public void Execute(object parameter)
        {
            string text = parameter as string;

            if (text != null)
            {
                Clipboard.SetText(text);
            }
        }
    }
}
