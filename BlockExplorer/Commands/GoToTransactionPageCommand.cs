﻿using System;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class GoToTransactionPageCommand : Command<GoToTransactionPageCommandParameter>
    {
        public override bool CanExecute(GoToTransactionPageCommandParameter parameters)
        {
            bool canExecute = parameters?.Data != null;
            return canExecute;
        }

        public override void Execute(GoToTransactionPageCommandParameter parameters)
        {
            App.Navigate(new TransactionPage(), new TransactionPage.NavigationParameters() { TransactionId = parameters.Data.TransactionId});
        }
    }
}
