﻿using System;
using System.Threading;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;
using BlockExplorer.Services;
using System.IO;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

namespace BlockExplorer.Commands
{
    public class SaveQRCodeCommand : Command<SaveQRCodeCommandParameter>
    {
        public override bool CanExecute(SaveQRCodeCommandParameter parameters)
        {
            bool canExecute = parameters.Data != null;
            return canExecute;
        }

        public override void Execute(SaveQRCodeCommandParameter parameters)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "address";
            dialog.DefaultExt = ".png";
            dialog.Filter = "PNG files (.png)|*.png";

            bool? result = dialog.ShowDialog();

            if (result == true)
            {
                string filePath = dialog.FileName;

                ThreadPool.QueueUserWorkItem
                (
                    delegate
                    {
                        using (FileStream stream = new FileStream(filePath, FileMode.Create))
                        {
                            PngBitmapEncoder encoder = new PngBitmapEncoder();
                            encoder.Interlace = PngInterlaceOption.On;
                            encoder.Frames.Add(BitmapFrame.Create(parameters.Data));
                            encoder.Save(stream);
                        }
                    }
                );
            }
        }
    }
}
