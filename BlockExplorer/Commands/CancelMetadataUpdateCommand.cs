﻿using System;
using System.Threading;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class CancelMetadataUpdateCommand : Command<CancelMetadataUpdateCommandParameter>
    {
        public override bool CanExecute(CancelMetadataUpdateCommandParameter parameters)
        {
            bool canExecute = parameters.Data != null && parameters.Data.IsUpdatingMetadata;
            return canExecute;
        }

        public override void Execute(CancelMetadataUpdateCommandParameter parameters)
        {
            ThreadPool.QueueUserWorkItem
            (
                delegate
                {
                    parameters.Data.CancelMetadataUpdate();
                }
            );
        }
    }
}
