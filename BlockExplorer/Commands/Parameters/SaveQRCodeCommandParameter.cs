﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using System.Windows.Media.Imaging;

namespace BlockExplorer.Commands.Parameters
{
    public class SaveQRCodeCommandParameter : CommandParameter
    {
        #region types

        private struct Properties
        {
            public BitmapSource data;
            public object viewModel;
        }

        #endregion

        #region constants

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public BitmapSource Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value);
            }
        }

        public object ViewModel
        {
            get
            {
                return this.properties.viewModel;
            }
            set
            {
                SetProperty(ref this.properties.viewModel, ref value);
            }
        }

        #endregion
    }
}
