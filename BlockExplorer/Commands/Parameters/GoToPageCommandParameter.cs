﻿using BlockExplorer.Commands.Core;

namespace BlockExplorer.Commands.Parameters
{
    public class GoToPageCommandParameter<T> : CommandParameter
    {
        #region types

        private struct Properties
        {
            public T data;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public T Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value);
            }
        }

        #endregion
    }
}