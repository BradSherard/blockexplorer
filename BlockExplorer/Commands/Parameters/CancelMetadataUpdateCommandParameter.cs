﻿using BlockExplorer.Commands.Core;
using BlockExplorer.Data.Core;

namespace BlockExplorer.Commands.Parameters
{
    public class CancelMetadataUpdateCommandParameter : CommandParameter
    {
        #region constants

        private static readonly string[] monitoredPropertyNames = new string[] { nameof(MetadataChangeObject.IsUpdatingMetadata) };

        #endregion

        #region types

        private struct Properties
        {
            public MetadataChangeObject data;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public MetadataChangeObject Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value, monitoredPropertyNamesOfProperty: CancelMetadataUpdateCommandParameter.monitoredPropertyNames);
            }
        }

        #endregion
    }
}