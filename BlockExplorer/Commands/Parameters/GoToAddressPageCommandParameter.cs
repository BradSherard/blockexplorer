﻿using BlockExplorer.Commands.Core;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using System.Collections.Generic;
using System.Windows;


namespace BlockExplorer.Commands.Parameters
{
    public class GoToAddressPageCommandParameter : GoToPageCommandParameter<ExplorerAddress>
    {
    }
}