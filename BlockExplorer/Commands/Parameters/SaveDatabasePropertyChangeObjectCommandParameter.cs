﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;

namespace BlockExplorer.Commands.Parameters
{
    public class SaveDatabasePropertyChangeObjectCommandParameter : CommandParameter
    {
        #region types

        private struct Properties
        {
            public DatabasePropertyChangeObject data;
            // brads todo: find way to make this only contracted to view models that have as its data an address object
            public object viewModel;
        }

        #endregion

        #region constants

        private static readonly string[] monitoredPropertyNames = new string[] { nameof(DatabasePropertyChangeObject.IsMarkedForStorage) };

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public DatabasePropertyChangeObject Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value, monitoredPropertyNamesOfProperty: SaveDatabasePropertyChangeObjectCommandParameter.monitoredPropertyNames);
            }
        }

        public object ViewModel
        {
            get
            {
                return this.properties.viewModel;
            }
            set
            {
                SetProperty(ref this.properties.viewModel, ref value);
            }
        }

        #endregion
    }
}
