﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data;

namespace BlockExplorer.Commands.Parameters
{
    public class AddressDataAndViewModelCommandParameter : CommandParameter
    {
        #region types

        private struct Properties
        {
            public ExplorerAddress address;
            // brads todo: find way to make this only contracted to view models that have as its data an address object
            public object viewModel;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public ExplorerAddress Address
        {
            get
            {
                return this.properties.address;
            }
            set
            {
                SetProperty(ref this.properties.address, ref value);
            }
        }

        public object ViewModel
        {
            get
            {
                return this.properties.viewModel;
            }
            set
            {
                SetProperty(ref this.properties.viewModel, ref value);
            }
        }

        #endregion
    }
}
