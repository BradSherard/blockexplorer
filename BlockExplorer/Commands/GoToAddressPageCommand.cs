﻿using System;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class GoToAddressPageCommand : Command<GoToAddressPageCommandParameter>
    {
        public override bool CanExecute(GoToAddressPageCommandParameter parameters)
        {
            bool canExecute = parameters?.Data != null;
            return canExecute;
        }

        public override void Execute(GoToAddressPageCommandParameter parameters)
        {
            //parameters.Data = Data.ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data("1NxTPBjqS5C4CjUUQSt37NLiuQVhssZQbF"));
            App.Navigate(new AddressPage(), new AddressPage.NavigationParameters() { AddressId = parameters.Data.AddressId });
        }
    }
}
