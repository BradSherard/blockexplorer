﻿using System;
using System.Threading;
using BlockExplorer.Data.Core;
using BlockExplorer.Data;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;
using BlockExplorer.Services;

namespace BlockExplorer.Commands
{
    public class SaveAddressCommand : Command<SaveDatabasePropertyChangeObjectCommandParameter>
    {
        public override bool CanExecute(SaveDatabasePropertyChangeObjectCommandParameter parameters)
        {
            bool canExecute = parameters?.Data != null && parameters.Data is ExplorerAddress && !parameters.Data.IsMarkedForStorage;
            return canExecute;
        }

        public override void Execute(SaveDatabasePropertyChangeObjectCommandParameter parameters)
        {
            ThreadPool.QueueUserWorkItem
            (
                async delegate
                {
                    MetadataPageViewModelBase.DataErrorTypes error = await CollectionsService.Instance.StoredExplorerAddresses.AddItem(((ExplorerAddress)parameters.Data));

                    AddressPageViewModel addressPageViewModel = parameters.ViewModel as AddressPageViewModel;

                    if (addressPageViewModel != null)
                    {
                        addressPageViewModel.DataError = error;
                    }
                }
            );
        }
    }
}
