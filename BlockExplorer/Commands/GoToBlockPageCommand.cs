﻿using System;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class GoToBlockPageCommand : Command<GoToBlockPageCommandParameter>
    {
        public override bool CanExecute(GoToBlockPageCommandParameter parameters)
        {
            bool canExecute = parameters?.Data != null;
            return canExecute;
        }

        public override void Execute(GoToBlockPageCommandParameter parameters)
        {
            App.Navigate(new BlockPage(), new BlockPage.NavigationParameters() { BlockId = parameters.Data.BlockId });
        }
    }
}
