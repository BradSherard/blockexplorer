﻿using System;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class GoToStartPageCommand : Command<GoToPageCommandParameter<Object>>
    {
        public override bool CanExecute(GoToPageCommandParameter<Object> parameters)
        {
            bool canExecute = parameters?.Data != null;
            return canExecute;
        }

        public override void Execute(GoToPageCommandParameter<Object> parameters)
        {
            App.Navigate(new StartPage(), new StartPage.NavigationParameters() { });
        }
    }
}
