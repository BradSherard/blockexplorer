﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using System.ComponentModel;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands.Core;

namespace BlockExplorer.Commands
{
    /// <summary>
    /// Note: WPF bug with hooking up command binding and events with the control requires setting command parameter before command in the xaml binding.
    /// Otherwise CanExecuteChanged is never called once everything is connected and so any CanExecute call raised from the event passes null parameters. 
    /// Some data that instantiates later may hide this bug by making a raising a new check after changing the data of the command parameter.
    /// </summary>
    public class CommandParameter : PropertyChangeObject
    {
        #region types

        private struct Properties
        {
            public ICommand command;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        // brads todo: to detect changes to a notifyPropertyChange property's properties, we register to the event.
        // To prevent calling RaiseCanExecute for any change, we filter by PropertyNames.
        // A flaw with this is that we may be registered to multiple notifyPropertyChange objects and we don't know
        // which is raising the event. So our whitelist filter will allow properties of a name for any of our notifyPropertyChange objects.
        // This won't cause any bug. It will just permit uneccesary calls to RaiseCanExecute
        private Dictionary<string, bool> MonitoredPropertyNamesOfProperty = new Dictionary<string, bool>();

        #endregion

        #region properties

        public ICommand Command
        {
            get
            {
                return this.properties.command;
            }
            set
            {
                base.SetProperty(ref this.properties.command, ref value);
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Only updates canExecute when non-command properties are updated. This means pattern is to set the command and command parameter,
        /// then set the data. Alternatively, one could simply call RaiseCanExecuteChanged manually when ready.
        /// If it isn't available at the time, make sure to reset the parameter data again onDataChanged in the parent class.
        /// </summary>
        protected bool SetProperty<PropertyType>(ref PropertyType propertyValue, ref PropertyType newValue, ChangedAction<PropertyType> changedAction = null, [CallerMemberName] string propertyName = null, IEnumerable<string> monitoredPropertyNamesOfProperty = null)
        {
            bool isValueChanged = base.SetProperty(ref propertyValue, ref newValue, changedAction, propertyName);
            IRaiseCanExecuteChanged command = this.Command as IRaiseCanExecuteChanged;

            command?.RaiseCanExecuteChanged();
            this.MonitorCollectionChanges(propertyValue);

            if (monitoredPropertyNamesOfProperty != null)
            {
                this.MonitorPropertyChanges(propertyValue, monitoredPropertyNamesOfProperty);
            }

            return isValueChanged;
        }

        #endregion

        #region non-public methods

        private void Collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            IRaiseCanExecuteChanged command = this.Command as IRaiseCanExecuteChanged;

            command?.RaiseCanExecuteChanged();
        }

        private void Parameter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.MonitoredPropertyNamesOfProperty.ContainsKey(e.PropertyName))
            {
                IRaiseCanExecuteChanged command = this.Command as IRaiseCanExecuteChanged;

                command?.RaiseCanExecuteChanged();
            }
        }

        private void MonitorPropertyChanges<PropertyType>(PropertyType propertyValue, IEnumerable<string> monitoredProperties)
        {
            PropertyChangeObject propertyChangeObject = propertyValue as PropertyChangeObject;

            if (propertyChangeObject != null)
            {
                foreach (string propertyName in monitoredProperties)
                {
                    this.MonitoredPropertyNamesOfProperty[propertyName] = true;
                }

                propertyChangeObject.PropertyChanged -= Parameter_PropertyChanged;
                propertyChangeObject.PropertyChanged += Parameter_PropertyChanged;
            }
        }

        private void MonitorCollectionChanges<PropertyType>(PropertyType propertyValue)
        {
            INotifyCollectionChanged collection = propertyValue as INotifyCollectionChanged;

            if (collection != null)
            {
                collection.CollectionChanged -= Collection_CollectionChanged;
                collection.CollectionChanged += Collection_CollectionChanged;
            }
        }

        #endregion
    }
}
