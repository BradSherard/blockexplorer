﻿using System.Windows.Input;

namespace BlockExplorer.Commands.Core
{
    public interface IRaiseCanExecuteChanged : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}