﻿using Prism.Commands;
using System;
using System.Linq.Expressions;

namespace BlockExplorer.Commands.Core
{
    /// <summary>
    /// Note: WPF bug with hooking up command binding and events with the control requires setting command parameter before command in the xaml binding.
    /// Otherwise CanExecuteChanged is never called once everything is connected and so any CanExecute call raised from the event passes null parameters. 
    /// Some data that instantiates later may hide this bug by making a raising a new check after changing the data of the command parameter.
    /// Additionally, some other bug causes the CanExecute check to not be made even if the xaml binding order is accounted for. In that case the solution
    /// is to use a custom behavior to force it to revaluate.
    /// </summary>
    /// <example>
    /// <Button
    ///     CommandParameter="{Binding GoToAddressPageCommandParameter}" Command="{Binding GoToAddressPageCommand}"
    ///     behaviors:CommandParameterBehavior.IsCommandRequeriedOnChange="True"
    /// />
    /// </example>
    public abstract class Command<ParametersType> : IRaiseCanExecuteChanged  where ParametersType : class // CommandParameter
    {
        #region data

        private DelegateCommand<ParametersType> delegateCommand;
        private bool previousCanExecuteValue = false;
        private bool initialNullTriggered = false;

        #endregion

        #region properties

        public event EventHandler CanExecuteChanged
        {
            add
            {
                this.delegateCommand.CanExecuteChanged += value;
                //this.delegateCommand.RaiseCanExecuteChanged();
            }
            remove
            {
                this.delegateCommand.CanExecuteChanged -= value;
            }
        }

        #endregion

        #region constructor

        public Command()
        {
            this.delegateCommand = new DelegateCommand<ParametersType>(Execute, CanExecute);
        }

        #endregion

        #region abstract methods

        public abstract bool CanExecute(ParametersType parameters);
        public abstract void Execute(ParametersType parameters);

        #endregion

        #region public methods

        public Command<ParametersType> ObservesProperty<T>(Expression<Func<T>> propertyExpression)
        {
            this.delegateCommand.ObservesProperty(propertyExpression);

            // this allows chaining
            return this;
        }

        public void RaiseCanExecuteChanged()
        {
            this.delegateCommand.RaiseCanExecuteChanged();
        }

        public bool CanExecute(object parameter)
        {
            //if (parameter == null)
            //{
            //    if (this.initialNullTriggered)
            //    {
            //        ParametersType parameters = ToCommandParameters(parameter);

            //        this.previousCanExecuteValue = this.delegateCommand.CanExecute(parameters);
            //    }
            //    else
            //    {
            //        this.initialNullTriggered = true;
            //    }
            //}
            //else
            //{
            //    ParametersType parameters = ToCommandParameters(parameter);

            //    this.previousCanExecuteValue = this.delegateCommand.CanExecute(parameters);
            //}

            //return this.previousCanExecuteValue;

            ParametersType parameters = ToCommandParameters(parameter);

            return this.delegateCommand.CanExecute(parameters);
        }

        public void Execute(object parameter)
        {
            ParametersType parameters = ToCommandParameters(parameter);

            this.delegateCommand.Execute(parameters);
        }

        #endregion

        #region non-public methods

        protected virtual ParametersType ToCommandParameters(object parameter)
        {
            return parameter as ParametersType;
        }

        #endregion
    }
}
