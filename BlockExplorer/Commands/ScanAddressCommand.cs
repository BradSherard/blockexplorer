﻿using System;
using System.Threading;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class ScanAddressCommand : Command<AddressDataAndViewModelCommandParameter>
    {
        public override bool CanExecute(AddressDataAndViewModelCommandParameter parameters)
        {
            bool canExecute = parameters?.Address != null && parameters.Address.MetadataLevel != MetadataChangeObject.MetadataLevels.Default && !parameters.Address.IsUpdatingMetadata;
            return canExecute;
        }

        public override void Execute(AddressDataAndViewModelCommandParameter parameters)
        {
            ThreadPool.QueueUserWorkItem
            (
                async delegate
                {
                    if (parameters.Address.MetadataLevel == MetadataChangeObject.MetadataLevels.Full)
                    {
                        parameters.Address.MetadataLevel = MetadataChangeObject.MetadataLevels.Lite;
                    }

                    MetadataPageViewModelBase.DataErrorTypes error = await parameters.Address.EnsureFullMetadata();

                    AddressPageViewModel addressPageViewModel = parameters.ViewModel as AddressPageViewModel;

                    if (addressPageViewModel != null)
                    {
                        addressPageViewModel.DataError = error;
                    }
                }
            );
        }
    }
}
