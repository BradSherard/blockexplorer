﻿using System;
using BlockExplorer.Commands.Core;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.Pages;
using System.Windows.Controls;
using NBitcoin;

namespace BlockExplorer.Commands
{
    public class GoToLatestTransactionsPageCommand : Command<string>
    {
        public override bool CanExecute(string parameters)
        {
            bool isAlreadyOnPage = App.ContentFrame.Content is LatestTransactionsPage;
            return !isAlreadyOnPage;
        }

        public override void Execute(string parameters)
        {
            App.Navigate(new LatestTransactionsPage(), new LatestTransactionsPage.NavigationParameters() { });
        }
    }
}
