﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using NBitcoin;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Media;
using ZXing.QrCode;
using ZXing;
using ZXing.Common;

namespace BlockExplorer.ValueConverters
{
    public class QRCodeImageSourceFormatter : IValueConverter
    {
        [DllImport("gdi32")]
        private static extern bool DeleteObject(IntPtr hObject);

        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value is string)
                {
                    return CreateImageSourceFromString((string)value);
                }
                else
                {
                    throw new ArgumentException("value must be of type string");
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region non-public methods

        private BitmapSource CreateImageSourceFromString(string data)
        {
            // assume data is base58 for now
            BitmapSource bitmapSource = null;

            // 41 x 41 matrix(version 6), 196 x 196 pix
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix matrix = qrCodeWriter.encode(data, BarcodeFormat.QR_CODE, 45, 45);

            BarcodeWriter barcodeWriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Height = 384,
                    Width = 384,
                    Margin = 0,
                }
            };

            using (Bitmap bitmap = barcodeWriter.Write(matrix))
            {
                IntPtr hbmp = bitmap.GetHbitmap();

                try
                {
                    bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(hbmp, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                }
                finally
                {
                    DeleteObject(hbmp);
                }
            }

            return bitmapSource;
        }

        #endregion
    }
}