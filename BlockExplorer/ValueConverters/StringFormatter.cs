﻿using System;
using System.Globalization;
using System.Windows.Data;
using NBitcoin;

namespace BlockExplorer.ValueConverters
{
    public class StringFormatter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = parameter as string;

            if (type != null)
            {
                switch (type)
                {
                    case nameof(String):
                    {
                        return (string)value;
                    }
                    case nameof(Enum):
                    {
                        return ((Enum)value).ToString();
                    }
                    case nameof(Int32):
                    {
                        return ((int)value).ToString();
                    }
                    case "Nullable" + nameof(Int32):
                    {
                        return ((int?)value).ToString();
                    }
                    case nameof(Int64):
                    {
                        return ((long)value).ToString();
                    }
                    case "Nullable" + nameof(Int64):
                    {
                        return ((long?)value).ToString();
                    }
                    case nameof(Double):
                    {
                        return ((double)value).ToString();
                    }
                    case "Nullable" + nameof(Double):
                    {
                        return ((double?)value).ToString();
                    }
                    case nameof(uint256):
                    {
                        return ((uint256)value)?.ToString() ?? "";
                    }
                    case nameof(Base58Data):
                    {
                        return ((Base58Data)value).ToWif();
                    }
                    case nameof(Boolean):
                    {
                        return ((bool)value).ToString();
                    }
                    case "Nullable" + nameof(Boolean):
                    {
                        return ((bool?)value).ToString();
                    }
                    case nameof(DateTime):
                    {
                        return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    case "Nullable" + nameof(DateTime):
                    {
                        return ((DateTime?)value).HasValue ? ((DateTime?)value).Value.ToString("yyyy-MM-dd HH:mm:ss") : "";
                    }
                    default:
                    {
                        throw new NotImplementedException();
                    }     
                }      
            }
            {
                throw new ArgumentException("Must have a Type object as the parameter");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}