﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using NBitcoin;

namespace BlockExplorer
{
    public class UInt256JSONConverter : JsonConverter
    {
        #region constants
        #endregion

        #region data
        #endregion

        #region constructor

        public UInt256JSONConverter() : base()
        {
        }

        #endregion

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(uint256));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string stringValue = JToken.Load(reader).ToString();
            return uint256.Parse(stringValue);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            string stringValue = ((uint256)value).ToString();
            JToken.FromObject(stringValue).WriteTo(writer);
        }
    }
}
