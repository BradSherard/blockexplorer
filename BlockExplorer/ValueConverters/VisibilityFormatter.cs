﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using NBitcoin;

namespace BlockExplorer.ValueConverters
{
    public class VisibilityFormatter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string type = parameter as string;

            if (type != null)
            {
                switch (type)
                {
                    case nameof(String):
                    {
                        return !String.IsNullOrEmpty((string)value) ? Visibility.Visible : Visibility.Collapsed;
                    }
                    //case nameof(Enum):
                    //{
                    //    return ((Enum)value).ToString();
                    //}
                    case nameof(Int32):
                    {
                        return ((int)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case "Nullable" + nameof(Int32):
                    {
                        return ((int?)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case nameof(Int64):
                    {
                        return ((long)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case "Nullable" + nameof(Int64):
                    {
                        return ((long?)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case nameof(Double):
                    {
                        return ((double)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case "Nullable" + nameof(Double):
                    {
                        return ((double?)value) > 0 ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case nameof(uint256):
                    {
                        return ((uint256)value) != null ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case nameof(Base58Data):
                    {
                        return ((Base58Data)value) != null ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case nameof(Boolean):
                    {
                        return ((bool)value) ? Visibility.Visible : Visibility.Collapsed;
                    }
                    case "Nullable" + nameof(Boolean):
                    {
                        return ((bool?)value).HasValue && ((bool?)value).Value ? Visibility.Visible : Visibility.Collapsed;
                    }
                    //case nameof(DateTime):
                    //{
                    //    return ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
                    //}
                    //case "Nullable" + nameof(DateTime):
                    //{
                    //    return ((DateTime?)value).HasValue ? ((DateTime?)value).Value.ToString("yyyy-MM-dd HH:mm:ss") : "";
                    //}
                    default:
                    {
                        throw new NotImplementedException();
                    }
                }
            }
            {
                throw new ArgumentException("Must have a Type object as the parameter");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}