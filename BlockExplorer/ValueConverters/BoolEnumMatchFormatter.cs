﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using NBitcoin;

namespace BlockExplorer.ValueConverters
{
    public class BoolEnumMatchFormatter : IValueConverter
    {
        #region data

        private const char parameterSeparator = '|';

        #endregion


        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool hasMatch = false;

            if (value is Enum)
            {
                string enumValue = ((Enum)value).ToString();

                if (parameter is string)
                {
                    foreach (string match in ((string)parameter).Split(BoolEnumMatchFormatter.parameterSeparator))
                    {
                        if (enumValue.Equals(match))
                        {
                            hasMatch = true;
                            break;
                        }
                    }
                }
                else
                {
                    throw new ArgumentException("parameter must be of type string");
                }
            }
            else
            {
                throw new ArgumentException("value must be of type Enum");
            }

            return hasMatch;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}