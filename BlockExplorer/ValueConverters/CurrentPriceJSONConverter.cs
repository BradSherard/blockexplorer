﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using BlockExplorer.Data.JSON;

namespace BlockExplorer.ValueConverters
{
    /// <summary>
    /// //http(s)://api.coindesk.com/v1/bpi/currentprice/<CODE>.json
    /// </summary>
    public class CurrentPriceJSONConverter : JsonConverter
    {
        #region constants

        private const string usdCurrencyCode = "USD";

        #endregion

        #region data

        private string otherCurrencyCode;

        #endregion

        #region constructor

        public CurrentPriceJSONConverter(string otherCurrencyCode) : base()
        {
            this.otherCurrencyCode = otherCurrencyCode;
        }

        #endregion

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(JsonCurrentPrice.BitcoinPriceIndex));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JsonCurrentPrice.BitcoinPriceIndex bitcoinPriceIndex = new JsonCurrentPrice.BitcoinPriceIndex();
            JObject jObject = JObject.Load(reader);

            bitcoinPriceIndex.USDCurrency = ((JObject)jObject[CurrentPriceJSONConverter.usdCurrencyCode]).ToObject<JsonCurrentPrice.Currency>();
            bitcoinPriceIndex.OtherCurrency = ((JObject)jObject[this.otherCurrencyCode]).ToObject<JsonCurrentPrice.Currency>();

            return bitcoinPriceIndex;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            //Document doc = (Document)value;

            //// Create a JObject from the document, respecting existing JSON attribs
            //JObject jo = JObject.FromObject(value);

            //// At this point the DocTypeIdentifier is not serialized correctly.
            //// Fix it by replacing the property with the correct name and value.
            //JProperty prop = jo.Children<JProperty>()
            //                   .Where(p => p.Name == "DocTypeIdentifier")
            //                   .First();

            //prop.AddAfterSelf(new JProperty(doc.DocTypeIdentifier.ParameterName,
            //                                doc.DocTypeIdentifier.Value));
            //prop.Remove();

            //// Write out the JSON
            //jo.WriteTo(writer);

            throw new NotImplementedException();
        }
    }
}
