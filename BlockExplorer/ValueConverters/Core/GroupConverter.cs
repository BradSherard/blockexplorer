﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Data;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Windows.Markup;

namespace BlockExplorer.ValueConverters.Core
{
    /// <summary>
    /// Chains value converters (also has single initial multiconverter capability that can be improved to be generalized later)
    /// </summary>
    /// <example>
    ///     <Grid x:Name="DataGrid" Grid.Row="1" Grid.Column="0" Margin="12">
    ///        <!--Visibility="{Binding ViewModel.HasDataError, Mode=OneWay, Converter={StaticResource VisibilityFormatter}, ConverterParameter={StaticResource Type_Bool}}"-->
    ///        <Grid.Visibility>
    ///            <Binding Path = "ViewModel.HasDataError" Mode="OneWay">
    ///                <Binding.Converter>
    ///                    <valueConvertersCore:GroupConverter>
    ///                        <StaticResource ResourceKey = "VisibilityFormatter" />
    ///                    </ valueConvertersCore:GroupConverter>
    ///                </Binding.Converter>
    ///                <Binding.ConverterParameter>
    ///                    <x:Array Type = "system:String" >
    ///                       < StaticResource ResourceKey="Type_Bool" />
    ///                     <!--<system:String>A selected</system:String>-->
    ///                 </x:Array>
    ///                </Binding.ConverterParameter>
    ///            </Binding>
    ///        </Grid.Visibility>
    ///        ...
    ///    </Grid>
    /// </example>
    [ContentProperty("Converters")]
    public class GroupConverter : IValueConverter, IMultiValueConverter
    {
        #region data

        private IMultiValueConverter multiValueConverter;
        private List<IValueConverter> converters = new List<IValueConverter>();

        #endregion

        #region properties

        public IMultiValueConverter MultiValueConverter
        {
            get { return this.multiValueConverter; }
            set { this.multiValueConverter = value; }
        }

        public List<IValueConverter> Converters
        {
            get { return this.converters; }
            set { this.converters = value; }
        }

        #endregion


        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string[] parameterArray = parameter as string[];

            if (parameterArray == null)
            {
                parameterArray = new string[this.Converters.Count];
            }

            return GroupConvert(value, this.Converters, parameterArray);
        }  

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GroupConvertBack(value, this.Converters.ToArray().Reverse());
        }

        
        #endregion

        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (this.MultiValueConverter == null)
            {
                throw new InvalidOperationException("To use the converter as a MultiValueConverter the MultiValueConverter property needs to be set.");
            }

            string[] parameterArray = parameter as string[];

            if (parameterArray == null)
            {
                parameterArray = new string[this.Converters.Count + 1];
            }

            var firstConvertedValue = this.MultiValueConverter.Convert(values, targetType, parameterArray[0], culture);

            return GroupConvert(firstConvertedValue, this.Converters, parameterArray.Skip(1).ToArray());
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (this.MultiValueConverter == null)
            {
                throw new InvalidOperationException("To use the converter as a MultiValueConverter the MultiValueConverter property needs to be set.");
            }

            var tailConverted = GroupConvertBack(value, Converters.ToArray().Reverse());
            return this.MultiValueConverter.ConvertBack(tailConverted, targetTypes, parameter, culture);
        }

        #endregion

        #region non-public methods

        private static object GroupConvert(object value, IEnumerable<IValueConverter> converters, string[] parameters)
        {
            int index = 0;

            return converters.Aggregate(value, 
                (aggregateValue, converter) => 
                {
                    return converter.Convert(aggregateValue, typeof(object), parameters[index++], null);
                }
            );
        }

        private static object GroupConvertBack(object value, IEnumerable<IValueConverter> converters)
        {
            return converters.Aggregate(value, (acc, conv) => { return conv.ConvertBack(acc, typeof(object), null, null); });
        }

        #endregion
    }
}
