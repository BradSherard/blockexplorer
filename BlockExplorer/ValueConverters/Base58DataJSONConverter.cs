﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using NBitcoin;
using BlockExplorer.Services;

namespace BlockExplorer
{
    public class Base58DataJSONConverter : JsonConverter
    {
        #region constants
        #endregion

        #region data
        #endregion

        #region constructor

        public Base58DataJSONConverter() : base()
        {
        }

        #endregion

        public override bool CanConvert(Type objectType)
        {
            return (objectType == typeof(Base58Data));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string stringValue = JToken.Load(reader).ToString();
            return Base58Data.GetFromBase58Data(stringValue, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            string stringValue = ((Base58Data)value).ToWif();
            JToken.FromObject(stringValue).WriteTo(writer);
        }
    }
}
