﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
using NBitcoin;

namespace BlockExplorer.ValueConverters
{
    public class NegateBoolFormatter : IValueConverter
    {
        #region IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return !((bool)value);
            }
            else
            {
                throw new ArgumentException("value must be of type bool");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}