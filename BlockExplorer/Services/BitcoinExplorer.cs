﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBitcoin;
using QBitNinja.Client;

namespace BlockExplorer.Services
{
    public class BitcoinExplorer
    {
        #region data

        private Network network;

        #endregion

        #region properties

        public QBitNinjaClient ExplorerClient { get; private set; }

        #endregion

        #region constructor

        public BitcoinExplorer()
        {

        }

        #endregion

        #region public methods

        public void Initialize(Network network = null)
        {
            this.network = network ?? Network.Main;
            this.ExplorerClient = new QBitNinjaClient(this.network);
        }

        #endregion
    }
}
