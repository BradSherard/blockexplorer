﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;
using System.Collections.Specialized;

namespace BlockExplorer.Services
{
    public class RestCaller
    {
        #region types

        public struct CallerResult<T>
        {
            public T Result { get; set; }
            public bool RequestSuccessful { get; set; }
        }

        #endregion

        #region data

        #endregion

        #region properties

        #endregion

        #region constructors

        public RestCaller()
        {
        }

        #endregion

        #region public methods   

        public void Initialize()
        {
        }

        public async Task<CallerResult<T>> CallGetJsonObjectAsync<T>(Uri uri, CancellationToken cancellationToken = new CancellationToken(), JsonConverter customConverter = null)
        {
            return await GetJsonObjectAsync<T>(uri, cancellationToken, customConverter);
        }

        public async Task<CallerResult<T>> CallGetJsonObjectAsync<T>(string function, IDictionary<string, string> parameters, CancellationToken cancellationToken = new CancellationToken(), JsonConverter customConverter = null)
        {
            UriBuilder uriBuilder = new UriBuilder(function);
            NameValueCollection query = new NameValueCollection();

            foreach (string key in parameters.Keys)
            {
                query[key] = parameters[key];
            }

            uriBuilder.Query = query.ToString();

            return await GetJsonObjectAsync<T>(uriBuilder.Uri, cancellationToken, customConverter);
        }

        #endregion

        #region non-public methods

        private async Task<HttpResponseMessage> GetAsync(Uri uri, CancellationToken cancellationToken = new CancellationToken())
        {
            HttpResponseMessage responseMessage = null;

            if (uri.IsWellFormedOriginalString())
            {
                string url = uri.AbsoluteUri;

                if (!string.IsNullOrEmpty(url))
                {
                    using (HttpClient httpClient = new HttpClient())
                    {
                        Task<HttpResponseMessage> httpClientGetTask = httpClient.GetAsync(url, cancellationToken);

                        responseMessage = await httpClientGetTask;
                    }
                }
            }

            return responseMessage;
        }

        private async Task<CallerResult<T>> GetJsonObjectAsync<T>(Uri uri, CancellationToken cancellationToken = new CancellationToken(), JsonConverter customConverter = null)
        {
            CallerResult<T> callerResult = new CallerResult<T>()
            {
                RequestSuccessful = false,
                Result = default(T),
            };

            HttpResponseMessage responseMessage = await GetAsync(uri, cancellationToken);

            if (!cancellationToken.IsCancellationRequested)
            {
                string responseMessageString = await responseMessage?.Content?.ReadAsStringAsync();

                if (!string.IsNullOrEmpty(responseMessageString))
                {
                    Task<T> jsonDeserializeTask = Task.Run<T>
                    (
                        delegate
                        {
                            if (customConverter == null)
                            {
                                return JsonConvert.DeserializeObject<T>(responseMessageString);
                            }
                            else
                            {
                                JsonSerializerSettings settings = new JsonSerializerSettings();
                                settings.Converters.Add(customConverter);

                                return JsonConvert.DeserializeObject<T>(responseMessageString, settings);
                            }
                        }
                    );

                    callerResult.Result = await jsonDeserializeTask;
                    callerResult.RequestSuccessful = true;
                }
            }

            return callerResult;
        }

        #endregion
    }
}
