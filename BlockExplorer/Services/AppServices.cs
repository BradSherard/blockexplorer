﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Services
{
    public class AppServices
    {
        #region data

        private static object mutex = new object();
        private DatabaseRepository databaseRepository;
        
        #endregion

        #region properties

        public static AppServices Instance { get; private set; }

        public RestCaller RestCaller { get; private set; }

        public User User { get; private set; }

        public BitcoinExplorer BitcoinExplorer { get; private set; }

        #endregion

        #region constructors

        public AppServices()
        {
            this.User = new User();
            this.databaseRepository = new DatabaseRepository();
            this.RestCaller = new RestCaller();
            this.BitcoinExplorer = new BitcoinExplorer();

            AppServices.Instance = this;
        }

        #endregion

        #region public methods

        #endregion

        #region non-public methods

        #endregion
    }
}
