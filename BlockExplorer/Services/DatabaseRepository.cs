﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.SQL;

namespace BlockExplorer.Services
{
    public class DatabaseRepository
    {
        #region static data

        public static Exception CreateDatabaseError = null;
        public static object Mutex = new object();

        #endregion

        #region data

        private WeakReference<ExplorerAddressRepository> explorerAddressRepositoryWeakReference;
        private WeakReference<TransferRepository> transferRepositoryWeakReference;
        private WeakReference<ExplorerTransactionRepository> explorerTransactionRepositoryWeakReference;

        private Database database;

        #endregion

        #region properties

        public ExplorerAddressRepository ExplorerAddressRepository
        {
            get
            {
                ExplorerAddressRepository explorerAddressRepository = null;

                if ((this.explorerAddressRepositoryWeakReference == null) || !this.explorerAddressRepositoryWeakReference.TryGetTarget(out explorerAddressRepository))
                {
                    explorerAddressRepository = new ExplorerAddressRepository(this.DatabaseInstance);

                    this.explorerAddressRepositoryWeakReference = new WeakReference<ExplorerAddressRepository>(explorerAddressRepository);
                }

                return explorerAddressRepository;
            }
        }

        public TransferRepository TransferRepository
        {
            get
            {
                TransferRepository transferRepository = null;

                if ((this.transferRepositoryWeakReference == null) || !this.transferRepositoryWeakReference.TryGetTarget(out transferRepository))
                {
                    transferRepository = new TransferRepository(this.DatabaseInstance);

                    this.transferRepositoryWeakReference = new WeakReference<TransferRepository>(transferRepository);
                }

                return transferRepository;
            }
        }

        public ExplorerTransactionRepository ExplorerTransactionRepository
        {
            get
            {
                ExplorerTransactionRepository explorerTransactionRepository = null;

                if ((this.explorerTransactionRepositoryWeakReference == null) || !this.explorerTransactionRepositoryWeakReference.TryGetTarget(out explorerTransactionRepository))
                {
                    explorerTransactionRepository = new ExplorerTransactionRepository(this.DatabaseInstance);

                    this.explorerTransactionRepositoryWeakReference = new WeakReference<ExplorerTransactionRepository>(explorerTransactionRepository);
                }

                return explorerTransactionRepository;
            }
        }

        public Database DatabaseInstance
        {
            get
            {
                lock (DatabaseRepository.Mutex)
                {
                    return this.database;
                }
            }
            set
            {
                lock (DatabaseRepository.Mutex)
                {
                    DatabaseRepository.CreateDatabaseError = null;

                    if (value != null)
                    {
                        DatabaseRepository.CreateDatabaseError = value.EnsureDatabaseExists();
                    }

                    if (DatabaseRepository.CreateDatabaseError == null)
                    {
                        this.database = value;
                    }
                }
            }
        }

        #endregion

        #region static properties

        public static DatabaseRepository Instance { get; set; }

        #endregion

        #region constructors

        public DatabaseRepository()
        {
            DatabaseRepository.Instance = this;
        }

        #endregion

        #region public methods

        public void Initialize()
        {
            Database database = new Database();
            database.Initalize(null);
            this.DatabaseInstance = database;            
        }

        #endregion
    }
}
