﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.SQL;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;

namespace BlockExplorer.Services
{
    public class CollectionsService
    {
        #region static data

        public static object Mutex = new object();

        #endregion

        #region data

        private WeakReference<MetadataObjectCollection<ExplorerAddress>> storedExplorerAddressesCollectionReference;

        #endregion

        #region properties

        public MetadataObjectCollection<ExplorerAddress> StoredExplorerAddresses
        {
            get
            {
                MetadataObjectCollection<ExplorerAddress> storedExplorerAddressesCollection = null;

                if ((this.storedExplorerAddressesCollectionReference == null) || !this.storedExplorerAddressesCollectionReference.TryGetTarget(out storedExplorerAddressesCollection))
                {
                    storedExplorerAddressesCollection = new MetadataObjectCollection<ExplorerAddress>(DatabaseRepository.Instance.ExplorerAddressRepository.ExplorerAddresses_IsMarkedForStorage, 
                        async delegate (ExplorerAddress item)
                        {
                            MetadataPageViewModelBase.DataErrorTypes error = MetadataPageViewModelBase.DataErrorTypes.None;

                            if (!item.IsMarkedForStorage)
                            {
                                error = await item.ChangeMetadata_IsMarkedForStorage(true);
                            }
                            else
                            {
                                error = MetadataPageViewModelBase.DataErrorTypes.RequestedDataChangeParameterInvalid;
                            }                         

                            return error;
                        },
                        async delegate (ExplorerAddress item)
                        {
                            MetadataPageViewModelBase.DataErrorTypes error = MetadataPageViewModelBase.DataErrorTypes.None;

                            if (item.IsMarkedForStorage)
                            {
                                error = await item.ChangeMetadata_IsMarkedForStorage(false);
                            }
                            else
                            {
                                error = MetadataPageViewModelBase.DataErrorTypes.RequestedDataChangeParameterInvalid;
                            }

                            return error;
                        }
                    );

                    this.storedExplorerAddressesCollectionReference = new WeakReference<MetadataObjectCollection<ExplorerAddress>>(storedExplorerAddressesCollection);
                }

                return storedExplorerAddressesCollection;
            }
        }

        #endregion

        #region static properties

        public static CollectionsService Instance { get; set; }

        #endregion

        #region constructors

        public CollectionsService()
        {
            CollectionsService.Instance = this;
        }

        #endregion

        #region public methods

        public void Initialize()
        {
        }

        #endregion
    }
}
