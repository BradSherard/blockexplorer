﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Services
{
    public class User
    {
        #region constants
        
        #endregion

        #region data

        private object mutex = new object();

        #endregion

        #region properties

        public string DirectoryPath { get; private set; }

        #endregion

        #region constructors

        public User()
        {
        }

        #endregion

        #region public methods

        public void Initialize()
        {
            DeterminePathInfo();
        }

        #endregion

        #region non-public methods

        private void DeterminePathInfo()
        {
            this.DirectoryPath = Environment.CurrentDirectory;
        }

        #endregion
    }
}
