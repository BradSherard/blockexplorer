﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Platform.Generic;
using SQLite.Net;
using SQLite.Net.Attributes;
using SQLite.Net.Interop;
using BlockExplorer.Services;
using BlockExplorer.Data.SQL;
using System.Diagnostics;

namespace BlockExplorer.Services
{
    public class Database : IDisposable
    {
        #region constants

        private const string fileName = "testDatabase.sqlite";
        private const string DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffffffZ";
        private static readonly IntPtr NegativePointer = new IntPtr(-1);

        #endregion

        #region data

        private Queue<Action> changesQueue = new Queue<Action>();
        private ISQLitePlatform sqlitePlatform;
        private string filePath;

        private object mutex = new object();

        #endregion

        #region properties

        public SQLiteConnection SQLiteConnectionInstance { get; private set; }

        public int BatchSize { get; private set; }

        #endregion

        #region constructors

        public Database()
        {
            this.BatchSize = 100;
        }

        #endregion

        #region public methods

        static void BindParameter(ISQLiteApi isqLite3Api, IDbStatement stmt, int index, object value, bool storeDateTimeAsTicks, IBlobSerializer serializer)
        {
            if (value == null)
            {
                isqLite3Api.BindNull(stmt, index);
            }
            else
            {
                if (value is int)
                {
                    isqLite3Api.BindInt(stmt, index, (int)value);
                }
                else if (value is ISerializable<int>)
                {
                    isqLite3Api.BindInt(stmt, index, ((ISerializable<int>)value).Serialize());
                }
                else if (value is string)
                {
                    isqLite3Api.BindText16(stmt, index, (string)value, -1, Database.NegativePointer);
                }
                else if (value is ISerializable<string>)
                {
                    isqLite3Api.BindText16(stmt, index, ((ISerializable<string>)value).Serialize(), -1, Database.NegativePointer);
                }
                else if (value is byte || value is ushort || value is sbyte || value is short)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(value));
                }
                else if (value is ISerializable<byte>)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(((ISerializable<byte>)value).Serialize()));
                }
                else if (value is ISerializable<ushort>)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(((ISerializable<ushort>)value).Serialize()));
                }
                else if (value is ISerializable<sbyte>)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(((ISerializable<sbyte>)value).Serialize()));
                }
                else if (value is ISerializable<short>)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(((ISerializable<short>)value).Serialize()));
                }
                else if (value is bool)
                {
                    isqLite3Api.BindInt(stmt, index, (bool)value ? 1 : 0);
                }
                else if (value is ISerializable<bool>)
                {
                    isqLite3Api.BindInt(stmt, index, ((ISerializable<bool>)value).Serialize() ? 1 : 0);
                }
                else if (value is uint || value is long)
                {
                    isqLite3Api.BindInt64(stmt, index, Convert.ToInt64(value));
                }
                else if (value is ISerializable<uint>)
                {
                    isqLite3Api.BindInt64(stmt, index, Convert.ToInt64(((ISerializable<uint>)value).Serialize()));
                }
                else if (value is ISerializable<long>)
                {
                    isqLite3Api.BindInt64(stmt, index, Convert.ToInt64(((ISerializable<long>)value).Serialize()));
                }
                else if (value is float || value is double || value is decimal)
                {
                    isqLite3Api.BindDouble(stmt, index, Convert.ToDouble(value));
                }
                else if (value is ISerializable<float>)
                {
                    isqLite3Api.BindDouble(stmt, index, Convert.ToDouble(((ISerializable<float>)value).Serialize()));
                }
                else if (value is ISerializable<double>)
                {
                    isqLite3Api.BindDouble(stmt, index, Convert.ToDouble(((ISerializable<double>)value).Serialize()));
                }
                else if (value is ISerializable<decimal>)
                {
                    isqLite3Api.BindDouble(stmt, index, Convert.ToDouble(((ISerializable<decimal>)value).Serialize()));
                }
                else if (value is TimeSpan)
                {
                    isqLite3Api.BindInt64(stmt, index, ((TimeSpan)value).Ticks);
                }
                else if (value is ISerializable<TimeSpan>)
                {
                    isqLite3Api.BindInt64(stmt, index, ((ISerializable<TimeSpan>)value).Serialize().Ticks);
                }
                else if (value is DateTime)
                {
                    if (storeDateTimeAsTicks)
                    {
                        long ticks = ((DateTime)value).ToUniversalTime().Ticks;
                        isqLite3Api.BindInt64(stmt, index, ticks);
                    }
                    else
                    {
                        string val = ((DateTime)value).ToUniversalTime().ToString(Database.DateTimeFormat, CultureInfo.InvariantCulture);
                        isqLite3Api.BindText16(stmt, index, val, -1, Database.NegativePointer);
                    }
                }
                else if (value is DateTimeOffset)
                {
                    isqLite3Api.BindInt64(stmt, index, ((DateTimeOffset)value).UtcTicks);
                }
                else if (value is ISerializable<DateTime>)
                {
                    if (storeDateTimeAsTicks)
                    {
                        long ticks = ((ISerializable<DateTime>)value).Serialize().ToUniversalTime().Ticks;
                        isqLite3Api.BindInt64(stmt, index, ticks);
                    }
                    else
                    {
                        string val = ((ISerializable<DateTime>)value).Serialize().ToUniversalTime().ToString(Database.DateTimeFormat, CultureInfo.InvariantCulture);
                        isqLite3Api.BindText16(stmt, index, val, -1, Database.NegativePointer);
                    }
                }
                else if (value.GetType().GetTypeInfo().IsEnum)
                {
                    isqLite3Api.BindInt(stmt, index, Convert.ToInt32(value));
                }
                else if (value is byte[])
                {
                    isqLite3Api.BindBlob(stmt, index, (byte[])value, ((byte[])value).Length, Database.NegativePointer);
                }
                else if (value is ISerializable<byte[]>)
                {
                    isqLite3Api.BindBlob(stmt, index, ((ISerializable<byte[]>)value).Serialize(), ((ISerializable<byte[]>)value).Serialize().Length,
                        Database.NegativePointer);
                }
                else if (value is Guid)
                {
                    isqLite3Api.BindText16(stmt, index, ((Guid)value).ToString(), 72, Database.NegativePointer);
                }
                else if (value is ISerializable<Guid>)
                {
                    isqLite3Api.BindText16(stmt, index, ((ISerializable<Guid>)value).Serialize().ToString(), 72, Database.NegativePointer);
                }
                else if (serializer != null && serializer.CanDeserialize(value.GetType()))
                {
                    var bytes = serializer.Serialize(value);
                    isqLite3Api.BindBlob(stmt, index, bytes, bytes.Length, Database.NegativePointer);
                }
                else
                {
                    throw new NotSupportedException("Cannot store type: " + value.GetType());
                }
            }
        }

        public bool DatabaseExists()
        {
            return File.Exists(this.filePath);
        }

        public void Dispose()
        {
            if (this.SQLiteConnectionInstance != null)
            {
                lock (this.mutex)
                {
                    if (this.SQLiteConnectionInstance != null)
                    {
                        this.SQLiteConnectionInstance.Close();
                        this.SQLiteConnectionInstance.Dispose();
                    }
                }
            }
        }

        public Exception EnsureDatabaseExists()
        {
            lock (DatabaseRepository.Mutex)
            {
                Exception error = null;
                bool exists = false;

                try
                {
                    exists = this.DatabaseExists();

                    this.sqlitePlatform = new SQLitePlatformGeneric();

                    this.sqlitePlatform.SQLiteApi.Shutdown();
                    this.sqlitePlatform.SQLiteApi.Config(SQLite.Net.Interop.ConfigOption.Serialized);
                    this.sqlitePlatform.SQLiteApi.Initialize();

                    if (this.SQLiteConnectionInstance == null)
                    {
                        this.SQLiteConnectionInstance = new SQLiteConnection(this.sqlitePlatform, this.filePath);

                        // SQLite documentation: A single quote within the string can be encoded by putting two single quotes in a row - as in Pascal. 
                        string encodedDirectoryPath = Path.GetDirectoryName(this.filePath).Replace("'", "''");
                        
                        // more about pragmas and how certain settings affect performance
                        // https://www.sqlite.org/pragma.html
                        List<string> pragmas = new List<string>
                        {
                            "PRAGMA journal_mode = WAL;",
                            "PRAGMA locking_mode = EXCLUSIVE;",
                            "PRAGMA cache_size = 10000;",
                            "PRAGMA page_size = 4096;",
                            "PRAGMA synchronous = NORMAL;",
                            String.Format("PRAGMA temp_store_directory = '{0}';", encodedDirectoryPath)
                        };

                        foreach (string pragma in pragmas)
                        {
                            this.ExecuteScalar<string>(pragma);
                        }
                    }

                    if (exists)
                    {
                        string databaseHealthy = this.ExecuteScalar<string>("PRAGMA integrity_check");

                        if (!String.IsNullOrEmpty(databaseHealthy) && String.Equals(databaseHealthy, "ok", StringComparison.OrdinalIgnoreCase))
                        {
                        }
                        else
                        {
                            throw new Exception("Rhapsody database is NOT healthy.");
                        }

                        // create database tables if they don't exist
                        this.CreateDatabase();
                    }
                    else
                    {
                        // create the database
                        this.CreateDatabase();
                    }
                }
                catch (Exception exception)
                {
                    error = exception;
                }

                return error;
            }
        }

        public List<T> ExecuteQuery<T>(string query) where T : class, new()
        {
            List<T> result = new List<T>();

            try
            {
                Dictionary<string, int> columns = new Dictionary<string, int>();
                SQLiteCommand command = this.SQLiteConnectionInstance.CreateCommand(query);

                IDbStatement statement = Prepare(command);

                Type type = typeof(T);

                for (int columnIndex = 0; columnIndex < this.sqlitePlatform.SQLiteApi.ColumnCount(statement); columnIndex++)
                {
                    columns.Add(this.sqlitePlatform.SQLiteApi.ColumnName16(statement, columnIndex), columnIndex);
                }

                if (type == typeof(SQLiteExplorerAddress))
                {
                    result = SQLiteExplorerAddressesFromStatement(statement, columns) as List<T>;
                }
                else if (type == typeof(SQLiteTransfer))
                {
                    result = SQLiteTransfersFromStatement(statement, columns) as List<T>;
                }
                else if (type == typeof(SQLiteExplorerTransaction))
                {
                    result = SQLiteExplorerTransactionsFromStatement(statement, columns) as List<T>;
                }
                else
                {
                    result = this.SQLiteConnectionInstance.Query<T>(query);
                }

                this.sqlitePlatform.SQLiteApi.Finalize(statement);
            }
            catch (SQLiteException sqliteException)
            {
                // this most likely happened because user signed out or is closing the app so we closed the DB connection.
                Debug.WriteLine($"{sqliteException.GetType().Name}: {sqliteException.Message}.");
            }

            return result;
        }

        public T ExecuteScalar<T>(string query)
        {
            T result = default(T);

            try
            {
                result = this.SQLiteConnectionInstance.ExecuteScalar<T>(query);
            }
            catch (SQLiteException sqliteException)
            {
                // this most likely happened because user signed out or is closing the app so we closed the DB connection.
                Debug.WriteLine($"{sqliteException.GetType().Name}: {sqliteException.Message}.");
            }

            return result;
        }

        public bool HasTable<T>() where T : class
        {
            bool hasTable = false;
            string tableName;
            Type type = typeof(T);

            TableAttribute tableAttribute = type.GetTypeInfo().GetCustomAttribute<TableAttribute>();

            if (tableAttribute != null)
            {
                tableName = tableAttribute.Name;

                if ((this.SQLiteConnectionInstance != null) && !String.IsNullOrEmpty(tableName))
                {
                    var result = this.ExecuteScalar<string>(String.Format("SELECT name FROM sqlite_master WHERE type='table' AND name='{0}';", tableName));

                    if (result == tableName)
                    {
                        hasTable = true;
                    }
                }
            }

            return hasTable;
        }

        public void Initalize(string directoryPath)
        {
            ConfigureDatabasePath(directoryPath);

            EnsureDatabaseExists();
        }

        public void DeleteAllOnSubmit(IEnumerable<object> entities)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    foreach (object entity in entities)
                    {
                        this.SQLiteConnectionInstance.Delete(entity);
                    }
                }
            );
        }

        public void DeleteOnSubmit(object entity)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    this.SQLiteConnectionInstance.Delete(entity);
                }
            );
        }

        public void InsertAllOnSubmit(IEnumerable<object> entities)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    foreach (object entity in entities)
                    {
                        this.SQLiteConnectionInstance.InsertOrReplace(entity);
                    }
                }
            );
        }

        public void InsertOnSubmit(object entity)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    this.SQLiteConnectionInstance.InsertOrReplace(entity);
                }
            );
        }

        public void SubmitChangesIfAny()
        {
            if (this.changesQueue.Count > 0)
            {
                this.SQLiteConnectionInstance.RunInTransaction(
                    delegate
                    {
                        while (this.changesQueue.Count > 0)
                        {
                            Action action = null;

                            try
                            {
                                action = this.changesQueue.Dequeue();
                            }
                            catch (Exception exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }

                            action?.Invoke();
                        }
                    }
                );
            }
        }

        public void UpdateAllOnSubmit(IEnumerable<object> entities)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    foreach (object entity in entities)
                    {
                        this.SQLiteConnectionInstance.Update(entity);
                    }
                }
            );
        }

        public void UpdateOnSubmit(object entity)
        {
            this.changesQueue.Enqueue(
                delegate
                {
                    this.SQLiteConnectionInstance.Update(entity);
                }
            );
        }

        #endregion

        #region non-public methods

        private void ConfigureDatabasePath(string directoryPath)
        {
            this.filePath = Path.Combine(directoryPath ?? AppServices.Instance.User.DirectoryPath, Database.fileName);
        }

        private void CreateDatabase()
        {
            // build all the tables
            if (!HasTable<SQLiteExplorerAddress>())
            {
                this.SQLiteConnectionInstance.CreateTable<SQLiteExplorerAddress>();
            }

            if (!HasTable<SQLiteTransfer>())
            {
                this.SQLiteConnectionInstance.CreateTable<SQLiteTransfer>();
            }

            if (!HasTable<SQLiteExplorerTransaction>())
            {
                this.SQLiteConnectionInstance.CreateTable<SQLiteExplorerTransaction>();
            }
        }

        /// <summary>
        /// Hack of the real prepare private function in SQLiteCommand.
        /// Based upon some of the source code, we know how the bindings are exposed and formatted.
        /// So we can assume quite a bit about the binding and get away with not accessing them directly.
        /// Source: https://github.com/oysteinkrog/SQLite.Net-PCL/blob/master/src/SQLite.Net/SQLiteCommand.cs
        /// https://github.com/oysteinkrog/SQLite.Net-PCL/blob/master/src/SQLite.Net/SQLiteConnection.cs
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private IDbStatement Prepare(SQLiteCommand command, params object[] args)
        {
                //    public SQLiteCommand CreateCommand(string cmdText, params object[] args)
                //{
                //    if (!_open)
                //    {
                //        throw SQLiteException.New(Result.Error, "Cannot create commands from unopened database");
                //    }

                //    var cmd = NewCommand();
                //    cmd.CommandText = cmdText;
                //    foreach (var o in args)
                //    {
                //        cmd.Bind(null, o);
                //    }
                //    return cmd;
                //}

                //public void Bind([CanBeNull] string name, [CanBeNull] object val)
                //{
                //    _bindings.Add(new Binding
                //    {
                //        Name = name,
                //        Value = val
                //    });
                //}

            IDbStatement statement = this.sqlitePlatform.SQLiteApi.Prepare2(this.SQLiteConnectionInstance.Handle, command.CommandText);
            int nextIndex = 1;

            foreach (object o in args)
            {
                BindParameter(this.sqlitePlatform.SQLiteApi, statement, nextIndex, o, this.SQLiteConnectionInstance.StoreDateTimeAsTicks, this.SQLiteConnectionInstance.Serializer);

                nextIndex += 1;
            }  

            return statement;
        }

        #region SQLite types from statement

        private List<SQLiteExplorerAddress> SQLiteExplorerAddressesFromStatement(IDbStatement statement, Dictionary<string, int> columns)
        {
            List<SQLiteExplorerAddress> explorerAddresses = new List<SQLiteExplorerAddress>();

            while (this.sqlitePlatform.SQLiteApi.Step(statement) == Result.Row)
            {
                SQLiteExplorerAddress explorerAddress = new SQLiteExplorerAddress();

                if (columns.ContainsKey(nameof(explorerAddress.AddressId)))
                {
                    explorerAddress.AddressId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(explorerAddress.AddressId)]);
                }

                if (columns.ContainsKey(nameof(explorerAddress.Balance)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerAddress.Balance)]);

                    explorerAddress.Balance = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerAddress.Balance)]);
                }

                if (columns.ContainsKey(nameof(explorerAddress.HeightScanned)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerAddress.HeightScanned)]);

                    explorerAddress.HeightScanned = (columnType == ColType.Null) ? new int?() : this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(explorerAddress.HeightScanned)]);
                }

                if (columns.ContainsKey(nameof(explorerAddress.IsMarkedForStorage)))
                {
                    explorerAddress.IsMarkedForStorage = this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(explorerAddress.IsMarkedForStorage)]) == 1 ? true : false;
                }

                if (columns.ContainsKey(nameof(explorerAddress.LastSyncDate)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerAddress.LastSyncDate)]);

                    explorerAddress.LastSyncDate = (columnType == ColType.Null) ? new DateTime?() : new DateTime(this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerAddress.LastSyncDate)]));
                }

                if (columns.ContainsKey(nameof(explorerAddress.MetadataLevel)))
                {
                    explorerAddress.MetadataLevel = (Data.ExplorerAddress.MetadataLevels)Convert.ToInt32(this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(explorerAddress.MetadataLevel)]));
                }

                if (columns.ContainsKey(nameof(explorerAddress.TotalReceived)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerAddress.TotalReceived)]);

                    explorerAddress.TotalReceived = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerAddress.TotalReceived)]);
                }

                if (columns.ContainsKey(nameof(explorerAddress.TotalSent)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerAddress.TotalSent)]);

                    explorerAddress.TotalSent = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerAddress.TotalSent)]);
                }

                if (columns.ContainsKey(nameof(explorerAddress.TransferIds)))
                {
                    explorerAddress.TransferIds = this.sqlitePlatform.SQLiteApi.ColumnByteArray(statement, columns[nameof(explorerAddress.TransferIds)]);
                }

                explorerAddresses.Add(explorerAddress);
            }

            return explorerAddresses;
        }

        private List<SQLiteTransfer> SQLiteTransfersFromStatement(IDbStatement statement, Dictionary<string, int> columns)
        {
            List<SQLiteTransfer> transfers = new List<SQLiteTransfer>();

            while (this.sqlitePlatform.SQLiteApi.Step(statement) == Result.Row)
            {
                SQLiteTransfer transfer = new SQLiteTransfer();

                if (columns.ContainsKey(nameof(transfer.AddressId)))
                {
                    transfer.AddressId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(transfer.AddressId)]);
                }

                if (columns.ContainsKey(nameof(transfer.Amount)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(transfer.Amount)]);

                    transfer.Amount = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(transfer.Amount)]);
                }

                if (columns.ContainsKey(nameof(transfer.IsTransferIncoming)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(transfer.IsTransferIncoming)]);

                    transfer.IsTransferIncoming = (columnType == ColType.Null) ? new bool?() : this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(transfer.IsTransferIncoming)]) == 1 ? true : false;
                }

                if (columns.ContainsKey(nameof(transfer.TransactionId)))
                {
                    transfer.TransactionId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(transfer.TransactionId)]);
                }

                if (columns.ContainsKey(nameof(transfer.TransferId)))
                {
                    transfer.TransferId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(transfer.TransferId)]);
                }

                transfers.Add(transfer);
            }

            return transfers;
        }

        private List<SQLiteExplorerTransaction> SQLiteExplorerTransactionsFromStatement(IDbStatement statement, Dictionary<string, int> columns)
        {
            List<SQLiteExplorerTransaction> explorerTransactions = new List<SQLiteExplorerTransaction>();

            while (this.sqlitePlatform.SQLiteApi.Step(statement) == Result.Row)
            {
                SQLiteExplorerTransaction explorerTransaction = new SQLiteExplorerTransaction();

                if (columns.ContainsKey(nameof(explorerTransaction.BlockId)))
                {
                    explorerTransaction.BlockId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(explorerTransaction.BlockId)]);
                }

                if (columns.ContainsKey(nameof(explorerTransaction.Fee)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerTransaction.Fee)]);

                    explorerTransaction.Fee = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerTransaction.Fee)]);
                }

                if (columns.ContainsKey(nameof(explorerTransaction.FirstSeen)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerTransaction.FirstSeen)]);

                    explorerTransaction.FirstSeen = (columnType == ColType.Null) ? new DateTime?() : new DateTime(this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerTransaction.FirstSeen)]));
                }

                if (columns.ContainsKey(nameof(explorerTransaction.IsMarkedForStorage)))
                {
                    explorerTransaction.IsMarkedForStorage = this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(explorerTransaction.IsMarkedForStorage)]) == 1 ? true : false;
                }

                if (columns.ContainsKey(nameof(explorerTransaction.MetadataLevel)))
                {
                    explorerTransaction.MetadataLevel = (Data.ExplorerAddress.MetadataLevels)Convert.ToInt32(this.sqlitePlatform.SQLiteApi.ColumnInt(statement, columns[nameof(explorerTransaction.MetadataLevel)]));
                }

                if (columns.ContainsKey(nameof(explorerTransaction.ReceivedIds)))
                {
                    explorerTransaction.ReceivedIds = this.sqlitePlatform.SQLiteApi.ColumnByteArray(statement, columns[nameof(explorerTransaction.ReceivedIds)]);
                }               

                if (columns.ContainsKey(nameof(explorerTransaction.SpentIds)))
                {
                    explorerTransaction.SpentIds = this.sqlitePlatform.SQLiteApi.ColumnByteArray(statement, columns[nameof(explorerTransaction.SpentIds)]);
                }

                if (columns.ContainsKey(nameof(explorerTransaction.TotalSpent)))
                {
                    ColType columnType = this.sqlitePlatform.SQLiteApi.ColumnType(statement, columns[nameof(explorerTransaction.TotalSpent)]);

                    explorerTransaction.TotalSpent = (columnType == ColType.Null) ? new long?() : this.sqlitePlatform.SQLiteApi.ColumnInt64(statement, columns[nameof(explorerTransaction.TotalSpent)]);
                }

                if (columns.ContainsKey(nameof(explorerTransaction.TransactionId)))
                {
                    explorerTransaction.TransactionId = this.sqlitePlatform.SQLiteApi.ColumnText16(statement, columns[nameof(explorerTransaction.TransactionId)]);
                }


                explorerTransactions.Add(explorerTransaction);
            }

            return explorerTransactions;
        }

        #endregion

        #endregion
    }
}
