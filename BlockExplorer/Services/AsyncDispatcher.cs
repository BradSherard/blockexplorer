﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;

namespace BlockExplorer.Services
{
    public class AsyncDispatcher
    {
        #region data

        private Dispatcher dispatcher = null;


        #endregion

        #region static properties

        public static AsyncDispatcher Instance { get; private set; }

        #endregion

        #region properties


        public int DispatcherThreadId
        {
            get
            {
                int threadId = 0;

                if (this.dispatcher != null)
                {
                    threadId = this.dispatcher.Thread.ManagedThreadId;
                }

                return threadId;
            }
        }


        #endregion

        #region constructors

        public AsyncDispatcher()
        {

            this.dispatcher = Dispatcher.CurrentDispatcher;
        }

        #endregion

        #region public static methods

        public static void EnsureInstance()
        {
            if (AsyncDispatcher.Instance == null)
            {
                AsyncDispatcher.Instance = new AsyncDispatcher();
            }
        }

        #endregion

        #region public methods

        public bool IsDispatcherThread()
        {

            return this.dispatcher.CheckAccess();

        }

        public void Invoke(Action action)
        {
            if (action != null)
            {
                if (this.IsDispatcherThread())
                {
                    action();
                }
                else
                {
                    this.dispatcher.Invoke(action);
                }
            }
        }

        public void InvokeAsync(Action action)
        {
            if (action != null)
            {
                if (this.IsDispatcherThread())
                {
                    action();
                }
                else
                {
                    this.dispatcher.BeginInvoke(action);
                }
            }
        }

        #endregion 
    }
}
