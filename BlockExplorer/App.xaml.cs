﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using BlockExplorer.Pages;
using BlockExplorer.Pages.Core;
using BlockExplorer.Services;

namespace BlockExplorer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region data

        private static string currentPageKey;
        private static Page previousPage;

        #endregion

        #region properties

        public static Frame ContentFrame { get; set; }

        public static NavigationMode NavigationMode { get; set; }

        public static Uri PageUri { get; set; }

        public static Page Page { get; set; }

        private static Frame RootFrame
        {
            get
            {
                return Application.Current.MainWindow.Content as Frame;          
            }
        }

        public static string InitialNavigationState { get; set; }

        #endregion

        #region public methods

        public static void Navigate(Page sourcePage)
        {
            if ((sourcePage != null) && (App.Page?.GetType() != sourcePage.GetType()))
            {
                App.ContentFrame?.Navigate(sourcePage);
            }
        }

        public static void Navigate<T>(Page sourcePage, T parameter)
        {
            if (sourcePage != null)
            {
                // if same page and same parameter, do nothing

                string json = JsonConvert.SerializeObject(parameter);

                // Navigation parameters are serialized to JSON so it is a string,
                // because the Frame can only save serialize simple types and not arbitrary objects
                App.ContentFrame?.Navigate(sourcePage, json);
            }
        }

        #endregion

        #region non-public methods

        protected async override void OnStartup(StartupEventArgs e)
        {
            await EnsureAppIsInitialized(e.Args);

            base.OnStartup(e);
        }

        private void InitializeApp()
        {
            AppServices appServices = this.Resources["AppServices"] as AppServices;
            CollectionsService collectionsService = this.Resources["CollectionsService"] as CollectionsService;

            AppServices.Instance.User.Initialize();
            DatabaseRepository.Instance.Initialize();
            collectionsService.Initialize();
            AppServices.Instance.RestCaller.Initialize();
            AppServices.Instance.BitcoinExplorer.Initialize();

            // sample execution, not to be done for real here
            //var test = Task.Run(() => AppServices.Instance.RestCaller.GetBitcoinPriceAsync("CNY")).Result;
        }

        private async Task EnsureAppIsInitialized(string[] arguments)
        {
            await Task.Run(
                delegate
                {
                    // later might add a state manager for suspended restore which would require async
                }
            );

            Frame rootFrame = Application.Current.MainWindow?.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                AsyncDispatcher.EnsureInstance();
                AsyncDispatcher asyncDispatcher = AsyncDispatcher.Instance;

                InitializeApp();

                Application.Current.MainWindow = new Window();
                Application.Current.MainWindow.Title = "Block Explorer";
                Application.Current.MainWindow.Icon = System.Windows.Media.Imaging.BitmapFrame.Create(new Uri("pack://application:,,,/BlockExplorer;component/Images/MainWindowIcon.ico"));

                rootFrame = new Frame();
                rootFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
                Application.Current.MainWindow.Content = rootFrame;

                App.RootFrame.Navigated += RootFrame_Navigated;

                Application.Current.MainWindow.Show();
                Application.Current.MainWindow.Activate();
            }

            if (rootFrame.Content == null)
            {
                //// When the navigation stack isn't restored navigate to the first page,
                //// configuring the new page by passing required information as a navigation
                //// parameter
                //Type pageType = typeof(MainPage);

                //rootFrame.Navigate(pageType, arguments);

                rootFrame.Navigate(new MainPage(), arguments);
            }
        }

        private void RootFrame_Navigated(object sender, NavigationEventArgs e)
        {
            object test = e.Content;
            Type type = test.GetType();

            Page navigateFromPage = previousPage;
            Page navigateToPage = e.Content as Page;

            App.previousPage = navigateToPage;
            
            if ((navigateFromPage != null) && (navigateFromPage is IPageNavigation))
            {
                IPageNavigation iPageNavigation = navigateFromPage as IPageNavigation;

                //IDictionary<string, object> frameState = SuspensionManager.SessionStateForFrame(App.RootFrame);
                //IDictionary<string, object> pageState = null;

                //if (frameState.ContainsKey(App.currentPageKey))
                //{
                //    pageState = frameState[App.currentPageKey] as IDictionary<string, object>;
                //}
                //else
                //{
                //    pageState = new Dictionary<string, object>();
                //}

                //iPageNavigation.OnNavigatedFrom(e, pageState);

                //frameState[App.currentPageKey] = pageState;

                iPageNavigation.OnNavigatedFrom(e, null);
            }

            if (navigateToPage is IPageNavigation)
            {
                IPageNavigation iPageNavigation = navigateToPage as IPageNavigation;

                //IDictionary<string, object> frameState = SuspensionManager.SessionStateForFrame(App.RootFrame);
                //IReadOnlyDictionary<string, object> readOnlyPageState = null;
                //App.currentPageKey = $"Page-{App.RootFrame.BackStackDepth}";

                //if (e.NavigationMode == NavigationMode.New)
                //{
                //    string nextPageKey = App.currentPageKey;
                //    int nextPageIndex = App.RootFrame.BackStackDepth;

                //    while (frameState.Remove(nextPageKey))
                //    {
                //        nextPageIndex++;
                //        nextPageKey = $"Page-{nextPageIndex}";
                //    }
                //}
                //else // if there are no navigation event arguments or NavigationMode differs from NavigationMode.New
                //{
                //    IDictionary<string, object> pageState = frameState[App.currentPageKey] as IDictionary<string, object>;
                //    readOnlyPageState = new ReadOnlyDictionary<string, object>(pageState);
                //}

                //iPageNavigation.OnNavigatedTo(e, readOnlyPageState);
                //iPageNavigation.PageState = readOnlyPageState;

                iPageNavigation.OnNavigatedTo(e, null);
                iPageNavigation.PageState = null;
            }
        }

        #endregion
    }
}
