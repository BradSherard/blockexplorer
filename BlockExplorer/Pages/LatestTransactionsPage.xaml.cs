﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;
using QBitNinja.Client;
using NBitcoin;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Data;

namespace BlockExplorer.Pages
{
    public partial class LatestTransactionsPage : ViewModelPage<LatestTransactionsPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
        }

        #endregion

        #region constructor

        public LatestTransactionsPage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region public methods

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public object GenerateNavigationParametersFromData(object data)
        {
            LatestTransactionsPage.NavigationParameters parameters = new NavigationParameters();

            return parameters;
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Data?.StopUpdating();
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            ExplorerLatestTransactions latestTransactions = new ExplorerLatestTransactions(ExplorerLatestTransactions.Default_Update_Period);
            this.ViewModel = new LatestTransactionsPageViewModel(latestTransactions);
            this.ViewModel?.Data?.StartUpdating();
        }

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void LatestTransactionsPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
