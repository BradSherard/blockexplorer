﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;


namespace BlockExplorer.Pages
{
    /// <summary>
    /// Interaction logic for StartPage.xaml
    /// </summary>
    public partial class StartPage : ViewModelPage<StartPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {       
        }

        #endregion

        #region constructor

        public StartPage()
        {
            InitializeComponent();
        }

        #endregion

        #region public methods

        public object GenerateNavigationParametersFromData(object data)
        {
            StartPage.NavigationParameters parameters = new NavigationParameters();

            return parameters;
        }

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            
        }

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void StartPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
