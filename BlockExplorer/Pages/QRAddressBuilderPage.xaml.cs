﻿using BlockExplorer.Pages.Core;
using BlockExplorer.Services;
using BlockExplorer.ViewModels;
using NBitcoin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BlockExplorer.Pages
{
    public partial class QRAddressBuilderPage : ViewModelPage<QRAddressBuilderPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
        }

        #endregion

        #region constructor

        public QRAddressBuilderPage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region properties

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region public methods

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public object GenerateNavigationParametersFromData(object data)
        {
            QRAddressBuilderPage.NavigationParameters parameters = new NavigationParameters();

            return parameters;
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            this.ViewModel = new QRAddressBuilderPageViewModel();
        }

        #endregion

        #region non public methods

        private void AddressTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.OriginalKey == VirtualKey.Enter)
            //{
            //}
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                ParseAddress();
            }
        }

        private void ParseAddress()
        {
            string addressValue = this.AddressTextBox.Text;

            ThreadPool.QueueUserWorkItem
            (
                delegate
                {
                    try
                    {
                        Base58Data address = Base58Data.GetFromBase58Data(addressValue, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network);
                        this.ViewModel.Address = address;
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        Debug.WriteLine($"Could not parse the following string to Base58Data: {addressValue}");
                    }
                }
            );
        }

        #endregion
    }
}
