﻿using System.Collections.Generic;
using System.Windows.Navigation;

namespace BlockExplorer.Pages.Core
{
    public interface IPageNavigation
    {
        IReadOnlyDictionary<string, object> PageState { get; set; }

        bool BackRequested();
        void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state);
        void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state);
        // brads todo: find a way to more tightly ensure type for page navigation command parameters
        object GenerateNavigationParametersFromData(object data); // should expect the least and closest data describing the NavigationParameters 
    }
}
