﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.ComponentModel;
using BlockExplorer.Data.Core;
using BlockExplorer.Services;

namespace BlockExplorer.Pages.Core
{
    public class ViewModelPage<T> : Page, INotifyPropertyChanged where T : PropertyChangeObject, IDisposable
    {
        #region events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region data

        protected object mutex = new object();
        private T viewModel;

        #endregion

        #region properties

        public T ViewModel
        {
            get { return this.viewModel; }
            set
            {
                if (!Equals(this.viewModel, value))
                {
                    lock (this.mutex)
                    {
                        // double check
                        if (!Equals(this.viewModel, value))
                        {
                            T oldValue = this.viewModel;
                            this.viewModel = value;

                            AsyncDispatcher.Instance.InvokeAsync
                            (
                                delegate
                                {
                                    this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(this.viewModel)));
                                }
                            );
                        }
                    }
                }
            }
        }

        #endregion
    }
}
