﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;
using QBitNinja.Client;
using NBitcoin;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Data;

namespace BlockExplorer.Pages
{
    public partial class AddressPage : ViewModelPage<AddressPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
            [JsonConverter(typeof(Base58DataJSONConverter))]
            public Base58Data AddressId { get; set; }
        }

        #endregion

        #region constructor

        public AddressPage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region public methods

        public object GenerateNavigationParametersFromData(object data)
        {
            AddressPage.NavigationParameters parameters = null;

            Base58Data parsedData = data as Base58Data;

            if (parsedData != null)
            {
                parameters = new NavigationParameters()
                {
                    AddressId = parsedData,
                };
            }

            return parameters;
        }

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            NavigationParameters parameters = JsonConvert.DeserializeObject<NavigationParameters>(e.ExtraData as string, new Base58DataJSONConverter());

            ExplorerAddress address = ExplorerAddress.ExplorerAddressFromAddressId(parameters?.AddressId);
            this.ViewModel = new AddressPageViewModel(address);

            ThreadPool.QueueUserWorkItem
            (
                async delegate
                {
                    this.ViewModel.DataError = await this.ViewModel.Data.EnsureLiteMetadata();
                }
            );
        }

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void AddressPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
