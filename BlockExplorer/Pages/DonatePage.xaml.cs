﻿using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BlockExplorer.Pages
{
    public partial class DonatePage : ViewModelPage<DonatePageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
        }

        #endregion

        #region constructor

        public DonatePage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region properties

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region public methods

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public object GenerateNavigationParametersFromData(object data)
        {
            DonatePage.NavigationParameters parameters = new NavigationParameters();

            return parameters;
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            this.ViewModel = new DonatePageViewModel();
        }

        #endregion
    }
}
