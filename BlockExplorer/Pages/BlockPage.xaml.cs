﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;
using QBitNinja.Client;
using NBitcoin;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Data;

namespace BlockExplorer.Pages
{
    public partial class BlockPage : ViewModelPage<BlockPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
            [JsonConverter(typeof(UInt256JSONConverter))]
            public uint256 BlockId { get; set; }
        }

        #endregion

        #region constructor

        public BlockPage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region public methods

        public object GenerateNavigationParametersFromData(object data)
        {
            BlockPage.NavigationParameters parameters = null;

            uint256 parsedData = data as uint256;

            if (parsedData != null)
            {
                parameters = new NavigationParameters()
                {
                    BlockId = parsedData,
                };
            }

            return parameters;
        }

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            NavigationParameters parameters = JsonConvert.DeserializeObject<NavigationParameters>(e.ExtraData as string, new UInt256JSONConverter());
            ExplorerBlock block = ExplorerBlock.ExplorerBlockFromBlockId(parameters?.BlockId);
            this.ViewModel = new BlockPageViewModel(block);

            ThreadPool.QueueUserWorkItem
            (
                async delegate
                {
                    this.ViewModel.DataError = await this.ViewModel.Data.EnsureFullMetadata();
                }
            );
        }

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void BlockPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
