﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NBitcoin;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.Services;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels;

namespace BlockExplorer.Pages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainPage : ViewModelPage<MainPageViewModel>
    {
        #region data

        string currentPageKey;
        private bool isFirstNavigation = true;
        private bool isRestoringFrameState = false;

        #endregion

        #region properties


        #endregion

        #region constructors

        public MainPage()
        {
            this.DataContext = this;
            InitializeComponent();
            
            //AppServices services = AppServices.Instance;

            App.ContentFrame = this.ContentFrame;
            App.InitialNavigationState = null; // App.ContentFrame.GetNavigationState();

            // suspension manager subscriptions and registrations
            //SuspensionManager.FrameNavigationStateRestored += SuspensionManager_FrameNavigationStateRestored;
            //SuspensionManager.FrameNavigationStateSaving += SuspensionManager_FrameNavigationStateSaving;
            //SuspensionManager.RegisterFrame(App.ContentFrame, nameof(App.ContentFrame));
        }

        #endregion

        #region public methods

        #endregion

        #region non-public methods

        private void ContentFrame_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.isFirstNavigation)
            {
                this.ViewModel = new MainPageViewModel();

                App.Navigate(new StartPage(),
                    new StartPage.NavigationParameters
                    {
                    }
                );
            }
        }

        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            Page navigateFromPage = App.Page;
            Page navigateToPage = e.Content as Page;
            string navigateFromPageTypeName = navigateFromPage?.GetType().Name;
            string navigateToPageTypeName = navigateToPage?.GetType().Name;
            NavigationMode navigationMode = App.NavigationMode;
            //PageContentState pageContentState = App.PageContentState;
            Uri uri = e.Uri;

            //if (pageContentState != null)
            //{
            //    uri = pageContentState.Uri;
            //    state = pageContentState.State;
            //}

            if (navigateFromPage is IPageNavigation)
            {
                IPageNavigation iPageNavigation = navigateFromPage as IPageNavigation;
                this.NavigatedFrom(e, iPageNavigation);
            }

            App.PageUri = e.Uri;
            App.Page = navigateToPage;

            if (navigateToPage is IPageNavigation)
            {
                IPageNavigation iPageNavigation = navigateToPage as IPageNavigation;
                this.NavigatedTo(e, iPageNavigation);
            }

            if (this.isFirstNavigation)
            {
                this.isFirstNavigation = false;
            }
        }

        private void ContentFrame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            App.NavigationMode = e.NavigationMode;
        }

        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
        }

        private void ContentFrame_NavigationStopped(object sender, NavigationEventArgs e)
        {
        }

        private void NavigatedFrom(NavigationEventArgs e, IPageNavigation iPageNavigation)
        {
            if ((iPageNavigation != null) && (this.currentPageKey != null))
            {
                ////IDictionary<string, object> frameState = SuspensionManager.SessionStateForFrame(this.ContentFrame);
                //IDictionary<string, object> pageState = null;

                //if (frameState.ContainsKey(this.currentPageKey))
                //{
                //    pageState = frameState[this.currentPageKey] as IDictionary<string, object>;
                //}
                //else
                //{
                //    pageState = new Dictionary<string, object>();
                //}

                //iPageNavigation.OnNavigatedFrom(e, pageState);

                //frameState[this.currentPageKey] = pageState;

                iPageNavigation.OnNavigatedFrom(e, null);
            }
        }

        private void NavigatedTo(NavigationEventArgs e, IPageNavigation iPageNavigation)
        {
            if (iPageNavigation != null)
            {
                //IDictionary<string, object> frameState = SuspensionManager.SessionStateForFrame(this.ContentFrame);
                //IReadOnlyDictionary<string, object> readOnlyPageState = null;
                //this.currentPageKey = $"Page-{this.ContentFrame.BackStackDepth}";

                //if (e.NavigationMode == NavigationMode.New)
                //{
                //    string nextPageKey = this.currentPageKey;
                //    int nextPageIndex = this.ContentFrame.BackStackDepth;

                //    while (frameState.Remove(nextPageKey))
                //    {
                //        nextPageIndex++;
                //        nextPageKey = $"Page-{nextPageIndex}";
                //    }
                //}
                //else // if there are no navigation event arguments or NavigationMode differs from NavigationMode.New
                //{
                //    IDictionary<string, object> pageState = frameState[this.currentPageKey] as IDictionary<string, object>;
                //    readOnlyPageState = new ReadOnlyDictionary<string, object>(pageState);
                //}

                //iPageNavigation.OnNavigatedTo(e, readOnlyPageState);
                //iPageNavigation.PageState = readOnlyPageState;

                iPageNavigation.OnNavigatedTo(e, null);
            }
        }


        private void SearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.OriginalKey == VirtualKey.Enter)
            //{
            //    this.ViewModel.StartSearch();
            //}
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                StartSearch();
            }
        }

        private void StartSearch()
        {
            string searchValue = this.SearchTextBox.Text;
            string searchTypeValue = this.SearchTypeComboBox.SelectionBoxItem as string;
            bool validId = true;

            switch (searchTypeValue)
            {
                case "Transaction":
                {
                    validId = GoToTransactionPage(searchValue);
                    break;
                }
                case "Block":
                {
                    validId = GoToBlockPage(searchValue);
                    break;
                }
                case "Address":
                {
                    validId = GoToAddressPage(searchValue);
                    break;
                }
                default:
                {
                    throw new Exception();
                }
            }

            this.SearchErrorTextBlock.Visibility = validId ? Visibility.Collapsed : Visibility.Visible;
        }

        private bool GoToTransactionPage(string searchValue)
        {
            bool isAddressValid = false;
            uint256 transactionHash;

            if (uint256.TryParse(searchValue, out transactionHash))
            {
                isAddressValid = true;

                App.Navigate(new TransactionPage(),
                    new TransactionPage.NavigationParameters
                    {
                        TransactionId = transactionHash,
                    }
                );
            }

            return isAddressValid;
        }

        private bool GoToBlockPage(string searchValue)
        {
            bool isAddressValid = false;
            uint256 blockHash;

            if (uint256.TryParse(searchValue, out blockHash))
            {
                isAddressValid = true;

                App.Navigate(new BlockPage(),
                    new BlockPage.NavigationParameters
                    {
                        BlockId = blockHash,
                    }
                );
            }

            return isAddressValid;
        }

        private bool GoToAddressPage(string searchValue)
        {
            bool isAddressValid = false;
            Base58Data addressHash = null;        

            try
            {
                addressHash = Base58Data.GetFromBase58Data(searchValue, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network);
            }
            catch (Exception exception)
            {
                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                Debug.WriteLine($"Could not parse the following string to Base58Data: {searchValue}");
            }

            if (addressHash != null)
            {
                isAddressValid = true;

                App.Navigate(new AddressPage(),
                    new AddressPage.NavigationParameters
                    {
                        AddressId = addressHash,
                    }
                );
            }

            return isAddressValid;
        }
        #endregion
    }
}
