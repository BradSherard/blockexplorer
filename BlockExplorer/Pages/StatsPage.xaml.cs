﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Newtonsoft.Json;
using QBitNinja.Client;
using NBitcoin;
using BlockExplorer;
using BlockExplorer.Pages.Core;
using BlockExplorer.ViewModels;
using BlockExplorer.Data;

namespace BlockExplorer.Pages
{
    public partial class StatsPage : ViewModelPage<StatsPageViewModel>, IPageNavigation
    {
        #region types

        public class NavigationParameters
        {
        }

        #endregion

        #region constructor

        public StatsPage()
        {
            this.DataContext = this;
            InitializeComponent();
        }

        #endregion

        #region public methods

        public bool BackRequested()
        {
            throw new NotImplementedException();
        }

        public object GenerateNavigationParametersFromData(object data)
        {
            StatsPage.NavigationParameters parameters = new NavigationParameters();

            return parameters;
        }

        public void OnNavigatedFrom(NavigationEventArgs e, IDictionary<string, object> state)
        {
            this.ViewModel?.Data?.StopUpdating();
            this.ViewModel?.Dispose();
        }

        public void OnNavigatedTo(NavigationEventArgs e, IReadOnlyDictionary<string, object> state)
        {
            CallerStats explorerStats = new CallerStats(CallerStats.Default_Update_Period);
            this.ViewModel = new StatsPageViewModel(explorerStats);
            this.ViewModel?.Data?.StartUpdating();
        }

        public IReadOnlyDictionary<string, object> PageState
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        private void StatsPage_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
