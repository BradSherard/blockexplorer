﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.RestCallers.Json;

namespace BlockExplorer.ViewModels
{
    public class StatsPageViewModel : DynamicPageViewModel<CallerStats>
    {
        #region types

        private struct Properties
        {
            public string conversionPrice;
            public IEnumerable<string> currencyCodes;
            public string selectedCurrencyCode;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        private Dictionary<DataErrorTypes, string> errorMessagesDictionary = new Dictionary<DataErrorTypes, string>();

        #endregion

        #region properties      

        protected override Dictionary<DataErrorTypes, string> ErrorMessageDictionary
        {
            get
            {
                return this.errorMessagesDictionary;
            }
        }

        public string ConversionPrice
        {
            get
            {
                return this.properties.conversionPrice;
            }
            set
            {
                SetProperty(ref this.properties.conversionPrice, ref value);
            }
        }

        public IEnumerable<string> CurrencyCodes
        {
            get
            {
                return this.properties.currencyCodes;
            }
            set
            {
                SetProperty(ref this.properties.currencyCodes, ref value);
            }
        }

        public string SelectedCurrencyCode
        {
            get
            {
                return this.properties.selectedCurrencyCode;
            }
            set
            {
                SetProperty(ref this.properties.selectedCurrencyCode, ref value, OnSelectedCurrencyCodeChanged);
            }
        } 

        #endregion

        #region constructor

        public StatsPageViewModel(CallerStats stats) : base(stats)
        {
            this.errorMessagesDictionary[DataErrorTypes.None] = "";
            this.errorMessagesDictionary[DataErrorTypes.NetworkRequestFailed] = "Could not get a response for requested data";
        }

        #endregion

        #region non-public methods

        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CallerStats.ConversionPrice))
            {
                // do nothing, xaml will pick up this change from the data itself
            }
        }

        private void OnSelectedCurrencyCodeChanged(string oldValue, string newvalue)
        {
            //brads todo: determine proper pattern for changing data based on changes to the UI as in the case of changing currency code
            // should changes go through view model then back to model? Probably best to include VM in case it should update too.
            this.Data.SelectedCurrencyCode = newvalue;
            this.Data.RunUpdate();
        }

        protected override void SynchronizeModelToData(CallerStats data)
        {
            this.Data = data;
            this.CurrencyCodes = Enum.GetNames(typeof(GetJsonCurrentPrice.CurrencyCodes));
            this.SelectedCurrencyCode = GetJsonCurrentPrice.CurrencyCodes.USD.ToString();

            this.Data.PropertyChanged += Data_PropertyChanged;
        }

        #endregion

        #region public methods
        #endregion
    }
}