﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.Data;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;

namespace BlockExplorer.ViewModels
{
    public class TransactionItemViewModel : ItemViewModel<ExplorerTransaction>
    {
        #region types

        private struct Properties
        {
            public GoToTransactionPageCommand goToTransactionPageCommand;
            public GoToTransactionPageCommandParameter goToTransactionPageCommandParameter;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public GoToTransactionPageCommand GoToTransactionPageCommand
        {
            get
            {
                return this.properties.goToTransactionPageCommand;
            }
            protected set
            {
                SetProperty(ref this.properties.goToTransactionPageCommand, ref value);
            }
        }

        public GoToTransactionPageCommandParameter GoToTransactionPageCommandParameter
        {
            get
            {
                return this.properties.goToTransactionPageCommandParameter;
            }
            protected set
            {
                SetProperty(ref this.properties.goToTransactionPageCommandParameter, ref value);
            }
        }

        #endregion

        #region constructors

        public TransactionItemViewModel(ExplorerTransaction transaction)
            : base(transaction)
        {
            this.Item = transaction;

            this.GoToTransactionPageCommand = new GoToTransactionPageCommand();
            this.GoToTransactionPageCommandParameter = new GoToTransactionPageCommandParameter()
            {
                Data = this.Item,
                Command = this.GoToTransactionPageCommand,
            };
        }

        #endregion

        #region public methods

        public override void Dispose()
        {
            this.Item = null;
            this.GoToTransactionPageCommand = null;
            this.GoToTransactionPageCommandParameter = null;
        }

        #endregion

        #region non-public methods

        protected override void OnItemChanged(ExplorerTransaction oldValue, ExplorerTransaction newValue)
        {
            if (this.GoToTransactionPageCommandParameter != null)
            {
                this.GoToTransactionPageCommandParameter.Data = newValue;
            }
        }

        #endregion
    }
}
