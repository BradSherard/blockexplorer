﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.Data;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;

namespace BlockExplorer.ViewModels
{
    public class AddressItemViewModel : ItemViewModel<ExplorerAddress>
    {
        #region types

        private struct Properties
        {
            public GoToAddressPageCommand goToAddressPageCommand;
            public GoToAddressPageCommandParameter goToAddressPageCommandParameter;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public GoToAddressPageCommand GoToAddressPageCommand
        {
            get
            {
                return this.properties.goToAddressPageCommand;
            }
            protected set
            {
                SetProperty(ref this.properties.goToAddressPageCommand, ref value);
            }
        }

        public GoToAddressPageCommandParameter GoToAddressPageCommandParameter
        {
            get
            {
                return this.properties.goToAddressPageCommandParameter;
            }
            protected set
            {
                SetProperty(ref this.properties.goToAddressPageCommandParameter, ref value);
            }
        }

        #endregion

        #region constructors

        public AddressItemViewModel(ExplorerAddress address)
            : base(address)
        {
            this.Item = address;

            this.GoToAddressPageCommand = new GoToAddressPageCommand();
            this.GoToAddressPageCommandParameter = new GoToAddressPageCommandParameter()
            {
                Data = this.Item,
                Command = this.GoToAddressPageCommand,
            };
        }

        #endregion

        #region public methods

        public override void Dispose()
        {
            this.Item = null;
            this.GoToAddressPageCommand = null;
            this.GoToAddressPageCommandParameter = null;
        }

        #endregion

        #region non-public methods

        protected override void OnItemChanged(ExplorerAddress oldValue, ExplorerAddress newValue)
        {
            if (this.GoToAddressPageCommandParameter != null)
            {
                this.GoToAddressPageCommandParameter.Data = newValue;
            }
        }

        #endregion
    }
}
