﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data.Core;
using NBitcoin;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using System.Windows.Media.Imaging;
using ZXing.QrCode;
using ZXing.Common;
using ZXing;
using System.Drawing;
using System.Windows.Interop;
using System.Windows;
using System.Runtime.InteropServices;
using BlockExplorer.Services;
using System.Threading;

namespace BlockExplorer.ViewModels
{
    public class QRAddressBuilderPageViewModel : PropertyChangeObject, IDisposable
    {
        #region types

        private struct Properties
        {
            public Base58Data address;
            public BitmapSource bitmapSourceQRCode;
            public string parsedAddress;
            public SaveQRCodeCommand saveQRCodeCommand;
            public SaveQRCodeCommandParameter saveQRCodeCommandParameter;
        }

        #endregion

        #region constants

        private const string addressPrefix = "bitcoin:";

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public Base58Data Address
        {
            get
            {
                return this.properties.address;
            }
            set
            {
                SetProperty(ref this.properties.address, ref value, OnAddressChanged);
            }
        }

        public BitmapSource BitmapSourceQRCode
        {
            get
            {
                return this.properties.bitmapSourceQRCode;
            }
            private set
            {
                SetProperty(ref this.properties.bitmapSourceQRCode, ref value);
            }
        }

        public string ParsedAddress
        {
            get
            {
                return this.properties.parsedAddress;
            }
            private set
            {
                SetProperty(ref this.properties.parsedAddress, ref value);
            }
        }

        public SaveQRCodeCommand SaveQRCodeCommand
        {
            get
            {
                return this.properties.saveQRCodeCommand;
            }
            private set
            {
                SetProperty(ref this.properties.saveQRCodeCommand, ref value);
            }
        }

        public SaveQRCodeCommandParameter SaveQRCodeCommandParameter
        {
            get
            {
                return this.properties.saveQRCodeCommandParameter;
            }
            private set
            {
                SetProperty(ref this.properties.saveQRCodeCommandParameter, ref value);
            }
        }

        #endregion

        #region constructor

        public QRAddressBuilderPageViewModel()
        {
            this.SaveQRCodeCommand = new SaveQRCodeCommand();
            this.SaveQRCodeCommandParameter = new SaveQRCodeCommandParameter()
            {
                ViewModel = this,
                Command = this.SaveQRCodeCommand,
            };
        }

        #endregion

        #region non-public methods

        private void OnAddressChanged(Base58Data oldValue, Base58Data newValue)
        {
            if (newValue == null)
            {
                this.ParsedAddress = null;
            }
            else
            {
                this.ParsedAddress = QRAddressBuilderPageViewModel.addressPrefix + newValue.ToWif();

                BitmapSource source = CreateImageSourceFromString(this.ParsedAddress);
                source.Freeze(); // Workaround for exception: Must create DependencySource on same Thread as the DependencyObject.

                this.BitmapSourceQRCode = source;
                this.SaveQRCodeCommandParameter.Data = this.BitmapSourceQRCode; 
            }
        }

        #region non-public methods

        private BitmapSource CreateImageSourceFromString(string data)
        {
            // assume data is base58 for now
            BitmapSource bitmapSource = null;

            // 41 x 41 matrix(version 6), 196 x 196 pix
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix matrix = qrCodeWriter.encode(data, BarcodeFormat.QR_CODE, 45, 45);

            BarcodeWriter barcodeWriter = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new EncodingOptions
                {
                    Height = 384,
                    Width = 384,
                    Margin = 0,
                }
            };

            using (Bitmap bitmap = barcodeWriter.Write(matrix))
            {
                IntPtr hbmp = bitmap.GetHbitmap();

                try
                {
                    bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(hbmp, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                }
                finally
                {
                    DeleteObject(hbmp);
                }
            }

            return bitmapSource;
        }

        [DllImport("gdi32")]
        private static extern bool DeleteObject(IntPtr hObject);

        #endregion

        #endregion

        #region public methods

        public void Dispose()
        {
        }

        #endregion
    }
}