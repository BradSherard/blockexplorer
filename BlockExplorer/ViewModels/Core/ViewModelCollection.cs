﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Collections.ObjectModel;
using BlockExplorer.Data.Core;
using BlockExplorer.Services;

namespace BlockExplorer.ViewModels.Core
{
    public class ViewModelCollection<ItemType, ItemViewModelType> : ThreadSafeObservableCollection<ItemViewModelType>, IDisposable
        where ItemType : class//, new()
        where ItemViewModelType : ItemViewModel<ItemType>
    {
        #region types

        protected new struct Properties
        {
            public ThreadSafeObservableCollection<ItemType> source;
            public ObservableCollection<ItemType> sourceObservable;
        }

        #endregion

        #region data

        protected new Properties properties = new Properties();
        private Func<ItemType, ItemViewModelType> createViewModelForItem;

        #endregion

        #region properties

        public ThreadSafeObservableCollection<ItemType> Source
        {
            get
            {
                return this.properties.source;
            }
            set
            {
                SetProperty(ref this.properties.source, ref value, OnSourceChanged);
            }
        }

        public ObservableCollection<ItemType> SourceObservable
        {
            get
            {
                return this.properties.sourceObservable;
            }
            private set
            {
                SetProperty(ref this.properties.sourceObservable, ref value, OnSourceObservableChanged);
            }
        }

        #endregion

        #region constructors

        public ViewModelCollection(ThreadSafeObservableCollection<ItemType> source, Func<ItemType, ItemViewModelType> createViewModelForItem)
            : base()
        {
            this.createViewModelForItem = createViewModelForItem;
            this.Source = source;
        }

        #endregion

        #region public methods

        public void Dispose()
        {
            this.Source = null;
        }

        #endregion

        #region non-public methods

        private void OnSourceChanged(ThreadSafeObservableCollection<ItemType> oldValue, ThreadSafeObservableCollection<ItemType> newValue)
        {
            if (oldValue != null)
            {
                this.SourceObservable = null;
            }

            if (newValue != null)
            {
                this.SourceObservable = newValue.Observable;
            }
        }

        private void OnSourceObservableChanged(ObservableCollection<ItemType> oldValue, ObservableCollection<ItemType> newValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= SourceObservable_CollectionChanged;
                this.Clear();
            }

            if (newValue != null)
            {
                // page view model sets viewModelCollection with constructor on main thread
                // brads todo: move all viewmodel stuff off dispatcher at least for viewmodelcollection, which has a dispatcher exposed component in Observable so it should be fine
                newValue.CollectionChanged += SourceObservable_CollectionChanged;
                ItemType[] items = newValue.ToArray();

                if (items.Length > 0)
                {
                    System.Threading.ThreadPool.QueueUserWorkItem(
                        delegate
                        {
                            ItemViewModelType[] itemViewModels = items.Select(item => this.createViewModelForItem(item)).ToArray();
                            this.AddItems(itemViewModels);
                        }
                    );
                }
            }
        }

        private void SourceObservable_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                {
                    for (int index = 0; index < e.NewItems.Count; ++index)
                    {
                        ItemType item = e.NewItems[index] as ItemType;
                        ItemViewModelType itemViewModel = this.createViewModelForItem(item);
                        this.Insert(e.NewStartingIndex + index, itemViewModel);
                    }

                    break;
                }
                case NotifyCollectionChangedAction.Move:
                {
                    if (e.OldItems.Count == 1)
                    {
                        this.Move(e.OldStartingIndex, e.NewStartingIndex);
                    }
                    else
                    {
                        ItemViewModelType[] viewModels = this.Skip(e.OldStartingIndex).Take(e.OldItems.Count).ToArray();

                        for (int index = 0; index < e.OldItems.Count; ++index)
                        {
                            this.RemoveAt(e.OldStartingIndex);
                        }

                        for (int index = 0; index < viewModels.Length; ++index)
                        {
                            ItemViewModelType itemViewModel = viewModels[index];

                            this.Insert(e.NewStartingIndex + index, itemViewModel);
                        }
                    }

                    break;
                }
                case NotifyCollectionChangedAction.Remove:
                {
                    for (int index = 0; index < e.OldItems.Count; ++index)
                    {
                        this.RemoveAt(e.OldStartingIndex);
                    }

                    break;
                }
                case NotifyCollectionChangedAction.Replace:
                {
                    for (int index = 0; index < e.OldItems.Count; ++index)
                    {
                        this.RemoveAt(e.OldStartingIndex);
                    }

                    goto case NotifyCollectionChangedAction.Add;
                }
                case NotifyCollectionChangedAction.Reset:
                {
                    base.Clear();

                    ItemType[] items = this.SourceObservable?.ToArray();

                    if ((items != null) && (items.Length > 0))
                    {
                        this.AddItems(items.Select(item => this.createViewModelForItem(item)));
                    }

                    break;
                }
            }
        }

        #endregion
    }
}
