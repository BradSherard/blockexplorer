﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels.Core
{
    public abstract class MetadataPageViewModelBase : PropertyChangeObject
    {
        #region types

        private struct Properties
        {
            public bool hasDataError;
            public DataErrorTypes dataError;
            public string errorMessage;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        
        #endregion

        #region enums

        public enum DataErrorTypes
        {
            None,
            RequestedDataChangeParameterInvalid,
            LiteMetadataResponseFailed,
            FullMetadataResponseFailed,
            NetworkRequestFailed, // brads todo: this can be confirmed by verifying other calls work, so it means the request was posed with bad data most likely
        }

        #endregion

        #region properties

        protected abstract Dictionary<DataErrorTypes, string> ErrorMessageDictionary { get; }

        public DataErrorTypes DataError
        {
            get
            {
                return this.properties.dataError;
            }
            set
            {
                SetProperty(ref this.properties.dataError, ref value, OnDataErrorChanged);
            }
        }

        public bool HasDataError
        {
            get
            {
                return this.properties.hasDataError;
            }
            private set
            {
                SetProperty(ref this.properties.hasDataError, ref value);
            }
        }

        public string ErrorMessage
        {
            get
            {
                return this.properties.errorMessage;
            }
            set
            {
                SetProperty(ref this.properties.errorMessage, ref value);
            }
        }

        #endregion

        #region non-public methods

        private void OnDataErrorChanged(DataErrorTypes oldValue, DataErrorTypes newValue)
        {
            if (newValue != DataErrorTypes.None)
            {
                this.HasDataError = true;

                this.ErrorMessage = this.ErrorMessageDictionary[newValue];
            }
        }

        #endregion
    }
}
