﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;

namespace BlockExplorer.ViewModels.Core
{
    public abstract class ItemViewModel<ItemType> : PropertyChangeObject, IDisposable where ItemType : class
    {
        #region types

        private struct Properties
        {
            public ItemType item;
            public PropertyChangeObject parentViewModel;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        
        public ItemType Item
        {
            get
            {
                return this.properties.item;
            }
            protected set
            {
                SetProperty(ref this.properties.item, ref value, OnItemChanged);
            }
        }

        public PropertyChangeObject ParentViewModel
        {
            get
            {
                return this.properties.parentViewModel;
            }
            set
            {
                SetProperty(ref this.properties.parentViewModel, ref value);
            }
        }

        #endregion

        #region constructors

        public ItemViewModel(ItemType item)
        {
            //this.Item = item;
        }

        #endregion

        #region public methods

        public abstract void Dispose();

        #endregion

        #region non-public methods

        protected abstract void OnItemChanged(ItemType oldValue, ItemType newValue);

        #endregion
    }
}
