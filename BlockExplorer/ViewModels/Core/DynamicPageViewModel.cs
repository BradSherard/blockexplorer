﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels.Core
{
    public abstract class DynamicPageViewModel<DynamicType> : DynamicPageViewModelBase, IDisposable where DynamicType : DynamicChangeObject
    {
        #region types

        private struct Properties
        {
            public DynamicType data;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public DynamicType Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value, OnDataChanged);
            }
        }

        #endregion

        #region constructor

        public DynamicPageViewModel(DynamicType data)
        {
            SynchronizeModelToData(data);
        }

        #endregion

        #region non-public methods

        protected abstract void SynchronizeModelToData(DynamicType data);

        private void OnDataChanged(DynamicType oldValue, DynamicType newValue)
        {
            if (oldValue != null)
            {
                oldValue.Updated -= Data_Updated;
            }

            if (newValue != null)
            {
                newValue.Updated += Data_Updated;
            }
        }

        private void Data_Updated(object sender, DynamicIsUpdatingChangedEventArgs e)
        {
            DynamicChangeObject castSender = sender as DynamicChangeObject;

            if (castSender != null)
            {
                this.DataError = castSender.ErrorType;
            }
        }

        #endregion

        #region public methods

        public void Dispose()
        {
            this.Data = null;
        }

        #endregion
    }
}