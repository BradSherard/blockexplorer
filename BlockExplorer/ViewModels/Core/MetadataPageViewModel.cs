﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels.Core
{
    public abstract class MetadataPageViewModel<MetadataType> : MetadataPageViewModelBase, IDisposable where MetadataType : MetadataChangeObject
    {
        #region types

        private struct Properties
        {
            public MetadataType data;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public MetadataType Data
        {
            get
            {
                return this.properties.data;
            }
            set
            {
                SetProperty(ref this.properties.data, ref value, OnDataChanged);
            }
        }

        #endregion

        #region constructor

        public MetadataPageViewModel(MetadataType data)
        {
            SyncDefaultMetadata(data);
            SyncLiteMetadata();
            SyncFullMetadata();
        }

        #endregion

        #region non-public methods

        protected abstract void SetDefaultMetadata(MetadataType data);
        protected abstract void SetLiteMetadata();
        protected abstract void SetFullMetadata();

        private void OnDataChanged(MetadataType oldValue, MetadataType newValue)
        {
            if (newValue != null)
            {
                newValue.MetadataChanged += Data_MetadataLevelChanged;
            }

            if (oldValue != null)
            {
                oldValue.MetadataChanged -= Data_MetadataLevelChanged;
            }
        }

        private void Data_MetadataLevelChanged(object sender, MetadataChangedEventArgs e)
        {
            // need to reset properties for ones that are null and have updated to a reference
            // properties that just change their values dont since they are already referred to
            if ((e.OldMetadataLevel < MetadataChangeObject.MetadataLevels.Lite))
            {
                SyncLiteMetadata();
            }

            if ((e.OldMetadataLevel < MetadataChangeObject.MetadataLevels.Full))
            {
                SyncFullMetadata();
            }
        }

        private void SyncDefaultMetadata(MetadataType data)
        {
            SetDefaultMetadata(data);
        }

        private void SyncLiteMetadata()
        {
            if (this.Data.MetadataLevel > MetadataChangeObject.MetadataLevels.Default)
            {
                lock (this.mutex)
                {
                    if (this.Data.MetadataLevel > MetadataChangeObject.MetadataLevels.Default)
                    {
                        SetLiteMetadata();
                    }
                }
            }
        }

        private void SyncFullMetadata()
        {
            if (this.Data.MetadataLevel == MetadataChangeObject.MetadataLevels.Full)
            {
                lock (this.mutex)
                {
                    if (this.Data.MetadataLevel == MetadataChangeObject.MetadataLevels.Full)
                    {
                        SetFullMetadata();
                    }
                }
            }
        }

        #endregion

        #region public methods

        public void Dispose()
        {
            this.Data = null;
        }

        #endregion
    }
}