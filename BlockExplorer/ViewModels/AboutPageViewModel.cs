﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;

namespace BlockExplorer.ViewModels
{
    public class AboutPageViewModel : PropertyChangeObject, IDisposable
    {
        #region types

        private struct Properties
        {
            public GoToDonatePageCommand goToDonatePageCommand;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public GoToDonatePageCommand GoToDonatePageCommand
        {
            get
            {
                return this.properties.goToDonatePageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToDonatePageCommand, ref value);
            }
        }

        #endregion

        #region constructor

        public AboutPageViewModel()
        {
            this.GoToDonatePageCommand = new GoToDonatePageCommand();
        }

        #endregion

        #region non-public methods


        #endregion

        #region public methods

        public void Dispose()
        {
        }

        #endregion
    }
}