﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels
{
    public class TransactionPageViewModel : MetadataPageViewModel<ExplorerTransaction>
    {
        #region types

        private struct Properties
        {
            public ViewModelCollection<Transfer, TransferItemViewModel> spentTransferViewModels;
            public ViewModelCollection<Transfer, TransferItemViewModel> receivedTransferViewModels;
            public GoToBlockPageCommand goToBlockPageCommand;
            public GoToBlockPageCommandParameter goToBlockPageCommandParameter;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        private Dictionary<DataErrorTypes, string> errorMessagesDictionary = new Dictionary<DataErrorTypes, string>();

        #endregion

        #region properties

        protected override Dictionary<DataErrorTypes, string> ErrorMessageDictionary
        { 
            get
            {
                return this.errorMessagesDictionary;
            }
        }

        public ViewModelCollection<Transfer, TransferItemViewModel> SpentTransferViewModels
        {
            get
            {
                return this.properties.spentTransferViewModels;
            }
            set
            {
                SetProperty(ref this.properties.spentTransferViewModels, ref value);
            }
        }

        public ViewModelCollection<Transfer, TransferItemViewModel> ReceivedTransferViewModels
        {
            get
            {
                return this.properties.receivedTransferViewModels;
            }
            set
            {
                SetProperty(ref this.properties.receivedTransferViewModels, ref value);
            }
        }

        public GoToBlockPageCommand GoToBlockPageCommand
        {
            get
            {
                return this.properties.goToBlockPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToBlockPageCommand, ref value);
            }
        }

        public GoToBlockPageCommandParameter GoToBlockPageCommandParameter
        {
            get
            {
                return this.properties.goToBlockPageCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.goToBlockPageCommandParameter, ref value);
            }
        }

        #endregion

        #region constructor

        public TransactionPageViewModel(ExplorerTransaction transaction) : base(transaction)
        {
            this.errorMessagesDictionary[DataErrorTypes.None] = "";
            this.errorMessagesDictionary[DataErrorTypes.LiteMetadataResponseFailed] = "No data found for this transaction Id";
            this.errorMessagesDictionary[DataErrorTypes.NetworkRequestFailed] = "Could not get a response for requested data";
        }

        #endregion

        #region non-public methods

        protected override void SetDefaultMetadata(ExplorerTransaction data)
        {
            this.Data = data;

            this.GoToBlockPageCommand = new GoToBlockPageCommand();
            this.GoToBlockPageCommandParameter = new GoToBlockPageCommandParameter()
            {
                Data = this.Data?.Block,
                Command = this.GoToBlockPageCommand,
            };
        }

        protected override void SetLiteMetadata()
        {
            if (this.GoToBlockPageCommandParameter.Data == null)
            {
                this.GoToBlockPageCommandParameter.Data = this.Data.Block;
            }

            this.ReceivedTransferViewModels = new ViewModelCollection<Transfer, TransferItemViewModel>(this.Data.Received, (transfer => new TransferItemViewModel(transfer)));
            this.SpentTransferViewModels = new ViewModelCollection<Transfer, TransferItemViewModel>(this.Data.Spent, (transfer => new TransferItemViewModel(transfer)));
        }

        protected override void SetFullMetadata()
        {

        }

        #endregion

        #region public methods

        #endregion
    }
}