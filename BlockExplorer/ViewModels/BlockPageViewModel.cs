﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels
{
    public class BlockPageViewModel : MetadataPageViewModel<ExplorerBlock>
    {
        #region types

        private struct Properties
        {
            public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> blockTransactionViewModels;
            public GoToBlockPageCommand goToBlockPageCommand;
            public GoToBlockPageCommandParameter goToBlockPageCommandParameter;
        }

        #endregion

        #region data
         
        private Properties properties = new Properties();
        private Dictionary<DataErrorTypes, string> errorMessagesDictionary = new Dictionary<DataErrorTypes, string>();

        #endregion

        #region properties      

        protected override Dictionary<DataErrorTypes, string> ErrorMessageDictionary
        {
            get
            {
                return this.errorMessagesDictionary;
            }
        }

        public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> BlockTransactionViewModels
        {
            get
            {
                return this.properties.blockTransactionViewModels;
            }
            set
            {
                SetProperty(ref this.properties.blockTransactionViewModels, ref value);
            }
        }

        public GoToBlockPageCommand GoToBlockPageCommand
        {
            get
            {
                return this.properties.goToBlockPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToBlockPageCommand, ref value);
            }
        }

        public GoToBlockPageCommandParameter GoToBlockPageCommandParameter
        {
            get
            {
                return this.properties.goToBlockPageCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.goToBlockPageCommandParameter, ref value);
            }
        }

        #endregion

        #region constructor

        public BlockPageViewModel(ExplorerBlock block) : base(block)
        {
            this.errorMessagesDictionary[DataErrorTypes.None] = "";
            this.errorMessagesDictionary[DataErrorTypes.LiteMetadataResponseFailed] = "No data found for this block Id";
            this.errorMessagesDictionary[DataErrorTypes.NetworkRequestFailed] = "Could not get a response for requested data";
        }

        #endregion

        #region non-public methods

        protected override void SetDefaultMetadata(ExplorerBlock data)
        {
            this.Data = data;

            this.GoToBlockPageCommand = new GoToBlockPageCommand();
            this.GoToBlockPageCommandParameter = new GoToBlockPageCommandParameter()
            {
                Command = this.GoToBlockPageCommand,
                //Data = this?.Block?.PreviousBlock,
            };
        }

        protected override void SetLiteMetadata()
        {
            this.GoToBlockPageCommandParameter.Data = this.Data.PreviousBlock;
        }

        protected override void SetFullMetadata()
        {
            this.BlockTransactionViewModels = new ViewModelCollection<ExplorerTransaction, TransactionItemViewModel>(this.Data.Transactions, (transaction => new TransactionItemViewModel(transaction)));
        } 

        #endregion

        #region public methods
        #endregion
    }
}