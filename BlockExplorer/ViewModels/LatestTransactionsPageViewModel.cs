﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels
{
    public class LatestTransactionsPageViewModel : DynamicPageViewModel<ExplorerLatestTransactions>
    {
        #region types

        private struct Properties
        {
            public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> transactionViewModels;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        private Dictionary<DataErrorTypes, string> errorMessagesDictionary = new Dictionary<DataErrorTypes, string>();

        #endregion

        #region properties      

        protected override Dictionary<DataErrorTypes, string> ErrorMessageDictionary
        {
            get
            {
                return this.errorMessagesDictionary;
            }
        }

        public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> TransactionViewModels
        {
            get
            {
                return this.properties.transactionViewModels;
            }
            set
            {
                SetProperty(ref this.properties.transactionViewModels, ref value);
            }
        }

        #endregion

        #region constructor

        public LatestTransactionsPageViewModel(ExplorerLatestTransactions transactions) : base(transactions)
        {
            this.errorMessagesDictionary[DataErrorTypes.None] = "";
            this.errorMessagesDictionary[DataErrorTypes.NetworkRequestFailed] = "Could not get a response for requested data";
        }

        #endregion

        #region non-public methods

        protected override void SynchronizeModelToData(ExplorerLatestTransactions data)
        {
            this.Data = data;

            if (this.Data.Transactions != null)
            {
                this.TransactionViewModels = new ViewModelCollection<ExplorerTransaction, TransactionItemViewModel>(this.Data.Transactions, (transaction => new TransactionItemViewModel(transaction)));
            }

            this.Data.PropertyChanged += Data_PropertyChanged;
        }

        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ExplorerLatestTransactions.Transactions))
            {
                if (this.Data?.Transactions != null)
                {
                    this.TransactionViewModels.Source = this.Data.Transactions;
                }
            }
        }

        #endregion

        #region public methods
        #endregion
    }
}