﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.Services;

namespace BlockExplorer.ViewModels
{
    public class MainPageViewModel : PropertyChangeObject, IDisposable
    {
        #region types

        private struct Properties
        {
            public GoToAboutPageCommand goToAboutPageCommand;
            public GoToLatestTransactionsPageCommand goToLatestTransactionsPageCommand;
            public GoToQRAddressBuilderPageCommand goToQRAddressBuilderPageCommand;
            public GoToStatsPageCommand goToStatsPageCommand;
            public MetadataObjectCollection<ExplorerAddress> storedExplorerAddresses;
            public ViewModelCollection<ExplorerAddress, AddressItemViewModel> addressViewModels;           
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        //private Dictionary<MetadataPageViewModelBase.DataErrorTypes, string> errorMessagesDictionary = new Dictionary<MetadataPageViewModelBase.DataErrorTypes, string>();

        #endregion

        #region properties

        public GoToAboutPageCommand GoToAboutPageCommand
        {
            get
            {
                return this.properties.goToAboutPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToAboutPageCommand, ref value);
            }
        }

        public GoToLatestTransactionsPageCommand GoToLatestTransactionsPageCommand
        {
            get
            {
                return this.properties.goToLatestTransactionsPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToLatestTransactionsPageCommand, ref value);
            }
        }

        public GoToQRAddressBuilderPageCommand GoToQRAddressBuilderPageCommand
        {
            get
            {
                return this.properties.goToQRAddressBuilderPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToQRAddressBuilderPageCommand, ref value);
            }
        }

        public GoToStatsPageCommand GoToStatsPageCommand
        {
            get
            {
                return this.properties.goToStatsPageCommand;
            }
            set
            {
                SetProperty(ref this.properties.goToStatsPageCommand, ref value);
            }
        }

        public MetadataObjectCollection<ExplorerAddress> StoredExplorerAddresses
        {
            get
            {
                return this.properties.storedExplorerAddresses;
            }
            set
            {
                SetProperty(ref this.properties.storedExplorerAddresses, ref value);
            }
        }

        public ViewModelCollection<ExplorerAddress, AddressItemViewModel> AddressViewModels
        {
            get
            {
                return this.properties.addressViewModels;
            }
            set
            {
                SetProperty(ref this.properties.addressViewModels, ref value);
            }
        }

        #endregion

        #region constructor

        public MainPageViewModel() 
        {
            this.GoToAboutPageCommand = new GoToAboutPageCommand();
            this.GoToLatestTransactionsPageCommand = new GoToLatestTransactionsPageCommand();
            this.GoToQRAddressBuilderPageCommand = new GoToQRAddressBuilderPageCommand();
            this.GoToStatsPageCommand = new GoToStatsPageCommand();
            
            this.StoredExplorerAddresses = CollectionsService.Instance.StoredExplorerAddresses;

            this.AddressViewModels = new ViewModelCollection<ExplorerAddress, AddressItemViewModel>(this.StoredExplorerAddresses.Collection, (address => new AddressItemViewModel(address)));
        }

        #endregion

        #region non-public methods

        #endregion

        #region public methods

        public void Dispose()
        {
        }

        #endregion
    }
}