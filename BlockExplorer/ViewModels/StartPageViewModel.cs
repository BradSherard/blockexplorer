﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data.Core;

namespace BlockExplorer.ViewModels
{
    public class StartPageViewModel : PropertyChangeObject, IDisposable
    {
        #region types

        private struct Properties
        {
            public string somePageValue;
        }

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public string SomePageValue
        {
            get
            {
                return this.properties.somePageValue;
            }
            set
            {
                SetProperty(ref this.properties.somePageValue, ref value);
            }
        }
       
        #endregion

        #region constructor

        public StartPageViewModel()
        {
        }

        #endregion

        #region non-public methods

       
        #endregion

        #region public methods

        public void Dispose()
        {
        }

        #endregion
    }
}