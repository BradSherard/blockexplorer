﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data;
using BlockExplorer.Data.Core;
using BlockExplorer.Commands;
using BlockExplorer.Commands.Parameters;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.ViewModels
{
    public class AddressPageViewModel : MetadataPageViewModel<ExplorerAddress>
    {
        #region types

        private struct Properties
        {
            public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> transactionViewModels;
            public ScanAddressCommand scanAddressCommand;
            public AddressDataAndViewModelCommandParameter scanAddressCommandParameter;
            public CancelMetadataUpdateCommand cancelMetadataUpdateCommand;
            public CancelMetadataUpdateCommandParameter cancelMetadataUpdateCommandParameter;
            public SaveAddressCommand saveAddressCommand;
            public SaveDatabasePropertyChangeObjectCommandParameter saveAddressCommandParameter;
            public UnsaveAddressCommand unsaveAddressCommand;
            public SaveDatabasePropertyChangeObjectCommandParameter unsaveAddressCommandParameter;
        }

        #endregion

        #region data

        private Properties properties = new Properties();
        private Dictionary<DataErrorTypes, string> errorMessagesDictionary = new Dictionary<DataErrorTypes, string>();

        #endregion

        #region properties      

        protected override Dictionary<DataErrorTypes, string> ErrorMessageDictionary
        {
            get
            {
                return this.errorMessagesDictionary;
            }
        }

        public ViewModelCollection<ExplorerTransaction, TransactionItemViewModel> TransactionViewModels
        {
            get
            {
                return this.properties.transactionViewModels;
            }
            set
            {
                SetProperty(ref this.properties.transactionViewModels, ref value);
            }
        }

        public ScanAddressCommand ScanAddressCommand
        {
            get
            {
                return this.properties.scanAddressCommand;
            }
            set
            {
                SetProperty(ref this.properties.scanAddressCommand, ref value);
            }
        }

        public AddressDataAndViewModelCommandParameter ScanAddressCommandParameter
        {
            get
            {
                return this.properties.scanAddressCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.scanAddressCommandParameter, ref value);
            }
        }

        public CancelMetadataUpdateCommand CancelMetadataUpdateCommand
        {
            get
            {
                return this.properties.cancelMetadataUpdateCommand;
            }
            set
            {
                SetProperty(ref this.properties.cancelMetadataUpdateCommand, ref value);
            }
        }

        public CancelMetadataUpdateCommandParameter CancelMetadataUpdateCommandParameter
        {
            get
            {
                return this.properties.cancelMetadataUpdateCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.cancelMetadataUpdateCommandParameter, ref value);
            }
        }

        public SaveAddressCommand SaveAddressCommand
        {
            get
            {
                return this.properties.saveAddressCommand;
            }
            set
            {
                SetProperty(ref this.properties.saveAddressCommand, ref value);
            }
        }

        public SaveDatabasePropertyChangeObjectCommandParameter SaveAddressCommandParameter
        {
            get
            {
                return this.properties.saveAddressCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.saveAddressCommandParameter, ref value);
            }
        }

        public UnsaveAddressCommand UnsaveAddressCommand
        {
            get
            {
                return this.properties.unsaveAddressCommand;
            }
            set
            {
                SetProperty(ref this.properties.unsaveAddressCommand, ref value);
            }
        }

        public SaveDatabasePropertyChangeObjectCommandParameter UnsaveAddressCommandParameter
        {
            get
            {
                return this.properties.unsaveAddressCommandParameter;
            }
            set
            {
                SetProperty(ref this.properties.unsaveAddressCommandParameter, ref value);
            }
        }

        #endregion

        #region constructor

        public AddressPageViewModel(ExplorerAddress address) : base(address)
        {
            this.errorMessagesDictionary[DataErrorTypes.None] = "";
            this.errorMessagesDictionary[DataErrorTypes.LiteMetadataResponseFailed] = "No data found for this address Id";
            this.errorMessagesDictionary[DataErrorTypes.FullMetadataResponseFailed] = "Block scan interrupted";
            this.errorMessagesDictionary[DataErrorTypes.NetworkRequestFailed] = "Could not get a response for requested data";
        }

        #endregion

        #region non-public methods

        protected override void SetDefaultMetadata(ExplorerAddress data)
        {
            this.Data = data;

            this.ScanAddressCommand = new ScanAddressCommand();
            this.ScanAddressCommandParameter = new AddressDataAndViewModelCommandParameter()
            {
                Address = this.Data,
                Command = this.ScanAddressCommand,
            };

            this.CancelMetadataUpdateCommand = new CancelMetadataUpdateCommand();
            this.CancelMetadataUpdateCommandParameter = new CancelMetadataUpdateCommandParameter()
            {
                Data = this.Data,
                Command = this.CancelMetadataUpdateCommand,
            };

            this.SaveAddressCommand = new SaveAddressCommand();
            this.SaveAddressCommandParameter = new SaveDatabasePropertyChangeObjectCommandParameter()
            {
                Data = this.Data,
                Command = this.SaveAddressCommand,
            };

            this.UnsaveAddressCommand = new UnsaveAddressCommand();
            this.UnsaveAddressCommandParameter = new SaveDatabasePropertyChangeObjectCommandParameter()
            {
                Data = this.Data,
                Command = this.UnsaveAddressCommand,
            };
        }

        protected override void SetLiteMetadata()
        {
            if (this.ScanAddressCommandParameter.ViewModel == null)
            {
                this.ScanAddressCommandParameter.ViewModel = this;
                this.ScanAddressCommandParameter.Address = this.Data;
            }

            if (this.CancelMetadataUpdateCommandParameter.Data == null)
            {
                this.CancelMetadataUpdateCommandParameter.Data = this.Data;
            }

            if (this.SaveAddressCommandParameter.ViewModel == null)
            {
                this.SaveAddressCommandParameter.ViewModel = this;
                this.SaveAddressCommandParameter.Data = this.Data;
            }

            if (this.UnsaveAddressCommandParameter.ViewModel == null)
            {
                this.UnsaveAddressCommandParameter.ViewModel = this;
                this.UnsaveAddressCommandParameter.Data = this.Data;
            }
        }

        protected override void SetFullMetadata()
        {

        }

        private void OnAddressChanged(ExplorerAddress oldValue, ExplorerAddress newValue)
        {
            //this.GoToBlockPageCommandParameter.Data = newValue?.PreviousBlock;

            if (newValue != null)
            {
                newValue.MetadataChanged += Address_MetadataChanged;
            }

            if (oldValue != null)
            {
                oldValue.MetadataChanged -= Address_MetadataChanged;
            }
        }

        private void Address_MetadataChanged(object sender, MetadataChangedEventArgs e)
        {
            //if (this?.Address?.PreviousBlock != null)
            //{
            //    this.GoToBlockPageCommandParameter.Data = this.Address.PreviousBlock;
            //}
        }

        #endregion

        #region public methods

        #endregion
    }
}