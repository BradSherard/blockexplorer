﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using BlockExplorer.Data.Core;

namespace BlockExplorer.ViewModels
{
    public class DonatePageViewModel : PropertyChangeObject, IDisposable
    {
        #region types

        private struct Properties
        {
            public string address;
        }

        #endregion

        #region constants

        private const string address = "bitcoin:1Pd62XQSNEiYzgDDNrS5zt13cbCqyhPmEi";

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public string Address
        {
            get
            {
                return this.properties.address;
            }
            set
            {
                SetProperty(ref this.properties.address, ref value);
            }
        }

        #endregion

        #region constructor

        public DonatePageViewModel()
        {
            this.Address = DonatePageViewModel.address;
        }

        #endregion

        #region non-public methods


        #endregion

        #region public methods

        public void Dispose()
        {
        }

        #endregion
    }
}