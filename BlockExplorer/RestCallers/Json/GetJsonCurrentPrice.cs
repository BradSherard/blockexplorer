﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.JSON;
using BlockExplorer.ValueConverters;
using BlockExplorer.Services;
using System.Threading;

namespace BlockExplorer.RestCallers.Json
{
    public class GetJsonCurrentPrice
    {
        #region constants

        private const string scheme = "https";
        private const string host = "api.coindesk.com";
        private const string path = "v1/bpi/currentprice/{0}.json";

        #endregion

        #region types

        public enum CurrencyCodes
        {
            USD,
            CNY,
        }

        #endregion

        #region data

        #endregion

        #region properties

        #endregion

        #region constructor

        #endregion

        #region public methods

        //http(s)://api.coindesk.com/v1/bpi/currentprice/<CODE>.json
        public async Task<RestCaller.CallerResult<JsonCurrentPrice>> GetBitcoinPriceAsync(CurrencyCodes currencyCode, CancellationToken cancellationToken = new CancellationToken())
        {
            UriBuilder uriBuilder = new UriBuilder()
            {
                Scheme = GetJsonCurrentPrice.scheme,
                Host = GetJsonCurrentPrice.host,
                Path = String.Format(GetJsonCurrentPrice.path, currencyCode.ToString()),
            };

            RestCaller.CallerResult<JsonCurrentPrice> result = await AppServices.Instance.RestCaller.CallGetJsonObjectAsync<JsonCurrentPrice>(uriBuilder.Uri, cancellationToken, new CurrentPriceJSONConverter(currencyCode.ToString()));

            return result;
        }

        #endregion

        #region non-public methods

        #endregion

    }
}
