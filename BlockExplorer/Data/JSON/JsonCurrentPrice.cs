﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BlockExplorer.Data.JSON
{
    /// <summary>
    /// //http(s)://api.coindesk.com/v1/bpi/currentprice/<CODE>.json
    /// </summary>
    public class JsonCurrentPrice
    {
        #region types

        public struct Time
        {
            //[JsonProperty("updated", ItemConverterType = typeof(DateTimeConverterBase))]
            //public DateTime Updated { get; set; }

            [JsonProperty("updatedISO", ItemConverterType = typeof(IsoDateTimeConverter), Order = 2)]
            public DateTime UpdatedISO { get; set; }

            //[JsonProperty("updateduk", ItemConverterType = typeof(DateTimeConverterBase))]
            //public DateTime UpdatedUK { get; set; }
        }

        public struct BitcoinPriceIndex
        {
            [JsonProperty("USD", Order = 1)]
            public Currency USDCurrency { get; set; }

            [JsonProperty(Order = 2)]
            public Currency OtherCurrency { get; set; }
        }

        public struct Currency
        {
            [JsonProperty("code", Order = 1)]
            public string Code { get; set; }

            [JsonProperty("rate", Order = 2)]
            public string Rate { get; set; }

            [JsonProperty("description", Order = 3)]
            public string Description { get; set; }

            [JsonProperty("rate_float", Order = 4)]
            public float Rate_Float { get; set; }
        }

        #endregion

        #region properties

        [JsonProperty("time")]
        public Time UpdatedTime { get; set; }

        [JsonProperty("disclaimer")]
        public string Disclaimer { get; set; }

        [JsonProperty("bpi")]
        public BitcoinPriceIndex Price { get; set; }

        #endregion

        #region constructors

        #endregion

        //{
        //        "time":
        //        {
        //            "updated":"Sep 18, 2013 17:27:00 UTC",
        //            "updatedISO":"2013-09-18T17:27:00+00:00"
        //        },
        //        "disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index. Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
        //        "bpi":
        //        {
        //            "USD":
        //            {
        //                "code":"USD",
        //                "rate":"126.5235",
        //                "description":"United States Dollar",
        //                "rate_float":126.5235
        //            },
        //            "CNY":
        //            {
        //                "code":"CNY",
        //                "rate":"775.0665",
        //                "description":"Chinese Yuan",
        //                "rate_float":"775.0665"
        //            }
        //        }
        //    }


    }
}
