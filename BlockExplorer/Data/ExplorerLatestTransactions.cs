﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using System.Threading;
using BlockExplorer.Services;
using QBitNinja.Client.Models;
using NBitcoin;
using MoreLinq;

namespace BlockExplorer.Data
{
    public class ExplorerLatestTransactions : DynamicChangeObject
    {
        #region types

        private struct Properties
        {
            public bool isUpdating;
            public DynamicPageViewModelBase.DataErrorTypes errorType;
            public uint256 latestBlockId;
            public ThreadSafeObservableCollection<ExplorerTransaction> transactions;
        }

        #endregion

        #region constants

        public static readonly TimeSpan Default_Update_Period = new TimeSpan(0, 0, 3, 0);

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public override DynamicPageViewModelBase.DataErrorTypes ErrorType
        {
            get
            {
                return this.properties.errorType;
            }
            protected set
            {
                SetProperty(ref this.properties.errorType, ref value);
            }
        }

        public override bool IsUpdating
        {
            get
            {
                return this.properties.isUpdating;
            }
            protected set
            {
                SetIsUpdating(ref this.properties.isUpdating, ref value);
            }
        }

        public uint256 LatestBlockId
        {
            get
            {
                return this.properties.latestBlockId;
            }
            protected set
            {
                SetProperty(ref this.properties.latestBlockId, ref value);
            }
        }

        public ThreadSafeObservableCollection<ExplorerTransaction> Transactions
        {
            get
            {
                return this.properties.transactions;
            }
            protected set
            {
                SetProperty(ref this.properties.transactions, ref value);
            }
        }

        #endregion

        #region constructor

        public ExplorerLatestTransactions(TimeSpan updatePeriod) : base(updatePeriod)
        {
            this.Transactions = new ThreadSafeObservableCollection<ExplorerTransaction>();
        }

        #endregion

        protected override async Task<DynamicResponse> UpdateAsync(CancellationToken cancellationToken)
        {
            DynamicResponse response = new DynamicResponse()
            {
                ErrorType = DynamicPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            GetBlockResponse blockResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetBlock(new BlockFeature(SpecialFeature.Last), true);
       
            System.Diagnostics.Debug.WriteLine(blockResponse.AdditionalInformation.BlockId.ToString());

            // brads todo: figure out if there is a way to get real time latest transactions. It seems like latest block is confirmed, not in the process of filling up.
            if (!blockResponse.AdditionalInformation.BlockId.Equals(this.LatestBlockId))
            {
                blockResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetBlock(new BlockFeature(SpecialFeature.Last), false);             

                this.Transactions.Clear();

                this.LatestBlockId = blockResponse.AdditionalInformation.BlockId;

                foreach (var transactions in blockResponse.Block.Transactions.Skip(this.Transactions.Count).Batch(200))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        response.ErrorType = DynamicPageViewModelBase.DataErrorTypes.Cancelled;
                        break;
                    }

                    LinkedList<ExplorerTransaction> explorerTransactions = new LinkedList<ExplorerTransaction>();

                    foreach (Transaction transaction in transactions)
                    {
                        ExplorerTransaction explorerTransaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(transaction.GetHash());
                        await explorerTransaction.ChangeMetadata_TotalSpent(transaction.TotalOut.Satoshi);
                        // brads todo: chain cancellations from different objects
                        // also update error logic to be a function of data, not viewmodel for page

                        explorerTransactions.AddLast(explorerTransaction);
                    }

                    this.Transactions.AddItems(explorerTransactions);
                }
            }

            return response;
        } 
    }
}
