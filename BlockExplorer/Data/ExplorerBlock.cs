﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NBitcoin;
using QBitNinja.Client.Models;
using BlockExplorer.Services;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels.Core;
using MoreLinq;

namespace BlockExplorer.Data
{
    public class ExplorerBlock : MetadataChangeObject
    {
        #region types

        private struct Properties
        {
            public uint256 blockId;
            public MetadataLevels metadataLevel;
            public bool isUpdatingMetadata;
            public DateTime? timeStamp;
            public int? confirmations;
            public int? height;
            public uint256 bits;
            public double? difficulty;
            public uint256 merkleRoot;
            public int? version;
            public ExplorerBlock previousBlock;
            public ThreadSafeObservableCollection<ExplorerTransaction> transactions;
        }

        #endregion

        #region constants

        public static readonly string[] DatabasePropertyNames = new string[]
        {
            nameof(ExplorerBlock.BlockId),
        };

        #endregion

        #region static data

        protected static References<ExplorerBlock, uint256> references = new References<ExplorerBlock, uint256>(block => block.BlockId);

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        [IsMetadataProperty(MetadataLevels.Lite)]
        public uint256 BlockId
        {
            get
            {
                return this.properties.blockId;
            }
            set
            {
                SetProperty(ref this.properties.blockId, ref value, OnBlockIdChanged);
            }
        }

        public override MetadataLevels MetadataLevel
        {
            get
            {
                return this.properties.metadataLevel;
            }
            set
            {
                SetMetadata(ref this.properties.metadataLevel, ref value);
            }
        }

        /// <summary>
        /// only change this in base
        /// </summary>
        public override bool IsUpdatingMetadata
        {
            get
            {
                return this.properties.isUpdatingMetadata;
            }
            protected set
            {
                SetProperty(ref this.properties.isUpdatingMetadata, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public DateTime? TimeStamp
        {
            get
            {
                return this.properties.timeStamp;
            }
            set
            {
                SetProperty(ref this.properties.timeStamp, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public int? Height
        {
            get
            {
                return this.properties.height;
            }
            set
            {
                SetProperty(ref this.properties.height, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public double? Difficulty
        {
            get
            {
                return this.properties.difficulty;
            }
            set
            {
                SetProperty(ref this.properties.difficulty, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public uint256 Bits
        {
            get
            {
                return this.properties.bits;
            }
            set
            {
                SetProperty(ref this.properties.bits, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public int? Confirmations
        {
            get
            {
                return this.properties.confirmations;
            }
            set
            {
                SetProperty(ref this.properties.confirmations, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public uint256 MerkleRoot
        {
            get
            {
                return this.properties.merkleRoot;
            }
            set
            {
                SetProperty(ref this.properties.merkleRoot, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public int? Version
        {
            get
            {
                return this.properties.version;
            }
            set
            {
                SetProperty(ref this.properties.version, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public ExplorerBlock PreviousBlock
        {
            get
            {
                return this.properties.previousBlock;
            }
            set
            {
                SetProperty(ref this.properties.previousBlock, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Full)]
        public ThreadSafeObservableCollection<ExplorerTransaction> Transactions
        {
            get
            {
                return this.properties.transactions;
            }
            set
            {
                SetProperty(ref this.properties.transactions, ref value);
            }
        }

        #endregion

        #region constructors

        private ExplorerBlock()
            : base(ExplorerBlock.DatabasePropertyNames)
        {
            this.MetadataLevel = MetadataLevels.Default;
        }

        private ExplorerBlock(uint256 blockId)
            : this()
        {
            this.BlockId = blockId;
        }

        #endregion

        #region public methods 

        public override bool CanBeRemovedFromDatabase()
        {
            return true;
        }

        public override MetadataLevels DetermineMetadataLevelRestoredFromDatabase()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region public static methods

        public static ExplorerBlock ExplorerBlockFromBlockId(uint256 blockId)
        {
            ExplorerBlock block = null;

            if (blockId != null)
            {
                lock (ExplorerBlock.references)
                {
                    block = ExplorerBlock.references.GetItem(blockId);

                    if (block == null)
                    {
                        if (block == null)
                        {
                            block = new ExplorerBlock(blockId);
                        }

                        ExplorerBlock.references.Add(block);
                    }
                }
            }

            return block;
        }

        #endregion

        #region non-public methods

        protected override async Task<MetadataResponse> GetLiteMetadataAsync(CancellationToken cancellationToken)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            GetBlockResponse blockResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetBlock(new BlockFeature(this.BlockId), true);
            Network network = AppServices.Instance.BitcoinExplorer.ExplorerClient.Network;
            
            if (blockResponse != null)
            {
                //response.RawResponseObject = blockResponse; don't bother sending it, it won't be used without full data in response

                this.Height = blockResponse.AdditionalInformation.Height;
                this.Confirmations = blockResponse.AdditionalInformation.Confirmations;
                this.TimeStamp = blockResponse.AdditionalInformation.BlockTime.UtcDateTime;
                this.Bits = blockResponse.AdditionalInformation.BlockHeader.Bits.ToUInt256();
                this.Difficulty = blockResponse.AdditionalInformation.BlockHeader.Bits.Difficulty;
                this.PreviousBlock = ExplorerBlock.ExplorerBlockFromBlockId(blockResponse.AdditionalInformation.BlockHeader.HashPrevBlock);
                this.Version = blockResponse.AdditionalInformation.BlockHeader.Version;
                this.MerkleRoot = blockResponse.AdditionalInformation.BlockHeader.HashMerkleRoot;
            }
            else
            {
                response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.LiteMetadataResponseFailed;
            }

            return response;
        }

        protected override async Task<MetadataResponse> GetFullMetadataAsync(CancellationToken cancellationToken, object rawResponseObject)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            Network network = AppServices.Instance.BitcoinExplorer.ExplorerClient.Network;
            GetBlockResponse blockResponse = rawResponseObject as GetBlockResponse;

            if (blockResponse?.Block == null)
            {
                blockResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetBlock(new BlockFeature(this.BlockId), false);
            }

            if (blockResponse?.Block != null)
            {
                this.Transactions = new ThreadSafeObservableCollection<ExplorerTransaction>();

                foreach (var transactions in blockResponse.Block.Transactions.Batch(200))
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.FullMetadataResponseFailed;
                        break;
                    }

                    LinkedList<ExplorerTransaction> explorerTransactions = new LinkedList<ExplorerTransaction>();

                    foreach (Transaction transaction in transactions)
                    {
                        ExplorerTransaction explorerTransaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(transaction.GetHash());
                        response.ErrorType = await explorerTransaction.ChangeMetadata_TotalSpent(transaction.TotalOut.Satoshi);

                        explorerTransactions.AddLast(explorerTransaction);
                    }

                    if (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None)
                    {
                        this.Transactions.AddItems(explorerTransactions);
                    }
                }
            }
            else
            {
                response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.FullMetadataResponseFailed;
            }

            return response;
        }

        private void OnBlockIdChanged(uint256 oldValue, uint256 newValue)
        {

        }

        protected override void OnAssociatedObjectIsMarkedForStorageChanged(object databasePropertyChangeObject, EventArgs eventArgs)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
