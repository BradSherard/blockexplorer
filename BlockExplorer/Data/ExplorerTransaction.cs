﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NBitcoin;
using QBitNinja.Client.Models;
using BlockExplorer.Services;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.Data.SQL;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;

namespace BlockExplorer.Data
{
    public class ExplorerTransaction : MetadataChangeObject
    {
        #region types

        private struct Properties
        {
            public uint256 transactionId;
            public MetadataLevels metadataLevel;
            public bool isUpdatingMetadata;
            public long? fee;
            public long? totalSpent;
            public DateTime? firstSeen;
            public ExplorerBlock block;
            public ThreadSafeObservableCollection<Transfer> spent;
            public ThreadSafeObservableCollection<Transfer> received;
        }

        #endregion

        #region constants

        public static readonly string[] DatabasePropertyNames = new string[]
        {
            nameof(ExplorerTransaction.TransactionId),
            nameof(ExplorerTransaction.Fee),
            nameof(ExplorerTransaction.TotalSpent),
            nameof(ExplorerTransaction.FirstSeen),
            nameof(ExplorerTransaction.BlockId),
            nameof(ExplorerTransaction.SpentIds),
            nameof(ExplorerTransaction.ReceivedIds),
        };

        #endregion

        #region static data

        protected static References<ExplorerTransaction, uint256> references = new References<ExplorerTransaction, uint256>(transaction => transaction.TransactionId);

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        [IsMetadataProperty(MetadataLevels.Lite)]
        public uint256 TransactionId
        {
            get
            {
                return this.properties.transactionId;
            }
            set
            {
                SetProperty(ref this.properties.transactionId, ref value, OnTransactionIdChanged);
            }
        }

        public override MetadataLevels MetadataLevel
        {
            get
            {
                return this.properties.metadataLevel;
            }
            set
            {
                SetMetadata(ref this.properties.metadataLevel, ref value);
            }
        }

        /// <summary>
        /// only change this in base
        /// </summary>
        public override bool IsUpdatingMetadata
        {
            get
            {
                return this.properties.isUpdatingMetadata;
            }
            protected set
            {
                SetProperty(ref this.properties.isUpdatingMetadata, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public long? Fee
        {
            get
            {
                return this.properties.fee;
            }
            set
            {
                SetProperty(ref this.properties.fee, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public long? TotalSpent
        {
            get
            {
                return this.properties.totalSpent;
            }
            set
            {
                SetProperty(ref this.properties.totalSpent, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public DateTime? FirstSeen
        {
            get
            {
                return this.properties.firstSeen;
            }
            set
            {
                SetProperty(ref this.properties.firstSeen, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public ExplorerBlock Block
        {
            get
            {
                return this.properties.block;
            }
            set
            {
                SetProperty(ref this.properties.block, ref value, OnBlockChanged);
            }
        }

        public string BlockId { get; private set; }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public ThreadSafeObservableCollection<Transfer> Spent
        {
            get
            {
                return this.properties.spent;
            }
            set
            {
                SetProperty(ref this.properties.spent, ref value, OnSpentChanged);
            }
        }

        public MappedDictionary<string, Transfer> SpentIds { get; private set; }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public ThreadSafeObservableCollection<Transfer> Received
        {
            get
            {
                return this.properties.received;
            }
            set
            {
                SetProperty(ref this.properties.received, ref value, OnReceivedChanged);
            }
        }

        public MappedDictionary<string, Transfer> ReceivedIds { get; private set; }

        #endregion

        #region constructors

        private ExplorerTransaction()
            : base(ExplorerTransaction.DatabasePropertyNames)
        {
            this.MetadataLevel = MetadataLevels.Default;
        }

        private ExplorerTransaction(uint256 transactionId)
            : this()
        {
            this.TransactionId = transactionId;
            this.Spent = new ThreadSafeObservableCollection<Transfer>();
            this.Received = new ThreadSafeObservableCollection<Transfer>();
        }

        #endregion

        #region public methods 

        public override bool CanBeRemovedFromDatabase()
        {
            return !this.IsMarkedForStorage && !this.Spent.Where(transfer => transfer.Address.IsMarkedForStorage).Any() && !this.Received.Where(transfer => transfer.Address.IsMarkedForStorage).Any();
        }

        public override MetadataLevels DetermineMetadataLevelRestoredFromDatabase()
        {
            MetadataLevels metadataLevel = this.MetadataLevel;

            if (metadataLevel == MetadataLevels.Lite)
            {
                if (this.Block == null)
                {
                    metadataLevel = MetadataLevels.Default;
                }
            }
            if (metadataLevel == MetadataLevels.Full)
            {
                if (this.Spent.Any(transfer => !transfer.IsAllPropertiesPopulated()) || this.Received.Any(transfer => !transfer.IsAllPropertiesPopulated()))
                {
                    metadataLevel = MetadataLevels.Lite;
                }
            }

            return metadataLevel;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> ChangeMetadata_TotalSpent(long totalSpent)
        {
            await ChangeMetadataSyncFunctionAsync(
                delegate ()
                {
                    MetadataResponse response = new MetadataResponse()
                    {
                        ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                        RawResponseObject = null,
                    };

                    this.TotalSpent = totalSpent;

                    return response;
                }
            );

            return MetadataPageViewModelBase.DataErrorTypes.None;
        }

        #region public static methods

        public static ExplorerTransaction[] ExplorerTransactionsFromSQLiteExplorerTransactions(IEnumerable<SQLiteExplorerTransaction> sqliteExplorerTransactions)
        {
            SQLiteExplorerTransaction[] sqliteExplorerTransactionsArray = sqliteExplorerTransactions.ToArray();
            uint256[] sqliteExplorerTransactionIds = sqliteExplorerTransactions.Select(explorerTransaction => uint256.Parse(explorerTransaction.TransactionId)).ToArray();
            ExplorerTransaction[] explorerTransactions;

            if (sqliteExplorerTransactionsArray.Length > 0)
            {
                Dictionary<uint256, ExplorerTransaction> explorerTransactionsByExplorerTransactionId = new Dictionary<uint256, ExplorerTransaction>();
                ExplorerTransaction[] explorerTransactionsFromReferences;

                lock (ExplorerTransaction.references)
                {
                    explorerTransactionsFromReferences = ExplorerTransaction.references.GetItems(sqliteExplorerTransactionIds).ToArray();

                    if (explorerTransactionsFromReferences.Length > 0)
                    {
                        // keep only the Transactions that weren't fetched from the references
                        sqliteExplorerTransactionsArray = sqliteExplorerTransactionsArray.Where(explorerTransaction => (explorerTransactionsFromReferences.FirstOrDefault(explorerTransactionReference => explorerTransactionReference.TransactionId.ToString() == explorerTransaction.TransactionId) == null)).ToArray();

                        // add Transactions from references
                        foreach (ExplorerTransaction explorerTransaction in explorerTransactionsFromReferences)
                        {
                            if (explorerTransaction.TransactionId != null)
                            {
                                explorerTransactionsByExplorerTransactionId[explorerTransaction.TransactionId] = explorerTransaction;
                            }
                        }
                    }

                    // if there are still more Transactions
                    if (sqliteExplorerTransactionsArray.Length > 0)
                    {
                        foreach (SQLiteExplorerTransaction sqliteExplorerTransaction in sqliteExplorerTransactionsArray)
                        {
                            ExplorerTransaction explorerTransaction = new ExplorerTransaction(uint256.Parse(sqliteExplorerTransaction.TransactionId))
                            {
                                Fee = sqliteExplorerTransaction.Fee,
                                FirstSeen = sqliteExplorerTransaction.FirstSeen,
                                IsMarkedForStorage = sqliteExplorerTransaction.IsMarkedForStorage,
                                MetadataLevel = sqliteExplorerTransaction.MetadataLevel,
                                TotalSpent = sqliteExplorerTransaction.TotalSpent,
                            };

                            if (explorerTransaction.TransactionId != null)
                            {
                                ExplorerTransaction.references.Add(explorerTransaction);

                                explorerTransactionsByExplorerTransactionId[explorerTransaction.TransactionId] = explorerTransaction;
                            }

                            // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                            explorerTransaction.Block = ExplorerBlock.ExplorerBlockFromBlockId(uint256.Parse(sqliteExplorerTransaction.BlockId));
                            explorerTransaction.Received = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerTransaction.ReceivedIds));
                            explorerTransaction.Spent = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerTransaction.SpentIds));

                            explorerTransaction.MetadataLevel = explorerTransaction.DetermineMetadataLevelRestoredFromDatabase();
                        }
                    }
                }

                explorerTransactions = sqliteExplorerTransactionIds.Select(transactionId => explorerTransactionsByExplorerTransactionId[transactionId]).ToArray();
            }
            else
            {
                explorerTransactions = new ExplorerTransaction[] { };
            }

            return explorerTransactions;
        }

        public static ExplorerTransaction ExplorerTransactionFromSQLiteExplorerTransaction(SQLiteExplorerTransaction sqliteExplorerTransaction)
        {
            ExplorerTransaction explorerTransaction = null;

            if (sqliteExplorerTransaction != null)
            {
                lock (ExplorerTransaction.references)
                {
                    uint256 transactionId = uint256.Parse(sqliteExplorerTransaction.TransactionId);

                    explorerTransaction = ExplorerTransaction.references.GetItem(transactionId);

                    if (explorerTransaction == null)
                    {
                        explorerTransaction = new ExplorerTransaction(transactionId)
                        {
                            Fee = sqliteExplorerTransaction.Fee,
                            FirstSeen = sqliteExplorerTransaction.FirstSeen,
                            IsMarkedForStorage = sqliteExplorerTransaction.IsMarkedForStorage,
                            MetadataLevel = sqliteExplorerTransaction.MetadataLevel,
                            TotalSpent = sqliteExplorerTransaction.TotalSpent,
                        };

                        ExplorerTransaction.references.Add(explorerTransaction);

                        // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                        explorerTransaction.Block = ExplorerBlock.ExplorerBlockFromBlockId(uint256.Parse(sqliteExplorerTransaction.BlockId));
                        explorerTransaction.Received = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerTransaction.ReceivedIds));
                        explorerTransaction.Spent = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerTransaction.SpentIds)); 

                        explorerTransaction.MetadataLevel = explorerTransaction.DetermineMetadataLevelRestoredFromDatabase();
                    }
                }
            }

            return explorerTransaction;
        }

        public static ExplorerTransaction ExplorerTransactionFromTransactionId(uint256 transactionId)
        {
            ExplorerTransaction transaction = null;
            
            if (transactionId != null)
            {
                lock (ExplorerTransaction.references)
                {
                    transaction = ExplorerTransaction.references.GetItem(transactionId);

                    if (transaction == null)
                    {
                        DatabaseRepository databaseRepository = DatabaseRepository.Instance;

                        if (databaseRepository != null)
                        {
                            transaction = databaseRepository.ExplorerTransactionRepository.GetEntity(transactionId.ToString());
                        }

                        if (transaction == null)
                        {
                            transaction = new ExplorerTransaction(transactionId);
                        }

                        ExplorerTransaction.references.Add(transaction);
                    }
                }
            }

            return transaction;
        }

        #endregion

        #endregion

        #region non-public methods

        protected override async Task<MetadataResponse> GetLiteMetadataAsync(CancellationToken cancellationToken)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            Network network = AppServices.Instance.BitcoinExplorer.ExplorerClient.Network;
            GetTransactionResponse transactionResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetTransaction(this.TransactionId);// service for bitcoin to get transaction details       

            if (transactionResponse != null)
            {
                response.RawResponseObject = transactionResponse;

                this.TotalSpent = transactionResponse.Transaction.TotalOut.Satoshi;
                this.Fee = transactionResponse.Fees.Satoshi;
                this.FirstSeen = transactionResponse.FirstSeen.UtcDateTime;
                this.Block = ExplorerBlock.ExplorerBlockFromBlockId(transactionResponse.Block.BlockId);
            }
            else
            {
                response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.LiteMetadataResponseFailed;
            }

            return response;
        }

        protected override async Task<MetadataResponse> GetFullMetadataAsync(CancellationToken cancellationToken, object rawResponseObject)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            Network network = AppServices.Instance.BitcoinExplorer.ExplorerClient.Network;
            GetTransactionResponse transactionResponse = rawResponseObject as GetTransactionResponse;

            if (transactionResponse == null)
            {
                transactionResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetTransaction(this.TransactionId);// service for bitcoin to get transaction details       
            }

            if (transactionResponse != null)
            {
                foreach (Coin coin in transactionResponse.SpentCoins)
                {
                    ExplorerAddress explorerAddress = null;

                    // brads todo: figure out what the meaning of multiple pub keys can be, as well as order(assuming order in the script stack can matter)
                    PubKey[] pubKeys = coin.TxOut.ScriptPubKey.GetDestinationPublicKeys();

                    if (pubKeys.Length != 1)
                    {
                        if (pubKeys.Length == 0)
                        {
                            string address = coin.TxOut.ScriptPubKey?.GetDestination()?.GetAddress(network)?.ToWif();

                            if (!string.IsNullOrEmpty(address))
                            {
                                explorerAddress = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(address, network));
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        explorerAddress = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(pubKeys[0].GetAddress(network).ToWif(), network));
                    }
                    
                    Transfer transfer = Transfer.TransferFromTransferId(this.TransactionId, false, explorerAddress.AddressId);

                    if (!transfer.IsAllPropertiesPopulated())
                    {
                        transfer.Address = explorerAddress;
                        transfer.Amount = coin.TxOut.Value.Satoshi;
                        transfer.IsTransferIncoming = false;
                        transfer.Transaction = this;
                    }

                    // manually add to DB because transfer is not metadataChangeObject which adds to DB on changes
                    if (!transfer.CanBeRemovedFromDatabase())
                    {
                        DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                    }

                    if (!explorerAddress.TransferIds.ContainsKey(transfer.TransferId))
                    {
                        explorerAddress.Transfers.Add(transfer);
                    }

                    if (!this.SpentIds.ContainsKey(transfer.TransferId))
                    {
                        this.Spent.Add(transfer);
                    }
                }

                foreach (Coin coin in transactionResponse.ReceivedCoins)
                {
                    ExplorerAddress explorerAddress = null;

                    // brads todo: figure out what the meaning of multiple pub keys can be, as well as order(assuming order in the script stack can matter)
                    PubKey[] pubKeys = coin.TxOut.ScriptPubKey.GetDestinationPublicKeys();

                    if (pubKeys.Length != 1)
                    {
                        if (pubKeys.Length == 0)
                        {
                            string address = coin.TxOut.ScriptPubKey?.GetDestination()?.GetAddress(network)?.ToWif();

                            if (!string.IsNullOrEmpty(address))
                            {
                                explorerAddress = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(address, network));
                            }
                            else
                            {

                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        explorerAddress = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(pubKeys[0].GetAddress(network).ToWif(), network));
                    }

                    Transfer transfer = Transfer.TransferFromTransferId(this.TransactionId, true, explorerAddress.AddressId);

                    if (!transfer.IsAllPropertiesPopulated())
                    {
                        transfer.Address = explorerAddress;
                        transfer.Amount = coin.TxOut.Value.Satoshi;
                        transfer.IsTransferIncoming = true;
                        transfer.Transaction = this;
                    }

                    // manually add to DB because transfer is not metadataChangeObject which adds to DB on changes
                    if (!transfer.CanBeRemovedFromDatabase())
                    {
                        DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                    }

                    if (!explorerAddress.TransferIds.ContainsKey(transfer.TransferId))
                    {
                        explorerAddress.Transfers.Add(transfer);
                    }

                    if (!this.ReceivedIds.ContainsKey(transfer.TransferId))
                    {
                        this.Received.Add(transfer);
                    }
                }
            }
            else
            {
                response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.FullMetadataResponseFailed;
            }

            return response;
        }

        protected override void OnAssociatedObjectIsMarkedForStorageChanged(object databasePropertyChangeObject, EventArgs eventArgs)
        {
            bool isInDatabase = DatabaseRepository.Instance.ExplorerTransactionRepository.Contains(this.TransactionId.ToString());

            if (isInDatabase && this.CanBeRemovedFromDatabase())
            {
                // update or remove
                DatabaseRepository.Instance.ExplorerTransactionRepository.UpdateInDatabase(this);
            }
            else if (!isInDatabase && !this.CanBeRemovedFromDatabase())
            {
                // adds or updates
                DatabaseRepository.Instance.ExplorerTransactionRepository.EnsureIsInDatabase(this);
            }
        }

        private void OnSpentChanged(ThreadSafeObservableCollection<Transfer> oldValue, ThreadSafeObservableCollection<Transfer> newValue)
        {
            if (newValue == null)
            {
                if (oldValue != null)
                {
                    this.SpentIds?.Clear();

                    lock (this.updateNeededMutex)
                    {
                        this.IsUpdateNeeded = true;
                    }
                }
            }
            else
            {
                this.SpentIds = new MappedDictionary<string, Transfer>(newValue, (transfer => transfer.TransferId),
                    delegate
                    {
                        lock (this.updateNeededMutex)
                        {
                            this.IsUpdateNeeded = true;
                        }
                    }
                );
            }
        }

        private void OnReceivedChanged(ThreadSafeObservableCollection<Transfer> oldValue, ThreadSafeObservableCollection<Transfer> newValue)
        {
            if (newValue == null)
            {
                if (oldValue != null)
                {
                    this.ReceivedIds?.Clear();

                    lock (this.updateNeededMutex)
                    {
                        this.IsUpdateNeeded = true;
                    }
                }
            }
            else
            {
                this.ReceivedIds = new MappedDictionary<string, Transfer>(newValue, (transfer => transfer.TransferId),
                    delegate
                    {
                        lock (this.updateNeededMutex)
                        {
                            this.IsUpdateNeeded = true;
                        }
                    }
                );
            }
        }

        private void OnTransactionIdChanged(uint256 oldValue, uint256 newValue)
        {
        }

        private void OnIsUpdatingMetadataChanged(bool oldValue, bool newValue)
        {
            // if was updating and now isn't
            if (!newValue && oldValue)
            {
                bool anyAddedOrRemoved = false;
                bool isInDatabase = DatabaseRepository.Instance.ExplorerTransactionRepository.Contains(this.TransactionId.ToString());

                if (isInDatabase && this.CanBeRemovedFromDatabase())
                {
                    // update or remove
                    anyAddedOrRemoved = DatabaseRepository.Instance.ExplorerTransactionRepository.UpdateInDatabase(this);
                }
                else if (!isInDatabase && !this.CanBeRemovedFromDatabase())
                {   // adds or updates
                    anyAddedOrRemoved = DatabaseRepository.Instance.ExplorerTransactionRepository.EnsureIsInDatabase(this);
                }

                if (anyAddedOrRemoved)
                {
                    foreach (Transfer transfer in this.Spent)
                    {
                        if (transfer.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.TransferRepository.UpdateInDatabase(transfer);
                        }
                        else
                        {
                            DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                        }

                        if (transfer.Address.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.ExplorerAddressRepository.UpdateInDatabase(transfer.Address);
                        }
                        else
                        {
                            DatabaseRepository.Instance.ExplorerAddressRepository.EnsureIsInDatabase(transfer.Address);
                        }
                    }

                    foreach (Transfer transfer in this.Received)
                    {
                        if (transfer.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.TransferRepository.UpdateInDatabase(transfer);
                        }
                        else
                        {
                            DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                        }

                        if (transfer.Address.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.ExplorerAddressRepository.UpdateInDatabase(transfer.Address);
                        }
                        else
                        {
                            DatabaseRepository.Instance.ExplorerAddressRepository.EnsureIsInDatabase(transfer.Address);
                        }
                    }
                }
            }
        }

        private void OnBlockChanged(ExplorerBlock oldValue, ExplorerBlock newValue)
        {
            if (oldValue != null)
            {
                oldValue.MarkedForStorageChanged -= OnAssociatedObjectIsMarkedForStorageChanged;
            }

            if (newValue != null)
            {
                this.BlockId = newValue.BlockId.ToString();

                newValue.MarkedForStorageChanged += OnAssociatedObjectIsMarkedForStorageChanged;
            }
            else
            {
                this.BlockId = null;
            }

            lock (this.updateNeededMutex)
            {
                this.IsUpdateNeeded = true;
            }
        }

        private static List<Transfer> DeserializeTransferIds(byte[] blob)
        {
            List<Transfer> rebuiltData = new List<Transfer>();
            List<String> deserializedData;

            using (MemoryStream stream = new MemoryStream(blob))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                deserializedData = (List<String>)formatter.Deserialize(stream);
            }

            foreach (string transferId in deserializedData)
            {
                rebuiltData.Add(Transfer.TransferFromTransferId(transferId));
            }

            return rebuiltData;
        }

        #endregion
    }
}
