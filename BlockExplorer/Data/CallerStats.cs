﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using System.Threading;
using BlockExplorer.Services;
using QBitNinja.Client.Models;
using NBitcoin;
using MoreLinq;
using BlockExplorer.RestCallers.Json;
using BlockExplorer.Data.JSON;

namespace BlockExplorer.Data
{
    public class CallerStats : DynamicChangeObject
    {
        #region types

        private struct Properties
        {
            public bool isUpdating;
            public string conversionPrice;
            public DynamicPageViewModelBase.DataErrorTypes errorType;
            public string selectedCurrencyCode;
        }

        #endregion

        #region constants

        public static readonly TimeSpan Default_Update_Period = new TimeSpan(0, 0, 0, 20);

        #endregion

        #region data

        private Properties properties = new Properties();
        private GetJsonCurrentPrice getJsonCurrentPrice = new GetJsonCurrentPrice();

        #endregion

        #region properties

        public override DynamicPageViewModelBase.DataErrorTypes ErrorType
        {
            get
            {
                return this.properties.errorType;
            }
            protected set
            {
                SetProperty(ref this.properties.errorType, ref value);
            }
        }

        public override bool IsUpdating
        {
            get
            {
                return this.properties.isUpdating;
            }
            protected set
            {
                SetIsUpdating(ref this.properties.isUpdating, ref value);
            }
        }

        public string ConversionPrice
        {
            get
            {
                return this.properties.conversionPrice;
            }
            set
            {
                SetProperty(ref this.properties.conversionPrice, ref value);
            }
        }

        public string SelectedCurrencyCode
        {
            get
            {
                return this.properties.selectedCurrencyCode;
            }
            set
            {
                SetProperty(ref this.properties.selectedCurrencyCode, ref value);
            }
        }

        #endregion

        #region constructor

        public CallerStats(TimeSpan updatePeriod) : base(updatePeriod)
        {
            this.SelectedCurrencyCode = GetJsonCurrentPrice.CurrencyCodes.USD.ToString();
        }

        #endregion

        protected override async Task<DynamicResponse> UpdateAsync(CancellationToken cancellationToken)
        {
            DynamicResponse response = new DynamicResponse()
            {
                ErrorType = DynamicPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            GetJsonCurrentPrice.CurrencyCodes currencyCode = GetJsonCurrentPrice.CurrencyCodes.USD;

            Enum.TryParse<GetJsonCurrentPrice.CurrencyCodes>(this.SelectedCurrencyCode, false, out currencyCode);

            RestCaller.CallerResult<JsonCurrentPrice> callerResult = await this.getJsonCurrentPrice.GetBitcoinPriceAsync(currencyCode, cancellationToken);

            if (!cancellationToken.IsCancellationRequested)
            {
                if (callerResult.RequestSuccessful)
                {
                    this.ConversionPrice = callerResult.Result.Price.OtherCurrency.Rate;
                }
                else
                {
                    response.ErrorType = DynamicPageViewModelBase.DataErrorTypes.NetworkRequestFailed;
                }
            }
            else
            {
                response.ErrorType = DynamicPageViewModelBase.DataErrorTypes.Cancelled;
            }

            return response;
        }
    }
}
