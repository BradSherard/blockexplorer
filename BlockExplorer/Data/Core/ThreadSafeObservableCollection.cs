﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BlockExplorer.Services;
using Nito.AsyncEx;

namespace BlockExplorer.Data.Core
{
    public class ThreadSafeObservableCollection<ItemType> : PropertyChangeObject, IList<ItemType>
    {
        #region types

        public struct ChangedAction
        {
            public int Id;
            public NotifyCollectionChangedEventArgs ChangedArgs;

            public ChangedAction(int id, NotifyCollectionChangedEventArgs changedArgs)
            {
                this.Id = id;
                this.ChangedArgs = changedArgs;
            }
        }

        protected struct Properties
        {
            public bool isObservableInSync;
        }

        #endregion

        #region constants

        #endregion

        #region data

        protected Properties properties = new Properties();
        private bool isGettingSnapshot;
        private int currentId = 0;
        private Collection<ItemType> collection;
        private WeakReference<ExtendedObservableCollection<ItemType>> observableWeakReference;
        private object observableMutex = new object();
        private QueuedBufferLock updateObservableQueuedLock = new QueuedBufferLock();
        private readonly AsyncLock updatingObservableMutex = new AsyncLock();
        private int observablelastIdUpdated = -1;

        #endregion

        #region events

        /// <summary>
        /// This event is raised immediately after the source collection is changed. Unlike the Observable, 
        /// changes to the source collection are not guaranteed to be on the dispatcher.
        /// So any event handling is responsible for ensuring that work is done on the right thread.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event EventHandler ObservableInSync;

        #endregion

        #region properties

        public Queue<ChangedAction> ChangedActionQueue
        {
            get; set;
        }

        public int Count
        {
            get
            {
                lock (this.mutex)
                {
                    return this.collection.Count;
                }
            }
        }

        public bool IsObservableInSync
        {
            get
            {
                return this.properties.isObservableInSync;
            }
            private set
            {
                if (!value)
                {
                    if (this.ObservableIfExists != null)
                    {
                        SetProperty(ref this.properties.isObservableInSync, ref value);

                        StartUpdatingObservable();
                    }
                }
                else
                {
                    SetProperty(ref this.properties.isObservableInSync, ref value);

                    SendEvent(this, this.ObservableInSync);
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public ExtendedObservableCollection<ItemType> Observable
        {
            get
            {
                ExtendedObservableCollection<ItemType> observable;

                lock (this.mutex)
                {
                    observable = this.ObservableIfExists;

                    if (observable == null)
                    {
                        observable = new ExtendedObservableCollection<ItemType>(this.collection, this);

                        this.ChangedActionQueue.Clear();
                        this.IsObservableInSync = true;
                        this.observableWeakReference = new WeakReference<ExtendedObservableCollection<ItemType>>(observable);
                        this.observablelastIdUpdated = -1;
                    }
                }

                return observable;
            }
        }

        private ExtendedObservableCollection<ItemType> ObservableIfExists
        {
            get
            {
                ExtendedObservableCollection<ItemType> observable = null;

                lock (this.mutex)
                {
                    if (this.observableWeakReference != null)
                    {
                        this.observableWeakReference.TryGetTarget(out observable);
                    }
                }

                return observable;
            }
        }

        #endregion

        #region constructors

        public ThreadSafeObservableCollection()
            : this(new List<ItemType>())
        {
        }

        public ThreadSafeObservableCollection(IEnumerable<ItemType> collection)
        {
            if (collection != null)
            {
                this.collection = new Collection<ItemType>(collection.ToList<ItemType>());
            }
            else
            {
                this.collection = new Collection<ItemType>();
            }

            this.ChangedActionQueue = new Queue<ChangedAction>();
            this.IsObservableInSync = true;
        }

        public ThreadSafeObservableCollection(IList<ItemType> list)
        {
            this.ChangedActionQueue = new Queue<ChangedAction>();
            this.collection = new Collection<ItemType>(list);
            this.IsObservableInSync = true;
        }

        #endregion

        #region indexers

        public ItemType this[int index]
        {
            get
            {
                lock (this.mutex)
                {
                    return this.collection[index];
                }
            }
            set
            {
                lock (this.mutex)
                {
                    Set(index, value);
                }
            }
        }

        #endregion

        #region public methods

        public void Add(ItemType item)
        {
            lock (this.mutex)
            {
                this.collection.Add(item);

                EnqueueAction(new ChangedAction(
                    this.currentId,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, this.collection.Count - 1)));
            }
        }

        public void AddItems(IEnumerable<ItemType> items)
        {
            if (items != null)
            {
                ItemType[] itemArray = items.ToArray();

                if (itemArray.Length > 0)
                {
                    lock (this.mutex)
                    {
                        int start = this.collection.Count;

                        foreach (ItemType item in itemArray)
                        {
                            this.collection.Add(item);
                        }

                        EnqueueAction(new ChangedAction(
                            this.currentId,
                            new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemArray, start)));
                    }
                }
            }
        }

        public void Clear()
        {
            lock (this.mutex)
            {
                this.collection.Clear();

                EnqueueAction(new ChangedAction(
                    this.currentId,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset)));
            }
        }

        public bool Contains(ItemType item)
        {
            bool contains;

            lock (this.mutex)
            {
                contains = this.collection.Contains(item);
            }

            return contains;
        }

        public void CopyTo(ItemType[] array, int arrayIndex)
        {
            if (array != null)
            {
                lock (this.mutex)
                {
                    this.collection.CopyTo(array, arrayIndex);
                }
            }
        }

        public int IndexOf(ItemType item)
        {
            int index;

            lock (this.mutex)
            {
                index = this.collection.IndexOf(item);
            }

            return index;
        }

        public void Insert(int index, ItemType item)
        {
            lock (this.mutex)
            {
                if ((index >= 0) && (index <= this.Count))
                {
                    this.collection.Insert(index, item);

                    EnqueueAction(new ChangedAction(
                        this.currentId,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index)));
                }
            }
        }

        public void Move(int oldIndex, int newIndex)
        {
            lock (this.mutex)
            {
                if ((oldIndex >= 0) && (oldIndex < this.Count) && (newIndex >= 0) && (newIndex <= this.Count))
                {
                    ItemType item = this.collection[oldIndex];

                    this.collection.RemoveAt(oldIndex);
                    this.collection.Insert(newIndex, item);

                    EnqueueAction(new ChangedAction(
                        this.currentId,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, newIndex, oldIndex)));
                }
            }
        }

        public bool Remove(ItemType item)
        {
            bool removed;

            lock (this.mutex)
            {
                int index = this.collection.IndexOf(item);
                removed = this.collection.Remove(item);

                if (removed)
                {
                    EnqueueAction(new ChangedAction(
                        this.currentId,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index)));
                }
            }

            return removed;
        }

        public void RemoveAt(int index)
        {
            lock (this.mutex)
            {
                ItemType item = this.collection[index];

                this.collection.RemoveAt(index);

                EnqueueAction(new ChangedAction(
                    this.currentId,
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index)));
            }
        }

        public bool RemoveItems(IEnumerable<ItemType> items)
        {
            bool removedAny = false;
            bool isContiguous = true;
            int start = 0;

            if (items != null)
            {
                ItemType[] itemArray = items.ToArray();

                if (itemArray.Length > 0)
                {
                    lock (this.mutex)
                    {
                        List<ItemType> removedItems = new List<ItemType>(itemArray.Length);

                        foreach (ItemType item in items)
                        {
                            int index = this.collection.IndexOf(item);

                            if (removedItems.Count == 0)
                            {
                                start = index;
                            }
                            else
                            {
                                if (index != start)
                                {
                                    isContiguous = false;
                                }
                            }

                            if (index >= 0)
                            {
                                removedAny = true;

                                this.collection.RemoveAt(index);

                                removedItems.Add(item);
                            }
                        }

                        if (removedAny)
                        {
                            NotifyCollectionChangedEventArgs changedArgs = null;

                            if (isContiguous)
                            {
                                changedArgs = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItems, start);
                            }
                            else
                            {
                                changedArgs = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItems);
                            }

                            EnqueueAction(new ChangedAction(this.currentId, changedArgs));
                        }
                    }
                }
            }

            return removedAny;
        }

        public void Set(int index, ItemType item)
        {
            lock (this.mutex)
            {
                if ((index >= 0) && (index < this.Count))
                {
                    ItemType oldItem = this.collection[index];
                    this.collection[index] = item;

                    EnqueueAction(new ChangedAction(
                        this.currentId,
                        new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, item, oldItem, index)));
                }
            }
        }

        public void StartUpdatingObservable()
        {
            lock (this.mutex)
            {
                if (!this.isGettingSnapshot)
                {
                    if (this.ObservableIfExists != null)
                    {
                        this.isGettingSnapshot = true;
                        BackgroundWorker worker = new BackgroundWorker();
                        worker.DoWork += UpdateObservableBackgroundWorker_DoWork;
                        worker.RunWorkerAsync(null);
                    }
                }
            }
        }

        #endregion

        #region non-public methods

        private async Task ApplyActions(ExtendedObservableCollection<ItemType> observableCollection, ChangedAction[] actions)
        {
            foreach (ChangedAction item in actions)
            {
                if (this.observablelastIdUpdated < item.Id)
                {
                    switch (item.ChangedArgs.Action)
                    {
                        case NotifyCollectionChangedAction.Add:
                        {
                            await observableCollection.ApplyAddAction(item.ChangedArgs);
                            break;
                        }

                        case NotifyCollectionChangedAction.Move:
                        {
                            observableCollection.ApplyMoveAction(item.ChangedArgs);
                            break;
                        }

                        case NotifyCollectionChangedAction.Remove:
                        {
                            await observableCollection.ApplyRemoveAction(item.ChangedArgs);
                            break;
                        }
                        case NotifyCollectionChangedAction.Replace:
                        {
                            observableCollection.ApplyReplaceAction(item.ChangedArgs);
                            break;
                        }
                        case NotifyCollectionChangedAction.Reset:
                        {
                            observableCollection.ApplyResetAction(item.ChangedArgs);
                            break;
                        }
                    }

                    this.observablelastIdUpdated = item.Id;
                }
            }
        }

        private void EnqueueAction(ChangedAction changedAction)
        {
            NotifyCollectionChangedEventArgs changedArgs = changedAction.ChangedArgs;

            switch (changedArgs.Action)
            {
                case NotifyCollectionChangedAction.Add:
                {
                    int index = changedArgs.NewStartingIndex;

                    foreach (ItemType item in changedArgs.NewItems)
                    {
                        OnCollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));

                        index += 1;
                    }
                    break;
                }
                case NotifyCollectionChangedAction.Move:
                {
                    // current implementation only handles a single item being moved 
                    OnCollectionChanged(this, changedArgs);
                    break;
                }
                case NotifyCollectionChangedAction.Remove:
                {
                    if (changedArgs.OldItems.Count == 1)
                    {
                        OnCollectionChanged(this, changedArgs);
                    }
                    else
                    {
                        // if removes are contiguous
                        if (changedArgs.OldStartingIndex >= 0)
                        {
                            foreach (ItemType item in changedArgs.OldItems)
                            {
                                OnCollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, changedArgs.OldStartingIndex));
                            }
                        }
                        else
                        {
                            foreach (ItemType item in changedArgs.OldItems)
                            {
                                OnCollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item));
                            }
                        }
                    }
                    break;
                }
                case NotifyCollectionChangedAction.Replace:
                {
                    // current implementation only handles a single item being replaced

                    // workaround for Replace notification requires specifying new item as the old item
                    OnCollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, changedArgs.NewItems[0], changedArgs.NewItems[0], changedArgs.OldStartingIndex));
                    break;
                }
                case NotifyCollectionChangedAction.Reset:
                {
                    OnCollectionChanged(this, changedAction.ChangedArgs);
                    break;
                }
            }

            this.ChangedActionQueue.Enqueue(changedAction);

            this.currentId += 1;
            this.IsObservableInSync = false;
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // not necessarily called on the dispatcher
            this.CollectionChanged?.Invoke(sender, e);
        }

        private async Task<bool> UpdateObservable(ExtendedObservableCollection<ItemType> observableCollection, ItemType[] sourceCollectionSnapshot, ChangedAction[] changedActionsSnapshot, int snapshotId)
        {
            bool hasUpdated = false;

            if (observableCollection != null)
            {
                using (await updatingObservableMutex.LockAsync())
                {
                    if (this.observablelastIdUpdated < snapshotId)
                    {
                        if (sourceCollectionSnapshot != null)
                        {
                            await observableCollection.ReplaceAll(sourceCollectionSnapshot, sourceCollectionSnapshot.Length);

                            hasUpdated = true;
                            this.observablelastIdUpdated = snapshotId;
                        }
                        else if (changedActionsSnapshot != null)
                        {
                            await ApplyActions(observableCollection, changedActionsSnapshot);

                            hasUpdated = true;
                        }
                    }
                }   
            }
            
            return hasUpdated;
        }

        /// <summary>
        /// Puts the observer in sync with the source collection. The background worker
        /// collects all the changes that will be made, then calls the dispatcher to
        /// make those changes, then updates the sync state back off the dispatcher
        /// </summary>
        private void UpdateObservableBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            lock (this.mutex)
            {
                this.isGettingSnapshot = false;
            }

            ExtendedObservableCollection<ItemType> observableCollection = this.ObservableIfExists;

            if (observableCollection != null)
            {
                ItemType[] collectionSnapshot = null;
                ChangedAction[] changedActionsSnapshot = null;
                int snapshotCurrentId;

                lock (this.mutex)
                {
                    if (!this.IsObservableInSync)
                    {
                        // if the collection is smaller than all the change operations that created it, use it
                        if (this.collection.Count < this.ChangedActionQueue.Count)
                        {
                            collectionSnapshot = new ItemType[this.Count];

                            CopyTo(collectionSnapshot, 0);
                        }
                        else
                        {
                            changedActionsSnapshot = this.ChangedActionQueue.ToArray();
                        }
                    }

                    snapshotCurrentId = this.currentId - 1;
                }

                if (collectionSnapshot != null || changedActionsSnapshot != null)
                {
                    AsyncDispatcher.Instance.InvokeAsync(
                        async delegate
                        {
                            if (await UpdateObservable(observableCollection, collectionSnapshot, changedActionsSnapshot, snapshotCurrentId))
                            {
                                ThreadPool.QueueUserWorkItem(
                                    delegate
                                    {
                                        lock (this.mutex)
                                        {
                                            // remove queued changes that have been applied to the observer
                                            while ((this.ChangedActionQueue.Count > 0) && (this.ChangedActionQueue.Peek().Id <= snapshotCurrentId))
                                            {
                                                this.ChangedActionQueue.Dequeue();
                                            }

                                            // if collection has not been changed since snapshot was taken
                                            if (snapshotCurrentId == this.currentId - 1)
                                            {
                                                if (!this.IsObservableInSync)
                                                {
                                                    this.IsObservableInSync = true;
                                                }
                                            }
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
            }
        }

        #endregion

        #region IEnumerable<ItemType> Members

        public IEnumerator<ItemType> GetEnumerator()
        {
            ItemType[] items;

            lock (this.mutex)
            {
                items = this.collection.ToArray();
            }

            foreach (ItemType item in items)
            {
                yield return item;
            }
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            ItemType[] items;

            lock (this.mutex)
            {
                items = this.collection.ToArray();
            }

            foreach (ItemType item in items)
            {
                yield return item;
            }
        }

        #endregion
    }
}
