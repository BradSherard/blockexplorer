﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public class MetadataChangedEventArgs : EventArgs
    {
        public MetadataChangedEventArgs(MetadataChangeObject.MetadataLevels oldValue, MetadataChangeObject.MetadataLevels newValue)
        {
            this.NewMetadataLevel = newValue;
            this.OldMetadataLevel = oldValue;
        }

        public MetadataChangeObject.MetadataLevels NewMetadataLevel { get; private set; }
        public MetadataChangeObject.MetadataLevels OldMetadataLevel { get; private set; }
    }
}
