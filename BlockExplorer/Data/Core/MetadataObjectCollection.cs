﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using BlockExplorer.Data;
using BlockExplorer.ViewModels.Core;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace BlockExplorer.Data.Core
{
    public class MetadataObjectCollection<MetadataType> where MetadataType : MetadataChangeObject
    {
        #region data

        private readonly AsyncLock updatingMetadataMutex = new AsyncLock();

        private Func<MetadataType, Task<MetadataPageViewModelBase.DataErrorTypes>> addAction;
        private Func<MetadataType, Task<MetadataPageViewModelBase.DataErrorTypes>> removeAction;

        #endregion

        #region properties

        public ThreadSafeObservableCollection<MetadataType> Collection { get; set; }

        #endregion

        #region constructors

        public MetadataObjectCollection(IEnumerable<MetadataType> collection, Func<MetadataType, Task<MetadataPageViewModelBase.DataErrorTypes>> add, Func<MetadataType, Task<MetadataPageViewModelBase.DataErrorTypes>> remove)
        {
            this.addAction = add;
            this.removeAction = remove;
            this.Collection = new ThreadSafeObservableCollection<MetadataType>(collection);
        }

        #endregion

        #region public methods

        public async Task<MetadataPageViewModelBase.DataErrorTypes> AddItem(MetadataType item)
        {
            MetadataPageViewModelBase.DataErrorTypes error = MetadataPageViewModelBase.DataErrorTypes.None;

            if (item != null)
            {
                using (await this.updatingMetadataMutex.LockAsync())
                {
                    error = await this.addAction.Invoke(item);

                    if (error == MetadataPageViewModelBase.DataErrorTypes.None)
                    {
                        this.Collection.Add(item);
                    }
                }
            }

            return error;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> RemoveItem(MetadataType item)
        {
            MetadataPageViewModelBase.DataErrorTypes error = MetadataPageViewModelBase.DataErrorTypes.None;

            if (item != null)
            {
                using (await this.updatingMetadataMutex.LockAsync())
                {
                    error = await this.removeAction.Invoke(item);

                    if (error == MetadataPageViewModelBase.DataErrorTypes.None)
                    {
                        this.Collection.Remove(item);
                    }
                }
            }

            return error;
        }

        #endregion

        #region non-public methods 

        #endregion
    }
}
