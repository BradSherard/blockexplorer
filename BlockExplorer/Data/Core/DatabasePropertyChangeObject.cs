﻿using BlockExplorer.ViewModels.Core;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public abstract class DatabasePropertyChangeObject : PropertyChangeObject
    {
        #region events

        // new event specifically for database messaging as to not get events for every possible property
        public event EventHandler MarkedForStorageChanged;

        #endregion

        #region data

        protected string[] propertyNames;
        protected object updateNeededMutex = new object();
        private bool isMarkedForStorage = false;

        #endregion

        #region constructors

        public DatabasePropertyChangeObject(string[] propertyNames)
        {
            this.propertyNames = propertyNames;
        }

        #endregion

        #region properties

        protected bool IsUpdateNeeded { get; set; }

        // necessarily true when object is being parsed from DB, so it doesn't need to be stored in DB
        public bool IsMarkedForStorage
        {
            get
            {
                return this.isMarkedForStorage;
            }
            set
            {
                bool oldValue = this.isMarkedForStorage;

                base.SetProperty(ref this.isMarkedForStorage, ref value);

                if (oldValue != value)
                {
                    MarkedForStorageChanged?.Invoke(this, new EventArgs());
                }
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// false if is database object and .IsMarkedForStorage is true, or if any directly associated database object is marked for storage
        /// </summary>
        /// <returns></returns>
        public abstract bool CanBeRemovedFromDatabase();

        #endregion

        #region non-public methods

        /// <summary>
        /// Register object.OnAssociatedObjectIsMarkedForStorageChanged with all associated databaseChangeObjects that
        /// determine if this object should be stored as well. When they change, they will call this object to update it.
        /// When this object is marked for storage, objects that reference it will be added to storage too without
        /// requiring this object to reference it. Standard pattern is to only use this when this object does not reference
        /// the target and so cannot update it when metadata is changed.
        /// </summary>
        /// <param name="databasePropertyChangeObject"></param>
        /// <param name="eventArgs"></param>
        protected abstract void OnAssociatedObjectIsMarkedForStorageChanged(object databasePropertyChangeObject, EventArgs eventArgs);

        protected new void SetProperty<PropertyType>(string propertyName, ref PropertyType propertyValue, ref PropertyType newValue, ChangedAction<PropertyType> changedAction = null)
        {
            bool isValueChanged = base.SetProperty<PropertyType>(propertyName, ref propertyValue, ref newValue, changedAction);

            if (isValueChanged && (this.propertyNames != null) && this.propertyNames.Contains(propertyName))
            {
                lock (this.updateNeededMutex)
                {
                    this.IsUpdateNeeded = true;
                }
            }
        }

        protected new void SetProperty<PropertyType>(ref PropertyType propertyValue, ref PropertyType newValue, ChangedAction<PropertyType> changedAction = null, [CallerMemberName] string propertyName = null)
        {
            SetProperty<PropertyType>(propertyName, ref propertyValue, ref newValue, changedAction);
        }

        #endregion

        #region public methods

        public bool CheckAndResetIsUpdateNeeded()
        {
            bool isUpdateNeeded;

            lock (this.updateNeededMutex)
            {
                isUpdateNeeded = this.IsUpdateNeeded;

                if (isUpdateNeeded)
                {
                    this.IsUpdateNeeded = false;
                }
            }

            return isUpdateNeeded;
        }

        #endregion
    }
}
