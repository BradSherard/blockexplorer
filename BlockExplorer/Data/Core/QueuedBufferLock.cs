﻿using System;
using System.Threading;
using Nito.AsyncEx;

public class QueuedBufferLock
{
    #region data

    private object innerLock;
    private volatile int ticket_EndOfQueue = 0;
    private volatile int ticket_NextToEnter = 1;

    #endregion

    #region constructor

    public QueuedBufferLock()
    {
        this.innerLock = new Object();
    }

    #endregion

    #region public methods

    public void Enter()
    {
        int myTicket = Interlocked.Increment(ref this.ticket_EndOfQueue);
        System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId + ": QueuedLock new end of queue: "+ myTicket);

        Monitor.Enter(this.innerLock);
        System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId + ": QueuedLock entered: " + myTicket);

        while (true)
        {
            if (myTicket == this.ticket_NextToEnter)
            {
                System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId + ": QueuedLock determined " + myTicket + " is allowed to enter");
                return;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId + ": QueuedLock determined " + myTicket + " is not allowed to enter and must wait");
                Monitor.Wait(this.innerLock);
            }
        }
    }

    public void Wait()
    {
        Monitor.Wait(this.innerLock);
    }

    public void Exit()
    {
        Interlocked.Increment(ref this.ticket_NextToEnter);
        System.Diagnostics.Debug.WriteLine(Thread.CurrentThread.ManagedThreadId + ": QueuedLock is empty again and is ready to take " + this.ticket_NextToEnter);
        Monitor.PulseAll(this.innerLock);
        Monitor.Exit(this.innerLock);
    }

    #endregion
}