﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public class MappedDictionary<T, SourceType> : Dictionary<T, SourceType> where SourceType : class
    {
        private Func<SourceType, T> mapFunc;
        private ThreadSafeObservableCollection<SourceType> source;
        private Action onItemsChanged;

        private ThreadSafeObservableCollection<SourceType> Source
        {
            get
            {
                return this.source;
            }
            set
            {
                var oldValue = this.source;
                this.source = value;

                OnSourceChanged(oldValue, value);
            }
        }

        
        public MappedDictionary(ThreadSafeObservableCollection<SourceType> source, Func<SourceType, T> sourceToMappedTypeFunc, Action onItemsChanged = null)
        {
            this.onItemsChanged = onItemsChanged;
            this.mapFunc = sourceToMappedTypeFunc;
            this.Source = source;
        }

        private void OnSourceChanged(ThreadSafeObservableCollection<SourceType> oldValue, ThreadSafeObservableCollection<SourceType> newValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= SourceObservable_CollectionChanged;

                this.Clear();
            }

            if (newValue != null)
            {
                newValue.CollectionChanged += SourceObservable_CollectionChanged;
                
                foreach (SourceType item in newValue)
                {
                    this[this.mapFunc(item)] = item;
                }
            }
        }

        private void SourceObservable_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                {
                    foreach (SourceType item in e.NewItems.OfType<SourceType>())
                    {
                        this[this.mapFunc(item)] = item;
                    }
                    
                    break;
                }
                case NotifyCollectionChangedAction.Move:
                {
                    break;
                }
                case NotifyCollectionChangedAction.Remove:
                {
                    foreach (SourceType item in e.OldItems.OfType<SourceType>())
                    {
                        this.Remove(this.mapFunc(item));
                    }

                    break;
                }
                case NotifyCollectionChangedAction.Replace:
                {
                    foreach (SourceType item in e.OldItems.OfType<SourceType>())
                    {
                        this.Remove(this.mapFunc(item));
                    }

                    goto case NotifyCollectionChangedAction.Add;
                }
                case NotifyCollectionChangedAction.Reset:
                {
                    base.Clear();

                    foreach (SourceType item in this.Source)
                    {
                        this[this.mapFunc(item)] = item;
                    }

                    break;
                }
            }

            this.onItemsChanged?.Invoke();
        }
    }
}
