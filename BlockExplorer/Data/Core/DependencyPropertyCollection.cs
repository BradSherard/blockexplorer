﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows;

namespace BlockExplorer.Data.Core
{
    public class DependencyPropertyCollection
    {
        #region data

        private Dictionary<string, DependencyProperty> properties = new Dictionary<string, DependencyProperty>();

        #endregion

        #region constructors

        public DependencyPropertyCollection(IEnumerable<KeyValuePair<string, DependencyProperty>> properties)
        {
            foreach (KeyValuePair<string, DependencyProperty> pair in properties)
            {
                this.properties.Add(pair.Key, pair.Value);
            }
        }

        #endregion

        #region indexers

        public DependencyProperty this[string name]
        {
            get
            {
                return this.properties[name];
            }
        }

        #endregion

        #region static methods

        public static KeyValuePair<string, DependencyProperty> Register(string name, Type propertyType, Type ownerType, PropertyMetadata typeMetadata)
        {
            return new KeyValuePair<string, DependencyProperty>(name,
                DependencyProperty.Register(name, propertyType, ownerType, typeMetadata));
        }

        public static KeyValuePair<string, DependencyProperty> RegisterAttached(string name, Type propertyType, Type ownerType, PropertyMetadata typeMetadata)
        {
            return new KeyValuePair<string, DependencyProperty>(name,
                DependencyProperty.RegisterAttached(name, propertyType, ownerType, typeMetadata));
        }

        #endregion

        #region methods

        public ValueType GetValue<ValueType>(DependencyObject dependencyObject, [CallerMemberName] string name = null)
        {
            return (ValueType)dependencyObject.GetValue(this.properties[name]);
        }

        public void SetValue<ValueType>(DependencyObject dependencyObject, ValueType value, [CallerMemberName] string name = null)
        {
            dependencyObject.SetValue(this.properties[name], value);
        }

        #endregion
    }
}