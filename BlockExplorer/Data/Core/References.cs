﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlockExplorer.Data.Core
{
    /// <summary>
    /// Maintains references to items based on a key value for the item.
    /// </summary>
    /// <typeparam name="ItemType">Type for the item</typeparam>
    /// <typeparam name="KeyType">Type for the key</typeparam>
    public class References<ItemType, KeyType> where ItemType : class
    {
        #region types

        public delegate KeyType KeyForItemDelegate(ItemType item);

        #endregion

        #region data

        private object mutex = new object();
        private Dictionary<KeyType, WeakReference<ItemType>> dictionary;
        private KeyForItemDelegate keyForItem;

        #endregion

        #region constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="keyForItem">Delegate that provides the key given the item.</param>
        public References(KeyForItemDelegate keyForItem)
        {
            this.dictionary = new Dictionary<KeyType, WeakReference<ItemType>>();
            this.keyForItem = keyForItem;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="keyForItem">Delegate that provides the key given the item.</param>
        /// <param name="keyComparer">Specifies how to compare keys for equality. You can use the case-insensitive string comparers provided by the StringComparer class for case-insensitive string keys.</param>
        public References(KeyForItemDelegate keyForItem, IEqualityComparer<KeyType> keyComparer)
        {
            this.dictionary = new Dictionary<KeyType, WeakReference<ItemType>>(keyComparer);
            this.keyForItem = keyForItem;
        }

        #endregion

        #region indexers

        /// <summary>
        /// Gets or sets the reference to an item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ItemType this[KeyType key]
        {
            get
            {
                lock (this.mutex)
                {
                    ItemType item = null;
                    WeakReference<ItemType> weakReference;

                    if (this.dictionary.TryGetValue(key, out weakReference))
                    {
                        weakReference.TryGetTarget(out item);
                    }

                    // if the item no longer exists
                    if (item == null)
                    {
                        // remove the reference
                        if (this.dictionary.ContainsKey(key))
                        {
                            this.dictionary.Remove(key);
                        }
                    }

                    return item;
                }
            }
            set
            {
                lock (this.mutex)
                {
                    this.dictionary[key] = new WeakReference<ItemType>(value);
                }
            }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Adds a reference to an item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Add(ItemType item)
        {
            KeyType key = KeyForItem(item);

            // Use the indexer to set the item in the dictionary
            // because the key may already exist while the WeakReference is null.
            // Using this.dictionary.Add() would cause a duplicate key exception.
            lock (this.mutex)
            {
                this.dictionary[key] = new WeakReference<ItemType>(item);
            }
        }

        /// <summary>
        /// Clears all references.
        /// </summary>
        public void Clear()
        {
            lock (this.mutex)
            {
                this.dictionary.Clear();
            }
        }

        /// <summary>
        /// Determines if there is a reference to an item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>True if there is a reference to an item, or false otherwise.</returns>
        public bool Contains(ItemType item)
        {
            KeyType key = this.KeyForItem(item);

            // return true if the item is in the dictionary and the item still exists
            lock (this.mutex)
            {
                return (this[key] != null);
            }
        }

        /// <summary>
        /// Gets an item based on the key.
        /// </summary>
        /// <param name="key">Key for the item.</param>
        /// <returns>The item or null if there is no item for that key value.</returns>
        public ItemType GetItem(KeyType key)
        {
            ItemType item;

            lock (this.mutex)
            {
                item = this[key];
            }

            return item;
        }

        /// <summary>
        /// Returns an enumerator to the items contains for the specified keys.
        /// </summary>
        /// <param name="keys">Keys</param>
        /// <returns>An enumertor for the items for the specified keys.</returns>
        public IEnumerable<ItemType> GetItems(IEnumerable<KeyType> keys)
        {
            return from key in keys
                   where this.ContainsKey(key)
                   let value = this[key]
                   where value != null
                   select value;
        }

        /// <summary>
        /// Detemrines if there is a reference for the key.
        /// </summary>
        /// <param name="key">Key</param>
        /// <returns>True if there is a reference, or false otherwise.</returns>
        public bool ContainsKey(KeyType key)
        {
            lock (this.mutex)
            {
                return this.dictionary.ContainsKey(key);
            }
        }

        /// <summary>
        /// Removes the reference to the specified key, if there is one.
        /// </summary>
        /// <param name="key">Key</param>
        public void RemoveKey(KeyType key)
        {
            lock (this.mutex)
            {
                if (this.dictionary.ContainsKey(key))
                {
                    this.dictionary.Remove(key);
                }
            }
        }

        /// <summary>
        /// Removes the reference to the item, if there is one.
        /// </summary>
        /// <param name="item"></param>
        public void RemoveItem(ItemType item)
        {
            KeyType key = KeyForItem(item);

            this.RemoveKey(key);
        }

        /// <summary>
        /// Returns the key for an item.
        /// </summary>
        /// <param name="item">Item</param>
        /// <returns>The key for the item.</returns>
        private KeyType KeyForItem(ItemType item)
        {
            return this.keyForItem(item);
        }

        #endregion
    }
}
