﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public static class Extensions
    {
        public static void AppendIds(this StringBuilder stringBuilder, IEnumerable<string> ids)
        {
            stringBuilder.Append("(");

            if (ids.Any())
            {
                foreach (string id in ids)
                {
                    stringBuilder.Append("'");
                    stringBuilder.Append(id);
                    stringBuilder.Append("'");
                    stringBuilder.Append(",");
                }

                stringBuilder.Remove(stringBuilder.Length - 1, 1);
            }

            stringBuilder.Append(")");
        }
    }
}
