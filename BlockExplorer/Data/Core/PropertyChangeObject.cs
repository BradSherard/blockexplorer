﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using BlockExplorer.Services;

namespace BlockExplorer.Data.Core
{
    public abstract class PropertyChangeObject : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region types

        public delegate void ChangedAction<PropertyType>(PropertyType oldValue, PropertyType newValue);

        #endregion

        #region events

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        #endregion

        #region data

        protected object mutex = new object();

        #endregion

        #region constructors

        public PropertyChangeObject()
        {
        }

        #endregion

        #region non-public methods

        protected void NotifyPropertyChanged(object sender = null, [CallerMemberName] string propertyName = null)
        {
            if (this.PropertyChanged != null)
            {
                AsyncDispatcher.Instance.InvokeAsync(
                    delegate
                    {
                        this.PropertyChanged?.Invoke(sender ?? this, new PropertyChangedEventArgs(propertyName));
                    }
                );
            }
        }

        protected void NotifyPropertyChanging(object sender = null, [CallerMemberName] string propertyName = null)
        {
            try
            {
                this.PropertyChanging?.Invoke(sender ?? null, new PropertyChangingEventArgs(propertyName));
            }
            catch (NullReferenceException)
            {
            }
            catch (InvalidCastException)
            {
            }
        }

        protected void SendEvent(PropertyChangeObject sender, EventHandler eventHandler)
        {
            SendEvent(sender, eventHandler, new EventArgs());
        }

        protected void SendEvent(PropertyChangeObject sender, EventHandler eventHandler, EventArgs eventArgs)
        {
            if (eventHandler != null)
            {
                AsyncDispatcher.Instance.InvokeAsync(
                    delegate
                    {
                        eventHandler?.Invoke(sender, eventArgs);
                    }
                );
            }
        }

        protected void SendEvent<EventArgsType>(PropertyChangeObject sender, EventHandler<EventArgsType> eventHandler) where EventArgsType : EventArgs, new()
        {
            SendEvent(sender, eventHandler, new EventArgsType());
        }

        protected void SendEvent<EventArgsType>(PropertyChangeObject sender, EventHandler<EventArgsType> eventHandler, EventArgsType eventArgs) where EventArgsType : EventArgs
        {
            if (eventHandler != null)
            {
                AsyncDispatcher.Instance.InvokeAsync(
                    delegate
                    {
                        eventHandler?.Invoke(sender, eventArgs);
                    }
                );
            }
        }

        protected bool SetProperty<PropertyType>(string propertyName, ref PropertyType propertyValue, ref PropertyType newValue, ChangedAction<PropertyType> changedAction = null)
        {
            bool isValueChanged = false;
            PropertyType oldValue = propertyValue;

            if (!Equals(propertyValue, newValue))
            {
                lock (this.mutex)
                {
                    propertyValue = newValue;
                    isValueChanged = true;

                    NotifyPropertyChanged(this, propertyName);

                    changedAction?.Invoke(oldValue, newValue);
                }
            }

            return isValueChanged;
        }

        protected bool SetProperty<PropertyType>(ref PropertyType propertyValue, ref PropertyType newValue, ChangedAction<PropertyType> changedAction = null, [CallerMemberName] string propertyName = null)
        {
            return SetProperty<PropertyType>(propertyName, ref propertyValue, ref newValue, changedAction);
        }

        #endregion
    }
}
