﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public class IsMetadataPropertyAttribute : Attribute
    {
        public MetadataChangeObject.MetadataLevels MetadataLevel { get; private set; }

        public IsMetadataPropertyAttribute(MetadataChangeObject.MetadataLevels metadataLevel)
        {
            this.MetadataLevel = metadataLevel;
        }
    }
}
