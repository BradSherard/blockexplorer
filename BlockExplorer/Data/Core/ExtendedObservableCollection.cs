﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public class ExtendedObservableCollection<ItemType> : ObservableCollection<ItemType>
    {
        #region constants

        private const string PropertyName_IndexerName = "Item[]";
        private const int paginationSize = 20;
        private const int paginationTimeoutMilliseconds = 200;

        #endregion

        #region data

        private ThreadSafeObservableCollection<ItemType> sourceCollection;

        #endregion

        #region constructors

        public ExtendedObservableCollection(IEnumerable<ItemType> collection, ThreadSafeObservableCollection<ItemType> source)
            : base(collection)
        {
            this.sourceCollection = source;
        }

        #endregion

        #region public methods

        public async Task ApplyAddAction(NotifyCollectionChangedEventArgs changedArgs)
        {
            CheckReentrancy();

            int index = changedArgs.NewStartingIndex;

            foreach (ItemType item in changedArgs.NewItems)
            {
                this.Items.Insert(index, item);

                OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
                OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));

                index++;

                if (index % ExtendedObservableCollection<ItemType>.paginationSize == 0)
                {
                    await Task.Delay(ExtendedObservableCollection<ItemType>.paginationTimeoutMilliseconds);
                }
            }
        }

        public void ApplyMoveAction(NotifyCollectionChangedEventArgs changedArgs)
        {
            // current implementation only handles a single item being moved 
            CheckReentrancy();

            this.Items.RemoveAt(changedArgs.OldStartingIndex);
            this.Items.Insert(changedArgs.NewStartingIndex, (ItemType)changedArgs.OldItems[0]);

            OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
            OnCollectionChanged(changedArgs);
        }

        public async Task ApplyRemoveAction(NotifyCollectionChangedEventArgs changedArgs)
        {
            CheckReentrancy();

            // if removes are contiguous
            if (changedArgs.OldStartingIndex >= 0)
            {
                int index = 0;

                foreach (ItemType item in changedArgs.OldItems)
                {
                    this.Items.RemoveAt(changedArgs.OldStartingIndex);

                    OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
                    OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
                    OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, changedArgs.OldStartingIndex));

                    index++;

                    if (index % ExtendedObservableCollection<ItemType>.paginationSize == 0)
                    {
                        await Task.Delay(ExtendedObservableCollection<ItemType>.paginationTimeoutMilliseconds);
                    }
                }
            }
            else
            {
                int delayCounter = 0;

                foreach (ItemType item in changedArgs.OldItems)
                {
                    int index = this.Items.IndexOf(item);

                    if (index >= 0)
                    {
                        this.Items.RemoveAt(index);

                        OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
                        OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
                        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));

                        delayCounter++;

                        if (delayCounter % ExtendedObservableCollection<ItemType>.paginationSize == 0)
                        {
                            await Task.Delay(ExtendedObservableCollection<ItemType>.paginationTimeoutMilliseconds);
                        }
                    }
                }
            }
        }

        public void ApplyReplaceAction(NotifyCollectionChangedEventArgs changedArgs)
        {
            // current implementation only handles a single item being replaced
            CheckReentrancy();

            SetItem(changedArgs.OldStartingIndex, (ItemType)changedArgs.NewItems[0]);

            OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
            // workaround for Replace notification requires specifying new item as the old item
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, changedArgs.NewItems[0], changedArgs.NewItems[0], changedArgs.OldStartingIndex));
        }

        public void ApplyResetAction(NotifyCollectionChangedEventArgs changedArgs)
        {
            CheckReentrancy();

            this.Items.Clear();

            OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
            OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public async Task ReplaceAll(IEnumerable<ItemType> collection, int length)
        {
            CheckReentrancy();

            this.Items.Clear();

            foreach (ItemType item in collection.Take(ExtendedObservableCollection<ItemType>.paginationSize))
            {
                this.Items.Add(item);
            }

            OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
            OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

            await Task.Delay(ExtendedObservableCollection<ItemType>.paginationTimeoutMilliseconds);

            int index = ExtendedObservableCollection<ItemType>.paginationSize;

            foreach (ItemType item in collection.Skip(ExtendedObservableCollection<ItemType>.paginationSize))
            {
                this.Items.Add(item);

                OnPropertyChanged(new PropertyChangedEventArgs(nameof(Count)));
                OnPropertyChanged(new PropertyChangedEventArgs(PropertyName_IndexerName));
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));

                index++;

                if (index % ExtendedObservableCollection<ItemType>.paginationSize == 0)
                {
                    await Task.Delay(ExtendedObservableCollection<ItemType>.paginationTimeoutMilliseconds);
                }
            }
        }

        #endregion
    }
}
