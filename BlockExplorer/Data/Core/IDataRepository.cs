﻿using System.Collections.Generic;

namespace BlockExplorer.Data.Core
{
    public interface IDataRepository<T> where T : class
    {
        bool Contains(string id);

        bool EnsureIsInDatabase(T entity);

        bool EnsureAreInDatabase(IEnumerable<T> entityEnumerable);

        T GetEntity(string id);

        T[] GetEntities(IEnumerable<string> entityIds);

        bool UpdateInDatabase(T entity);

        bool UpdateInDatabase(IEnumerable<T> entities);
    }
}
