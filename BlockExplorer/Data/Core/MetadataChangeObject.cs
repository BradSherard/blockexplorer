﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using BlockExplorer.Services;
using System.Reflection;
using Nito.AsyncEx;
using BlockExplorer.ViewModels.Core;

namespace BlockExplorer.Data.Core
{
    public abstract class MetadataChangeObject : DatabasePropertyChangeObject, INotifyMetadataChanged, IDisposable
    {
        #region types

        /// <summary>
        /// Describes the initialization state of the object's properties.
        /// Lite means all constant properties are populated, and all reference data is initialized to some default non null value;
        /// Full means all properties are populated. Should rely on manual consideration of errors or cancellations to prevent false full metadata, not just population of properties
        /// </summary>
        public enum MetadataLevels
        {
            Default,
            Lite,
            Full
        }

        public struct MetadataResponse
        {
            public MetadataPageViewModelBase.DataErrorTypes ErrorType {get; set;}
            public object RawResponseObject { get; set; }
        }

        public delegate void MetadataChangedAction(MetadataLevels oldValue, MetadataLevels newValue);

        #endregion

        #region events

        public event EventHandler<MetadataChangedEventArgs> MetadataChanged;

        #endregion

        #region data

        private readonly AsyncLock updatingMetadataMutex = new AsyncLock();
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private CancellationToken cancellationToken;

        #endregion

        #region properties

        public abstract MetadataLevels MetadataLevel { get; set; }

        /// <summary>
        /// only change this inside Ensure Metadata. Won't update database since not included in propertyname list for databasepropertychangeobject
        /// </summary>
        public abstract bool IsUpdatingMetadata { get; protected set; }

        #endregion

        public MetadataChangeObject(string[] propertyNames) : base(propertyNames)
        {
        }

        #region public methods

        /// <summary>
        /// Properties that are reference objects retrieved from DB might be unpopulated. If so, then reduce metadata level
        /// to the level at which this object would be if that metadata property were null.
        /// </summary>
        /// <returns></returns>
        public abstract MetadataLevels DetermineMetadataLevelRestoredFromDatabase();

        public async Task<MetadataPageViewModelBase.DataErrorTypes> ChangeMetadata_IsMarkedForStorage(bool isMarked)
        {
            using (await this.updatingMetadataMutex.LockAsync())
            {
                this.IsUpdatingMetadata = true;

                this.IsMarkedForStorage = isMarked;

                this.IsUpdatingMetadata = false;
            }

            return MetadataPageViewModelBase.DataErrorTypes.None;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> ChangeMetadataAsyncFunctionAsync(Func<CancellationToken, Task<MetadataResponse>> function, bool evaluateMetadata = false)
        {
            this.cancellationToken = this.cancellationTokenSource.Token;
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            using (await this.updatingMetadataMutex.LockAsync())
            {
                this.IsUpdatingMetadata = true;

                response = await function(this.cancellationToken);

                if (evaluateMetadata)
                {
                    DetermineMetadataLevel();
                }

                this.IsUpdatingMetadata = false;
            }

            return response.ErrorType;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> ChangeMetadataSyncFunctionAsync(Func<MetadataResponse> function, bool evaluateMetadata = false)
        {
            this.cancellationToken = this.cancellationTokenSource.Token;
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            using (await this.updatingMetadataMutex.LockAsync())
            {
                this.IsUpdatingMetadata = true;

                response = function.Invoke();

                if (evaluateMetadata)
                {
                    DetermineMetadataLevel();
                }

                this.IsUpdatingMetadata = false;
            }

            return response.ErrorType;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> EnsureLiteMetadata()
        {
            this.cancellationToken = this.cancellationTokenSource.Token;
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            if (this.MetadataLevel == MetadataLevels.Default)
            {
                using (await this.updatingMetadataMutex.LockAsync())
                {
                    if ((this.MetadataLevel == MetadataLevels.Default))
                    {
                        this.IsUpdatingMetadata = true;

                        response = await GetLiteMetadataAsync(this.cancellationToken);

                        DetermineMetadataLevel();

                        this.IsUpdatingMetadata = false;
                    }
                }
            }

            return response.ErrorType;
        }

        public async Task<MetadataPageViewModelBase.DataErrorTypes> EnsureFullMetadata()
        {
            this.cancellationToken = this.cancellationTokenSource.Token;
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            if (this.MetadataLevel != MetadataLevels.Full)
            {
                using (await this.updatingMetadataMutex.LockAsync())
                {
                    if ((this.MetadataLevel != MetadataLevels.Full))
                    {
                        this.IsUpdatingMetadata = true;
                        bool updateLite = this.MetadataLevel == MetadataLevels.Default;

                        if (updateLite)
                        {
                            response = await GetLiteMetadataAsync(this.cancellationToken);

                            if (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None)
                            {
                                DetermineMetadataLevel(skipFullCheck: true);
                            }
                        }

                        if (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None)
                        {
                            response = await GetFullMetadataAsync(this.cancellationToken, response.RawResponseObject);

                            if (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None)
                            {
                                DetermineMetadataLevel(reevaluateCurrentLevel: !updateLite);
                            }
                        }

                        this.IsUpdatingMetadata = false;
                    }

                    this.cancellationTokenSource.Dispose();
                    this.cancellationTokenSource = new CancellationTokenSource();
                }
            }      

            return response.ErrorType;
        }

        public void CancelMetadataUpdate()
        {
            this.cancellationTokenSource.Cancel();
        }

        public bool IsAllLiteMetadataPropertiesPopulated()
        {
            // edge case: no properties for lite?
            bool isAllMetadataPopulated = true;

            foreach (PropertyInfo liteMetadataProperty in this.GetType().GetProperties().Where(property => property.GetCustomAttributes(typeof(IsMetadataPropertyAttribute), false).Any(attribute => (attribute as IsMetadataPropertyAttribute)?.MetadataLevel == MetadataLevels.Lite)))
            {
                if (liteMetadataProperty.GetValue(this) == null)
                {
                    isAllMetadataPopulated = false;
                    break;
                }
            }

            return isAllMetadataPopulated;
        }

        public bool IsAllFullMetadataPropertiesPopulated()
        {
            // edge case: no properties for full?
            bool isAllMetadataPopulated = true;

            foreach (PropertyInfo fullMetadataProperty in this.GetType().GetProperties().Where(property => property.GetCustomAttributes(typeof(IsMetadataPropertyAttribute), false).Any(attribute => (attribute as IsMetadataPropertyAttribute)?.MetadataLevel == MetadataLevels.Full)))
            {
                if (fullMetadataProperty.GetValue(this) == null)
                {
                    isAllMetadataPopulated = false;
                    break;
                }
            }

            return isAllMetadataPopulated;
        }

        #endregion

        #region non-public methods

        protected void NotifyMetadataChanged(MetadataLevels oldValue, MetadataLevels newValue, object sender = null)
        {
            if (this.MetadataChanged != null)
            {
                AsyncDispatcher.Instance.InvokeAsync(
                    delegate
                    {
                        this.MetadataChanged?.Invoke(sender ?? this, new MetadataChangedEventArgs(oldValue, newValue));
                    }
                );
            }
        }

        protected bool SetMetadata(ref MetadataLevels propertyValue, ref MetadataLevels newValue, ChangedAction<MetadataLevels> changedAction = null, MetadataChangedAction metadataChangedAction = null)
        {
            bool isValueChanged = false;
            MetadataLevels oldValue = propertyValue;

            if (!Equals(propertyValue, newValue))
            {
                lock (this.mutex)
                {
                    propertyValue = newValue;
                    isValueChanged = true;

                    NotifyPropertyChanged(this, nameof(this.MetadataLevel));
                    NotifyMetadataChanged(oldValue, newValue);

                    changedAction?.Invoke(oldValue, newValue);
                    metadataChangedAction?.Invoke(oldValue, newValue);
                }
            }

            return isValueChanged;
        }

        protected abstract Task<MetadataResponse> GetLiteMetadataAsync(CancellationToken cancellationToken);

        protected abstract Task<MetadataResponse> GetFullMetadataAsync(CancellationToken cancellationToken, object rawResponseObject);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="reevaluateCurrentLevel">Skips confirming metadata level that is lower or equal to current level if parameter is false, otherwise metadata levels are rechecked.</param> 
        /// <param name="skipFullCheck">Skips confirming metadata level above lite. Note, this will set the metadata to lite erroneously if used on something with full metadata.</param>
        private void DetermineMetadataLevel(bool reevaluateCurrentLevel = true, bool skipFullCheck = false)
        {
            MetadataLevels newLevel = MetadataLevels.Default;

            if ((!reevaluateCurrentLevel && (this.MetadataLevel != MetadataLevels.Default)) || IsAllLiteMetadataPropertiesPopulated())
            {
                newLevel = MetadataLevels.Lite;

                if (!skipFullCheck)
                {
                    if ((!reevaluateCurrentLevel && (this.MetadataLevel == MetadataLevels.Full)) || IsAllFullMetadataPropertiesPopulated())
                    {
                        newLevel = MetadataLevels.Full;
                    }
                }
            }

            this.MetadataLevel = newLevel;
        }

        public void Dispose()
        {
            cancellationTokenSource.Dispose();
        }

        #endregion
    }
}
