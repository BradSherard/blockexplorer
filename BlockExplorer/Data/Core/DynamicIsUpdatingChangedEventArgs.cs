﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockExplorer.Data.Core
{
    public class DynamicIsUpdatingChangedEventArgs : EventArgs
    {
        public DynamicIsUpdatingChangedEventArgs(bool oldValue, bool newValue)
        {
            this.NewValue = newValue;
            this.OldValue = oldValue;
        }

        public bool NewValue { get; private set; }
        public bool OldValue { get; private set; }
    }
}
