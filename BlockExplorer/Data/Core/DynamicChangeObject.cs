﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using BlockExplorer.Services;
using System.Reflection;
using Nito.AsyncEx;
using BlockExplorer.ViewModels.Core;
using System.Windows.Threading;

namespace BlockExplorer.Data.Core
{
    public abstract class DynamicChangeObject : PropertyChangeObject, IDisposable
    {
        #region types

        public struct DynamicResponse
        {
            public DynamicPageViewModelBase.DataErrorTypes ErrorType { get; set; }
            public object RawResponseObject { get; set; }
        }

        public delegate void IsUpdatingChangedAction(bool oldValue, bool newValue);

        #endregion

        #region events

        public event EventHandler<DynamicIsUpdatingChangedEventArgs> Updated;

        #endregion

        #region data

        private readonly AsyncLock updatingMutex = new AsyncLock();
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private CancellationToken cancellationToken;
        private TimeSpan updatePeriod;
        protected object rawUpdateObject;
        private System.Timers.Timer updateTimer = new System.Timers.Timer();
        #endregion

        #region properties

        /// <summary>
        /// only change this inside UpdateAsync
        /// </summary>
        public abstract bool IsUpdating { get; protected set; }

        /// <summary>
        /// Only change this inside HandleTimer
        /// </summary>
        public abstract DynamicPageViewModelBase.DataErrorTypes ErrorType { get; protected set; }

        #endregion

        public DynamicChangeObject(TimeSpan updatePeriod)
        {
            this.updatePeriod = updatePeriod;

            this.updateTimer.AutoReset = true;
            this.updateTimer.Interval = this.updatePeriod.TotalMilliseconds;
            this.updateTimer.Elapsed += async (sender, e) => await HandleTimer();
        }

        #region public methods

        public void CancelUpdate()
        {
            this.cancellationTokenSource.Cancel();
        }

        public void RunUpdate()
        {
            ThreadPool.QueueUserWorkItem(
                async delegate
                {
                    await HandleTimer();
                }
            );
        }

        public void StartUpdating(bool runImmediately = true)
        {
            if (!this.updateTimer.Enabled)
            {
                this.updateTimer.Start();

                if (runImmediately)
                {
                    RunUpdate();
                }
            }
        }

        public void StopUpdating()
        {
            if (this.updateTimer.Enabled)
            {
                this.updateTimer.Stop();
            }
        }

        #endregion

        #region non-public methods

        private async Task HandleTimer()
        {
            if (!this.IsUpdating)
            {
                using (await this.updatingMutex.LockAsync())
                {
                    if (!this.IsUpdating)
                    {
                        this.IsUpdating = true;

                        this.ErrorType = await RunUpdateAsync();

                        this.IsUpdating = false;
                    }
                }       
            }
        }

        private async Task<DynamicPageViewModelBase.DataErrorTypes> RunUpdateAsync()
        {
            this.cancellationToken = this.cancellationTokenSource.Token;
            DynamicResponse response = new DynamicResponse()
            {
                ErrorType = DynamicPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            response = await UpdateAsync(this.cancellationToken);

            if (response.ErrorType == DynamicPageViewModelBase.DataErrorTypes.None)
            {
                //DetermineMetadataLevel(skipFullCheck: true);
            }

            this.cancellationTokenSource.Dispose();
            this.cancellationTokenSource = new CancellationTokenSource();

            return response.ErrorType;
        }

        protected void NotifyIsUpdatingChanged(bool oldValue, bool newValue, object sender = null)
        {
            if (this.Updated != null)
            {
                AsyncDispatcher.Instance.InvokeAsync(
                    delegate
                    {
                        this.Updated?.Invoke(sender ?? this, new DynamicIsUpdatingChangedEventArgs(oldValue, newValue));
                    }
                );
            }
        }

        protected bool SetIsUpdating(ref bool propertyValue, ref bool newValue, ChangedAction<bool> changedAction = null, IsUpdatingChangedAction isUpdatingChangedAction = null)
        {
            bool isValueChanged = false;
            bool oldValue = propertyValue;

            if (!Equals(propertyValue, newValue))
            {
                lock (this.mutex)
                {
                    propertyValue = newValue;
                    isValueChanged = true;

                    NotifyPropertyChanged(this, nameof(this.IsUpdating));
                    NotifyIsUpdatingChanged(oldValue, newValue);

                    changedAction?.Invoke(oldValue, newValue);
                    isUpdatingChangedAction?.Invoke(oldValue, newValue);
                }
            }

            return isValueChanged;
        }

        /// <summary>
        /// The specific logic for the implementing child class. The call is wrapped in all the management logic,
        /// requiring the call to only deal with the logic of the child class itself.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected abstract Task<DynamicResponse> UpdateAsync(CancellationToken cancellationToken);

        public void Dispose()
        {
            cancellationTokenSource.Dispose();
        }

        #endregion
    }
}
