﻿using System.Collections.Generic;
using SQLite.Net.Attributes;


namespace BlockExplorer.Data.SQL
{
    [Table("Transfer")]
    public class SQLiteTransfer
    {
        [Column(nameof(SQLiteTransfer.IsTransferIncoming))]
        public bool? IsTransferIncoming { get; set; }

        [Column(nameof(SQLiteTransfer.AddressId))]
        public string AddressId { get; set; }

        [Column(nameof(SQLiteTransfer.TransactionId))]
        public string TransactionId { get; set; }

        [Column(nameof(SQLiteTransfer.Amount))]
        public long? Amount { get; set; }

        [Column(nameof(SQLiteTransfer.TransferId))]
        [PrimaryKey, NotNull]
        public string TransferId { get; set; }

        #region public methods

        public static SQLiteTransfer SQLiteTransferFromTransfer(Transfer transfer)
        {
            SQLiteTransfer sqliteTransfer = null;

            if (transfer != null)
            {
                sqliteTransfer = new SQLiteTransfer
                {
                    IsTransferIncoming = transfer.IsTransferIncoming,
                    AddressId = transfer.AddressId,
                    TransactionId = transfer.TransactionId,
                    Amount = transfer.Amount,
                    TransferId = transfer.TransferId,
                };
            }

            return sqliteTransfer;
        }

        public static List<SQLiteTransfer> SQLiteTransfersFromTransfers(IEnumerable<Transfer> transfers)
        {
            List<SQLiteTransfer> sqliteTransfers = new List<SQLiteTransfer>();

            if (transfers != null)
            {
                foreach (Transfer transfer in transfers)
                {
                    sqliteTransfers.Add(SQLiteTransfer.SQLiteTransferFromTransfer(transfer));
                }
            }

            return sqliteTransfers;
        }

        #endregion

        #region non-public methods

        #endregion
    }
}
