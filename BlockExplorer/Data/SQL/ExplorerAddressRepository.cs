﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.Services;
using System.Diagnostics;

namespace BlockExplorer.Data.SQL
{
    public class ExplorerAddressRepository : IDataRepository<ExplorerAddress>
    {
        #region queries

        private static readonly string Query_AddressIds = @"select AddressId from ExplorerAddress";
        private static readonly string Query_ExplorerAddressWhereAddressId = @"select * from ExplorerAddress where AddressId = '{0}'";
        private static readonly string Query_ExplorerAddressesWhereAddressIds = @"select * from ExplorerAddress where AddressId in {0}";
        private static readonly string Query_ExplorerAddressesWhereIsMarkedForStorage = @"select * from ExplorerAddress where IsMarkedForStorage = 1";

        #endregion

        #region data

        private readonly Database database;

        #endregion

        #region properties

        public string[] AddressIds
        {
            get
            {
                var sqliteExplorerAddresses = this.database.ExecuteQuery<SQLiteExplorerAddress>(ExplorerAddressRepository.Query_AddressIds);

                return sqliteExplorerAddresses.Select(explorerAddress => explorerAddress.AddressId).ToArray();
            }
        }

        public ExplorerAddress[] ExplorerAddresses_IsMarkedForStorage
        {
            get
            {
                return ExplorerAddress.ExplorerAddressesFromSQLiteExplorerAddresses(this.database.ExecuteQuery<SQLiteExplorerAddress>(Query_ExplorerAddressesWhereIsMarkedForStorage));
            }
        }

        #endregion

        #region constructor

        public ExplorerAddressRepository(Database database)
        {
            this.database = database;
        }

        #endregion

        #region public methods

        public bool Contains(string addressId)
        {
            bool containsExplorerAddress = false;

            if (!String.IsNullOrEmpty(addressId))
            {
                string query = String.Format(Query_ExplorerAddressWhereAddressId, addressId);

                containsExplorerAddress = this.database.ExecuteQuery<SQLiteExplorerAddress>(query).Any();
            }

            return containsExplorerAddress;
        }

        public bool EnsureIsInDatabase(ExplorerAddress explorerAddress)
        {
            bool anyAddedOrRemoved = false;

            if (explorerAddress != null)
            {
                lock (DatabaseRepository.Mutex)
                {
                    this.database.SubmitChangesIfAny();

                    bool isInDatabase = this.Contains(explorerAddress.AddressId.ToWif());

                    if (isInDatabase)
                    {
                        if (explorerAddress.CheckAndResetIsUpdateNeeded())
                        {
                            try
                            {
                                this.database.UpdateOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressFromExplorerAddress(explorerAddress));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            this.database.InsertOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressFromExplorerAddress(explorerAddress));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }
                    }

                    this.database.SubmitChangesIfAny();
                }
            }

            return anyAddedOrRemoved;
        }

        public bool EnsureAreInDatabase(IEnumerable<ExplorerAddress> explorerAddresses)
        {
            bool anyAddedOrRemoved = false;

            if (explorerAddresses != null)
            {
                List<ExplorerAddress> explorerAddressesToSave = explorerAddresses.Distinct().ToList();

                while (explorerAddressesToSave.Any())
                {
                    ExplorerAddress[] batchToSave = explorerAddressesToSave.Take(this.database.BatchSize).ToArray();

                    explorerAddressesToSave.RemoveRange(0, Math.Min(this.database.BatchSize, batchToSave.Count()));

                    lock (DatabaseRepository.Mutex)
                    {
                        this.database.SubmitChangesIfAny();

                        ExplorerAddress[] batchInDatabase = batchToSave.Where(explorerAddress => this.Contains(explorerAddress.AddressId.ToWif())).ToArray();
                        ExplorerAddress[] batchToUpdate = batchInDatabase.Where(explorerAddress => explorerAddress.CheckAndResetIsUpdateNeeded()).ToArray();
                        ExplorerAddress[] batchToInsert = batchToSave.Except(batchInDatabase).ToArray();

                        if (batchToUpdate.Any())
                        {
                            try
                            {
                                this.database.UpdateAllOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressesFromExplorerAddresses(batchToUpdate));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        if (batchToInsert.Any())
                        {
                            try
                            {
                                this.database.InsertAllOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressesFromExplorerAddresses(batchToInsert));
                                anyAddedOrRemoved = true;
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        public ExplorerAddress GetEntity(string addressId)
        {
            ExplorerAddress explorerAddress = null;

            if (!String.IsNullOrEmpty(addressId))
            {
                string query = String.Format(ExplorerAddressRepository.Query_ExplorerAddressWhereAddressId, addressId);

                explorerAddress = ExplorerAddress.ExplorerAddressFromSQLiteExplorerAddress(this.database.ExecuteQuery<SQLiteExplorerAddress>(query).FirstOrDefault());
            }

            return explorerAddress;
        }

        public ExplorerAddress[] GetEntities(IEnumerable<string> explorerAddressIds)
        {
            ExplorerAddress[] explorerAddresses;

            if (explorerAddressIds != null)
            {
                string[] explorerAddressIdsArray = (from explorerAddressId in explorerAddressIds
                                                    where !String.IsNullOrEmpty(explorerAddressId)
                                                    select explorerAddressId).ToArray();

                StringBuilder ids = new StringBuilder();
                ids.AppendIds(explorerAddressIdsArray);

                string query = String.Format(ExplorerAddressRepository.Query_ExplorerAddressesWhereAddressIds, ids);

                explorerAddresses = ExplorerAddress.ExplorerAddressesFromSQLiteExplorerAddresses(this.database.ExecuteQuery<SQLiteExplorerAddress>(query).ToArray());

            }
            else
            {
                explorerAddresses = new ExplorerAddress[] { };
            }

            return explorerAddresses;
        }

        public bool UpdateInDatabase(ExplorerAddress explorerAddress)
        {
            bool anyAddedOrRemoved = false;

            if (explorerAddress != null)
            {
                anyAddedOrRemoved = this.UpdateInDatabase(new ExplorerAddress[] { explorerAddress });
            }

            return anyAddedOrRemoved;
        }

        public bool UpdateInDatabase(IEnumerable<ExplorerAddress> explorerAddresses)
        {
            bool anyAddedOrRemoved = false;

            if (explorerAddresses != null && explorerAddresses.Any())
            {
                lock (DatabaseRepository.Mutex)
                {
                    List<ExplorerAddress> distinctExplorerAddresses = explorerAddresses.Distinct().ToList();
                    string[] addressIds = this.AddressIds;
                    List<ExplorerAddress> explorerAddressesToKeep = distinctExplorerAddresses.Where(explorerAddress => !explorerAddress.CanBeRemovedFromDatabase()).ToList();
                    List<ExplorerAddress> explorerAddressesToUpdate = explorerAddressesToKeep.Where(explorerAddress => explorerAddress.CheckAndResetIsUpdateNeeded()).ToList();
                    List<ExplorerAddress> explorerAddressesToDelete = distinctExplorerAddresses.Except(explorerAddressesToKeep).Where(explorerAddressToDelete => addressIds.Contains(explorerAddressToDelete.AddressId.ToWif())).ToList();

                    if (explorerAddressesToUpdate.Any())
                    {
                        this.database.UpdateAllOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressesFromExplorerAddresses(explorerAddressesToUpdate));
                    }

                    this.database.SubmitChangesIfAny();

                    while (explorerAddressesToDelete.Any())
                    {
                        ExplorerAddress[] batchToDelete = explorerAddressesToDelete.Take(this.database.BatchSize).ToArray();

                        explorerAddressesToDelete.RemoveRange(0, Math.Min(this.database.BatchSize, batchToDelete.Count()));

                        try
                        {
                            this.database.DeleteAllOnSubmit(SQLiteExplorerAddress.SQLiteExplorerAddressesFromExplorerAddresses(batchToDelete));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        #endregion

    }
}
