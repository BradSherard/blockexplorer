﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.Services;
using System.Diagnostics;

namespace BlockExplorer.Data.SQL
{
    public class TransferRepository : IDataRepository<Transfer>
    {
        #region queries

        private static readonly string Query_TransferIds = @"select TransferId from Transfer";
        private static readonly string Query_TransferWhereTransferId = @"select * from Transfer where TransferId = '{0}'";
        private static readonly string Query_TransfersWhereTransferIds = @"select * from Transfer where TransferId in {0}";

        #endregion

        #region data

        private readonly Database database;

        #endregion

        #region properties

        public string[] TransferIds
        {
            get
            {
                var sqliteTransfers = this.database.ExecuteQuery<SQLiteTransfer>(TransferRepository.Query_TransferIds);

                return sqliteTransfers.Select(transfer => transfer.TransferId).ToArray();
            }
        }

        #endregion

        #region constructor

        public TransferRepository(Database database)
        {
            this.database = database;
        }

        #endregion

        #region public methods

        public bool Contains(string transferId)
        {
            bool containsTransfer = false;

            if (!String.IsNullOrEmpty(transferId))
            {
                string query = String.Format(Query_TransferWhereTransferId, transferId);

                containsTransfer = this.database.ExecuteQuery<SQLiteTransfer>(query).Any();
            }

            return containsTransfer;
        }

        public bool EnsureIsInDatabase(Transfer transfer)
        {
            bool anyAddedOrRemoved = false;

            if (transfer != null)
            {
                lock (DatabaseRepository.Mutex)
                {
                    this.database.SubmitChangesIfAny();

                    bool isInDatabase = this.Contains(transfer.TransferId);

                    if (isInDatabase)
                    {
                        if (transfer.CheckAndResetIsUpdateNeeded())
                        {
                            try
                            {
                                this.database.UpdateOnSubmit(SQLiteTransfer.SQLiteTransferFromTransfer(transfer));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            this.database.InsertOnSubmit(SQLiteTransfer.SQLiteTransferFromTransfer(transfer));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }
                    }

                    this.database.SubmitChangesIfAny();
                }
            }

            return anyAddedOrRemoved;
        }

        public bool EnsureAreInDatabase(IEnumerable<Transfer> transfers)
        {
            bool anyAddedOrRemoved = false;

            if (transfers != null)
            {
                List<Transfer> transfersToSave = transfers.Distinct().ToList();

                while (transfersToSave.Any())
                {
                    Transfer[] batchToSave = transfersToSave.Take(this.database.BatchSize).ToArray();

                    transfersToSave.RemoveRange(0, Math.Min(this.database.BatchSize, batchToSave.Count()));

                    lock (DatabaseRepository.Mutex)
                    {
                        this.database.SubmitChangesIfAny();

                        Transfer[] batchInDatabase = batchToSave.Where(transfer => this.Contains(transfer.TransferId)).ToArray();
                        Transfer[] batchToUpdate = batchInDatabase.Where(transfer => transfer.CheckAndResetIsUpdateNeeded()).ToArray();
                        Transfer[] batchToInsert = batchToSave.Except(batchInDatabase).ToArray();

                        if (batchToUpdate.Any())
                        {
                            try
                            {
                                this.database.UpdateAllOnSubmit(SQLiteTransfer.SQLiteTransfersFromTransfers(batchToUpdate));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        if (batchToInsert.Any())
                        {
                            try
                            {
                                this.database.InsertAllOnSubmit(SQLiteTransfer.SQLiteTransfersFromTransfers(batchToInsert));
                                anyAddedOrRemoved = true;
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        public Transfer GetEntity(string transferId)
        {
            Transfer transfer = null;

            if (!String.IsNullOrEmpty(transferId))
            {
                string query = String.Format(TransferRepository.Query_TransferWhereTransferId, transferId);

                transfer = Transfer.TransferFromSQLiteTransfer(this.database.ExecuteQuery<SQLiteTransfer>(query).FirstOrDefault());
            }

            return transfer;
        }

        public Transfer[] GetEntities(IEnumerable<string> transferIds)
        {
            Transfer[] transfers;

            if (transferIds != null)
            {
                string[] transferIdsArray = (from transferId in transferIds
                                             where !String.IsNullOrEmpty(transferId)
                                             select transferId).ToArray();

                StringBuilder ids = new StringBuilder();
                ids.AppendIds(transferIdsArray);

                string query = String.Format(TransferRepository.Query_TransfersWhereTransferIds, ids);

                transfers = Transfer.TransfersFromSQLiteTransfers(this.database.ExecuteQuery<SQLiteTransfer>(query).ToArray());
            }
            else
            {
                transfers = new Transfer[] { };
            }

            return transfers;
        }

        public bool UpdateInDatabase(Transfer transfer)
        {
            bool anyAddedOrRemoved = false;

            if (transfer != null)
            {
                anyAddedOrRemoved = this.UpdateInDatabase(new Transfer[] { transfer });
            }

            return anyAddedOrRemoved;
        }

        public bool UpdateInDatabase(IEnumerable<Transfer> transfers)
        {
            bool anyAddedOrRemoved = false;

            if (transfers != null && transfers.Any())
            {
                lock (DatabaseRepository.Mutex)
                {
                    List<Transfer> distinctTransfers = transfers.Distinct().ToList();
                    string[] transferIds = this.TransferIds;
                    List<Transfer> transfersToKeep = distinctTransfers.Where(transfer => !transfer.CanBeRemovedFromDatabase()).ToList();
                    List<Transfer> transfersToUpdate = transfersToKeep.Where(transfer => transfer.CheckAndResetIsUpdateNeeded()).ToList();
                    List<Transfer> transfersToDelete = distinctTransfers.Except(transfersToKeep).Where(transferToDelete => transferIds.Contains(transferToDelete.TransferId)).ToList();

                    if (transfersToUpdate.Any())
                    {
                        this.database.UpdateAllOnSubmit(SQLiteTransfer.SQLiteTransfersFromTransfers(transfersToUpdate));
                    }

                    this.database.SubmitChangesIfAny();

                    while (transfersToDelete.Any())
                    {
                        Transfer[] batchToDelete = transfersToDelete.Take(this.database.BatchSize).ToArray();

                        transfersToDelete.RemoveRange(0, Math.Min(this.database.BatchSize, batchToDelete.Count()));

                        try
                        {
                            this.database.DeleteAllOnSubmit(SQLiteTransfer.SQLiteTransfersFromTransfers(batchToDelete));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        #endregion

    }
}
