﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
using BlockExplorer.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BlockExplorer.Data.SQL
{
    [Table("ExplorerTransaction")]
    public class SQLiteExplorerTransaction
    {
        [Column(nameof(SQLiteExplorerTransaction.BlockId))]
        public string BlockId { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.Fee))]
        public long? Fee { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.FirstSeen))]
        public DateTime? FirstSeen { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.IsMarkedForStorage))]
        public bool IsMarkedForStorage { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.MetadataLevel))]
        public ExplorerTransaction.MetadataLevels MetadataLevel { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.ReceivedIds))]
        public byte[] ReceivedIds { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.SpentIds))]
        public byte[] SpentIds { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.TotalSpent))]
        public long? TotalSpent { get; set; }

        [Column(nameof(SQLiteExplorerTransaction.TransactionId))]
        [PrimaryKey, NotNull]
        public string TransactionId { get; set; }

        #region public methods

        public static SQLiteExplorerTransaction SQLiteExplorerTransactionFromExplorerTransaction(ExplorerTransaction transaction)
        {
            SQLiteExplorerTransaction sqliteExplorerTransaction = null;

            if (transaction != null)
            {
                sqliteExplorerTransaction = new SQLiteExplorerTransaction
                {
                    BlockId = transaction.BlockId.ToString(),
                    Fee = transaction.Fee,
                    FirstSeen = transaction.FirstSeen,
                    IsMarkedForStorage = transaction.IsMarkedForStorage,
                    MetadataLevel = transaction.MetadataLevel,
                    ReceivedIds = SerializeTransactionIds(transaction.ReceivedIds.Keys.ToList()),
                    SpentIds = SerializeTransactionIds(transaction.SpentIds.Keys.ToList()),
                    TotalSpent = transaction.TotalSpent,
                    TransactionId = transaction.TransactionId.ToString(),
                };
            }

            return sqliteExplorerTransaction;
        }

        public static List<SQLiteExplorerTransaction> SQLiteExplorerTransactionsFromExplorerTransactions(IEnumerable<ExplorerTransaction> explorerTransactions)
        {
            List<SQLiteExplorerTransaction> sqliteExplorerTransactions = new List<SQLiteExplorerTransaction>();

            if ((explorerTransactions != null) && (explorerTransactions.Any()))
            {
                foreach (ExplorerTransaction explorerTransaction in explorerTransactions)
                {
                    sqliteExplorerTransactions.Add(SQLiteExplorerTransaction.SQLiteExplorerTransactionFromExplorerTransaction(explorerTransaction));
                }
            }

            return sqliteExplorerTransactions;
        }

        #endregion

        #region non-public methods

        private static byte[] SerializeTransactionIds(List<String> transactionIds)
        {
            byte[] serializedData;

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, transactionIds);
                serializedData = stream.ToArray();
            }

            return serializedData;
        }

        #endregion
    }
}
