﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlockExplorer.Data.Core;
using BlockExplorer.Services;
using System.Diagnostics;

namespace BlockExplorer.Data.SQL
{
    public class ExplorerTransactionRepository : IDataRepository<ExplorerTransaction>
    {
        #region queries

        private static readonly string Query_TransactionIds = @"select TransactionId from ExplorerTransaction";
        private static readonly string Query_ExplorerTransactionWhereTransactionId = @"select * from ExplorerTransaction where TransactionId = '{0}'";
        private static readonly string Query_ExplorerTransactionsWhereTransactionIds = @"select * from ExplorerTransaction where TransactionId in {0}";

        #endregion

        #region data

        private readonly Database database;

        #endregion

        #region properties

        public string[] TransactionIds
        {
            get
            {
                var sqliteExplorerTransactions = this.database.ExecuteQuery<SQLiteExplorerTransaction>(ExplorerTransactionRepository.Query_TransactionIds);

                return sqliteExplorerTransactions.Select(explorerTransaction => explorerTransaction.TransactionId).ToArray();
            }
        }

        #endregion

        #region constructor

        public ExplorerTransactionRepository(Database database)
        {
            this.database = database;
        }

        #endregion

        #region public methods

        public bool Contains(string TransactionId)
        {
            bool containsExplorerTransaction = false;

            if (!String.IsNullOrEmpty(TransactionId))
            {
                string query = String.Format(Query_ExplorerTransactionWhereTransactionId, TransactionId);

                containsExplorerTransaction = this.database.ExecuteQuery<SQLiteExplorerTransaction>(query).Any();
            }

            return containsExplorerTransaction;
        }

        public bool EnsureIsInDatabase(ExplorerTransaction explorerTransaction)
        {
            bool anyAddedOrRemoved = false;

            if (explorerTransaction != null)
            {
                lock (DatabaseRepository.Mutex)
                {
                    this.database.SubmitChangesIfAny();

                    bool isInDatabase = this.Contains(explorerTransaction.TransactionId.ToString());

                    if (isInDatabase)
                    {
                        if (explorerTransaction.CheckAndResetIsUpdateNeeded())
                        {
                            try
                            {
                                this.database.UpdateOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionFromExplorerTransaction(explorerTransaction));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            this.database.InsertOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionFromExplorerTransaction(explorerTransaction));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }
                    }

                    this.database.SubmitChangesIfAny();
                }
            }

            return anyAddedOrRemoved;
        }

        public bool EnsureAreInDatabase(IEnumerable<ExplorerTransaction> explorerTransactions)
        {
            bool anyAddedOrRemoved = false;

            if (explorerTransactions != null)
            {
                List<ExplorerTransaction> explorerTransactionsToSave = explorerTransactions.Distinct().ToList();

                while (explorerTransactionsToSave.Any())
                {
                    ExplorerTransaction[] batchToSave = explorerTransactionsToSave.Take(this.database.BatchSize).ToArray();

                    explorerTransactionsToSave.RemoveRange(0, Math.Min(this.database.BatchSize, batchToSave.Count()));

                    lock (DatabaseRepository.Mutex)
                    {
                        this.database.SubmitChangesIfAny();

                        ExplorerTransaction[] batchInDatabase = batchToSave.Where(explorerTransaction => this.Contains(explorerTransaction.TransactionId.ToString())).ToArray();
                        ExplorerTransaction[] batchToUpdate = batchInDatabase.Where(explorerTransaction => explorerTransaction.CheckAndResetIsUpdateNeeded()).ToArray();
                        ExplorerTransaction[] batchToInsert = batchToSave.Except(batchInDatabase).ToArray();

                        if (batchToUpdate.Any())
                        {
                            try
                            {
                                this.database.UpdateAllOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionsFromExplorerTransactions(batchToUpdate));
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        if (batchToInsert.Any())
                        {
                            try
                            {
                                this.database.InsertAllOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionsFromExplorerTransactions(batchToInsert));
                                anyAddedOrRemoved = true;
                            }
                            catch (InvalidOperationException exception)
                            {
                                Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                            }
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        public ExplorerTransaction GetEntity(string TransactionId)
        {
            ExplorerTransaction explorerTransaction = null;

            if (!String.IsNullOrEmpty(TransactionId))
            {
                string query = String.Format(ExplorerTransactionRepository.Query_ExplorerTransactionWhereTransactionId, TransactionId);

                explorerTransaction = ExplorerTransaction.ExplorerTransactionFromSQLiteExplorerTransaction(this.database.ExecuteQuery<SQLiteExplorerTransaction>(query).FirstOrDefault());
            }

            return explorerTransaction;
        }

        public ExplorerTransaction[] GetEntities(IEnumerable<string> explorerTransactionIds)
        {
            ExplorerTransaction[] explorerTransactions;

            if (explorerTransactionIds != null)
            {
                string[] explorerTransactionIdsArray = (from explorerTransactionId in explorerTransactionIds
                                                    where !String.IsNullOrEmpty(explorerTransactionId)
                                                    select explorerTransactionId).ToArray();

                StringBuilder ids = new StringBuilder();
                ids.AppendIds(explorerTransactionIdsArray);

                string query = String.Format(ExplorerTransactionRepository.Query_ExplorerTransactionsWhereTransactionIds, ids);

                explorerTransactions = ExplorerTransaction.ExplorerTransactionsFromSQLiteExplorerTransactions(this.database.ExecuteQuery<SQLiteExplorerTransaction>(query).ToArray());

            }
            else
            {
                explorerTransactions = new ExplorerTransaction[] { };
            }

            return explorerTransactions;
        }

        public bool UpdateInDatabase(ExplorerTransaction explorerTransaction)
        {
            bool anyAddedOrRemoved = false;

            if (explorerTransaction != null)
            {
                anyAddedOrRemoved = this.UpdateInDatabase(new ExplorerTransaction[] { explorerTransaction });
            }

            return anyAddedOrRemoved;
        }

        public bool UpdateInDatabase(IEnumerable<ExplorerTransaction> explorerTransactions)
        {
            bool anyAddedOrRemoved = false;

            if (explorerTransactions != null && explorerTransactions.Any())
            {
                lock (DatabaseRepository.Mutex)
                {
                    List<ExplorerTransaction> distinctExplorerTransactions = explorerTransactions.Distinct().ToList();
                    string[] TransactionIds = this.TransactionIds;
                    List<ExplorerTransaction> explorerTransactionsToKeep = distinctExplorerTransactions.Where(explorerTransaction => !explorerTransaction.CanBeRemovedFromDatabase()).ToList();
                    List<ExplorerTransaction> explorerTransactionsToUpdate = explorerTransactionsToKeep.Where(explorerTransaction => explorerTransaction.CheckAndResetIsUpdateNeeded()).ToList();
                    List<ExplorerTransaction> explorerTransactionsToDelete = distinctExplorerTransactions.Except(explorerTransactionsToKeep).Where(explorerTransactionToDelete => TransactionIds.Contains(explorerTransactionToDelete.TransactionId.ToString())).ToList();

                    if (explorerTransactionsToUpdate.Any())
                    {
                        this.database.UpdateAllOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionsFromExplorerTransactions(explorerTransactionsToUpdate));
                    }

                    this.database.SubmitChangesIfAny();

                    while (explorerTransactionsToDelete.Any())
                    {
                        ExplorerTransaction[] batchToDelete = explorerTransactionsToDelete.Take(this.database.BatchSize).ToArray();

                        explorerTransactionsToDelete.RemoveRange(0, Math.Min(this.database.BatchSize, batchToDelete.Count()));

                        try
                        {
                            this.database.DeleteAllOnSubmit(SQLiteExplorerTransaction.SQLiteExplorerTransactionsFromExplorerTransactions(batchToDelete));
                            anyAddedOrRemoved = true;
                        }
                        catch (InvalidOperationException exception)
                        {
                            Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");
                        }

                        this.database.SubmitChangesIfAny();
                    }
                }
            }

            return anyAddedOrRemoved;
        }

        #endregion

    }
}
