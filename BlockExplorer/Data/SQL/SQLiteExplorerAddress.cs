﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
using BlockExplorer.Data;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BlockExplorer.Data.SQL
{
    [Table("ExplorerAddress")]
    public class SQLiteExplorerAddress
    {
        [Column(nameof(SQLiteExplorerAddress.AddressId))]
        [PrimaryKey, NotNull]
        public string AddressId { get; set; }

        [Column(nameof(SQLiteExplorerAddress.Balance))]
        public long? Balance { get; set; }

        [Column(nameof(SQLiteExplorerAddress.HeightScanned))]
        public int? HeightScanned { get; set; }

        [Column(nameof(SQLiteExplorerAddress.IsMarkedForStorage))]
        public bool IsMarkedForStorage { get; set; }

        [Column(nameof(SQLiteExplorerAddress.LastSyncDate))]
        public DateTime? LastSyncDate { get; set; }

        [Column(nameof(SQLiteExplorerAddress.MetadataLevel))]
        public ExplorerAddress.MetadataLevels MetadataLevel { get; set; }
  
        [Column(nameof(SQLiteExplorerAddress.TotalSent))]
        public long? TotalSent { get; set; }

        [Column(nameof(SQLiteExplorerAddress.TotalReceived))]
        public long? TotalReceived { get; set; }

        [Column(nameof(SQLiteExplorerAddress.TransferIds))]
        public byte[] TransferIds { get; set; }

        #region public methods

        public static SQLiteExplorerAddress SQLiteExplorerAddressFromExplorerAddress(ExplorerAddress address)
        {
            SQLiteExplorerAddress sqliteExplorerAddress = null;

            if (address != null)
            {
                sqliteExplorerAddress = new SQLiteExplorerAddress
                {
                    AddressId = address.AddressId.ToWif(),
                    Balance = address.Balance,
                    HeightScanned = address.HeightScanned,
                    IsMarkedForStorage = address.IsMarkedForStorage,
                    LastSyncDate = address.LastSyncDate,
                    MetadataLevel = address.MetadataLevel,
                    TotalSent = address.TotalSent,
                    TotalReceived = address.TotalReceived,
                    TransferIds = SerializeTransferIds(address.TransferIds.Keys.ToList()),
                };
            }

            return sqliteExplorerAddress;
        }

        public static List<SQLiteExplorerAddress> SQLiteExplorerAddressesFromExplorerAddresses(IEnumerable<ExplorerAddress> explorerAddresses)
        {
            List<SQLiteExplorerAddress> sqliteExplorerAddresses = new List<SQLiteExplorerAddress>();

            if ((explorerAddresses != null) && (explorerAddresses.Any()))
            {
                foreach (ExplorerAddress explorerAddress in explorerAddresses)
                {
                    sqliteExplorerAddresses.Add(SQLiteExplorerAddress.SQLiteExplorerAddressFromExplorerAddress(explorerAddress));
                }
            }

            return sqliteExplorerAddresses;
        }

        #endregion

        #region non-public methods

        private static byte[] SerializeTransferIds(List<String> transferIds)
        {
            byte[] serializedData;

            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, transferIds);
                serializedData = stream.ToArray();
            }

            return serializedData;
        }

        #endregion
    }
}
