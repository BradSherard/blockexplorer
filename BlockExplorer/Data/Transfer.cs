﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBitcoin;
using BlockExplorer.Data.Core;
using BlockExplorer.Data.SQL;
using BlockExplorer.Services;

namespace BlockExplorer.Data
{
    /// <summary>
    /// not a metadataChangeObject so have to manually ensure in db when appropriate
    /// </summary>
    public class Transfer : DatabasePropertyChangeObject
    {
        #region types

        private struct Properties
        {
            public bool? isTransferIncoming;
            public ExplorerAddress Address;
            public ExplorerTransaction transaction;
            public string transferId;
            public long? amount;
        }

        #endregion

        #region constants

        public static readonly string[] DatabasePropertyNames = new string[]
        {
            nameof(Transfer.IsTransferIncoming),
            nameof(Transfer.AddressId),
            nameof(Transfer.TransactionId),
            nameof(Transfer.Amount),
            nameof(Transfer.TransferId),
        };

        #endregion

        #region static data

        protected static References<Transfer, string> references = new References<Transfer, string>(transfer => transfer.TransferId);

        #endregion

        #region data

        private Properties properties = new Properties();

        #endregion

        #region properties

        public bool? IsTransferIncoming
        {
            get
            {
                return this.properties.isTransferIncoming;
            }
            set
            {
                SetProperty(ref this.properties.isTransferIncoming, ref value);
            }
        }

        public ExplorerAddress Address
        {
            get
            {
                return this.properties.Address;
            }
            set
            {
                SetProperty(ref this.properties.Address, ref value, OnAddressChanged);
            }
        }

        public string AddressId { get; private set; }

        public ExplorerTransaction Transaction
        {
            get
            {
                return this.properties.transaction;
            }
            set
            {
                SetProperty(ref this.properties.transaction, ref value, OnTransactionChanged);
            }
        }

        public string TransactionId { get; private set; }

        /// <summary>
        /// A unique value for DB and references match, defined by TransactionId + AddressId
        /// </summary>
        public string TransferId
        {
            get
            {
                return this.properties.transferId;
            }
            set
            {
                SetProperty(ref this.properties.transferId, ref value);
            }
        }

        public long? Amount
        {
            get
            {
                return this.properties.amount;
            }
            set
            {
                SetProperty(ref this.properties.amount, ref value);
            }
        }

        #endregion

        #region constructor

        private Transfer() : base(Transfer.DatabasePropertyNames)
        {
        }

        private Transfer(string transferId) : this()
        {
            this.TransferId = transferId;
        }

        #endregion

        #region public methods

        public static Transfer[] TransfersFromSQLiteTransfers(IEnumerable<SQLiteTransfer> sqliteTransfers)
        {
            SQLiteTransfer[] sqliteTransfersArray = sqliteTransfers.ToArray();
            string[] sqliteTransferIds = sqliteTransfers.Select(transfer => transfer.TransferId).ToArray();
            Transfer[] transfers;

            if (sqliteTransfersArray.Length > 0)
            {
                Dictionary<string, Transfer> transfersByTransferId = new Dictionary<string, Transfer>();
                Transfer[] transfersFromReferences;

                lock (Transfer.references)
                {
                    transfersFromReferences = Transfer.references.GetItems(sqliteTransferIds).ToArray();

                    if (transfersFromReferences.Length > 0)
                    {
                        // keep only the transfers that weren't fetched from the references
                        sqliteTransfersArray = sqliteTransfersArray.Where(transfer => (transfersFromReferences.FirstOrDefault(transferReference => transferReference.TransferId == transfer.TransferId) == null)).ToArray();

                        // add transfers from references
                        foreach (Transfer transfer in transfersFromReferences)
                        {
                            if (transfer.TransferId != null)
                            {
                                transfersByTransferId[transfer.TransferId] = transfer;
                            }
                        }
                    }

                    // if there are still more transfers
                    if (sqliteTransfersArray.Length > 0)
                    {
                        foreach (SQLiteTransfer sqliteTransfer in sqliteTransfersArray)
                        {
                            Transfer transfer = new Transfer(sqliteTransfer.TransferId)
                            {
                                IsMarkedForStorage = false, // never true, whether transfer is in DB is a function of only address and transaction 
                                Amount = sqliteTransfer.Amount,
                                IsTransferIncoming = sqliteTransfer.IsTransferIncoming,
                            };

                            if (transfer.TransferId != null)
                            {
                                Transfer.references.Add(transfer);

                                transfersByTransferId[transfer.TransferId] = transfer;
                            }

                            // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                            transfer.Address = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(sqliteTransfer.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network));
                            transfer.Transaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(uint256.Parse(sqliteTransfer.TransactionId));
                        }
                    }
                }

                transfers = sqliteTransferIds.Select(transferId => transfersByTransferId[transferId]).ToArray();
            }
            else
            {
                transfers = new Transfer[] { };
            }

            return transfers;
        }

        public static Transfer TransferFromSQLiteTransfer(SQLiteTransfer sqliteTransfer)
        {
            Transfer transfer = null;

            if (sqliteTransfer != null)
            {
                lock (Transfer.references)
                {
                    Base58Data base58AddressId = Base58Data.GetFromBase58Data(sqliteTransfer.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network);

                    transfer = Transfer.references.GetItem(sqliteTransfer.TransferId);

                    if (transfer == null)
                    {
                        transfer = new Transfer(sqliteTransfer.TransferId)
                        {
                            IsMarkedForStorage = false, // never true for Transfer, whether transfer is in DB is a function of only address and transaction 
                            Amount = sqliteTransfer.Amount,
                            IsTransferIncoming = sqliteTransfer.IsTransferIncoming,
                        };

                        Transfer.references.Add(transfer);

                        // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                        transfer.Address = ExplorerAddress.ExplorerAddressFromAddressId(Base58Data.GetFromBase58Data(sqliteTransfer.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network));
                        transfer.Transaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(uint256.Parse(sqliteTransfer.TransactionId));
                    }
                }
            }

            return transfer;
        }
        public static Transfer TransferFromTransferId(uint256 transactionId, bool isTransferIncoming, Base58Data addressId)
        {
            return TransferFromTransferId(HashTransferId(transactionId, isTransferIncoming, addressId));
        }

        public static Transfer TransferFromTransferId(string transferId)
        {
            Transfer transfer = null;

            if (transferId != null)
            {
                lock (Transfer.references)
                {
                    transfer = Transfer.references.GetItem(transferId);

                    if (transfer == null)
                    {
                        DatabaseRepository databaseRepository = DatabaseRepository.Instance;

                        if (databaseRepository != null)
                        {
                            transfer = databaseRepository.TransferRepository.GetEntity(transferId);
                        }

                        if (transfer == null)
                        {
                            transfer = new Transfer(transferId);
                        }

                        Transfer.references.Add(transfer);
                    }
                }
            }

            return transfer;
        }

        public static string HashTransferId(uint256 transactionId, bool isTransferIncoming, Base58Data addressId)
        {
            return transactionId.ToString() + isTransferIncoming + addressId.ToWif();
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append(this.Amount);

            if (this.IsTransferIncoming.Value)
            {
                stringBuilder.Append(" to ");
            }
            else
            {
                stringBuilder.Append(" from ");
            }

            stringBuilder.Append(this.Address.AddressId.ToWif());

            return stringBuilder.ToString();
        }

        public override bool CanBeRemovedFromDatabase()
        {
            return !this.Transaction.IsMarkedForStorage && !this.Address.IsMarkedForStorage;
        }

        public bool IsAllPropertiesPopulated()
        {
            return this.TransferId != null && this.IsTransferIncoming != null && this.Address != null && this.Transaction != null && this.Amount != null;
        }

        #endregion

        #region non-public methods

        protected override void OnAssociatedObjectIsMarkedForStorageChanged(object databasePropertyChangeObject, EventArgs eventArgs)
        {
            bool isInDatabase = DatabaseRepository.Instance.TransferRepository.Contains(this.TransferId);

            if (isInDatabase && this.CanBeRemovedFromDatabase())
            {
                // update or remove
                DatabaseRepository.Instance.TransferRepository.UpdateInDatabase(this);
            }
            else if (!isInDatabase && !this.CanBeRemovedFromDatabase())
            {
                // adds or updates
                DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(this);
            }
        }

        private void OnAddressChanged(ExplorerAddress oldValue, ExplorerAddress newValue)
        {
            if (oldValue != null)
            {
                //oldValue.MarkedForStorageChanged -= OnAssociatedObjectIsMarkedForStorageChanged;
            }

            if (newValue != null)
            {
                this.AddressId = newValue.AddressId.ToWif();

                //newValue.MarkedForStorageChanged += OnAssociatedObjectIsMarkedForStorageChanged;
            }
            else
            {
                this.AddressId = null;
            }

            lock (this.updateNeededMutex)
            {
                this.IsUpdateNeeded = true;
            }
        }

        private void OnTransactionChanged(ExplorerTransaction oldValue, ExplorerTransaction newValue)
        {
            if (oldValue != null)
            {
                //oldValue.MarkedForStorageChanged -= OnAssociatedObjectIsMarkedForStorageChanged;
            }

            if (newValue != null)
            {
                this.TransactionId = newValue.TransactionId.ToString();

                //newValue.MarkedForStorageChanged += OnAssociatedObjectIsMarkedForStorageChanged;
            }
            else
            {
                this.TransactionId = null;
            }

            lock (this.updateNeededMutex)
            {
                this.IsUpdateNeeded = true;
            }
        }

        #endregion
    }
}
