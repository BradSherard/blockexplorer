﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NBitcoin;
using QBitNinja.Client.Models;
using BlockExplorer.Services;
using BlockExplorer.Data.Core;
using BlockExplorer.Data.SQL;
using BlockExplorer.ViewModels.Core;
using BlockExplorer.ViewModels;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Specialized;
using System.Diagnostics;

namespace BlockExplorer.Data
{
    public class ExplorerAddress : MetadataChangeObject
    {
        #region types

        private struct Properties
        {
            public Base58Data addressId;
            public MetadataLevels metadataLevel;
            public bool isUpdatingMetadata;
            public int? heightScanned;
            public long? totalReceived;
            public long? totalSent;
            public long? balance;
            public DateTime? lastSyncDate;
            public ThreadSafeObservableCollection<Transfer> transfers;
        }

        #endregion

        #region constants

        public static readonly string[] DatabasePropertyNames = new string[]
        {
            nameof(ExplorerAddress.AddressId),
            nameof(ExplorerAddress.Balance),
            nameof(ExplorerAddress.HeightScanned),
            nameof(ExplorerAddress.LastSyncDate),
            nameof(ExplorerAddress.MetadataLevel),
            nameof(ExplorerAddress.TotalReceived),
            nameof(ExplorerAddress.TotalSent),
            nameof(ExplorerAddress.TransferIds),
        };

        #endregion

        #region static data

        protected static References<ExplorerAddress, Base58Data> references = new References<ExplorerAddress, Base58Data>(address => address.AddressId);

        #endregion

        #region data

        private Properties properties = new Properties();
       
        #endregion

        #region properties

        [IsMetadataProperty(MetadataLevels.Lite)]
        public Base58Data AddressId
        {
            get
            {
                return this.properties.addressId;
            }
            set
            {
                SetProperty(ref this.properties.addressId, ref value, OnAddressIdChanged);
            }
        }

        public override MetadataLevels MetadataLevel
        {
            get
            {
                return this.properties.metadataLevel;
            }
            set
            {
                SetMetadata(ref this.properties.metadataLevel, ref value);
            }
        }

        /// <summary>
        /// only change this in base
        /// </summary>
        public override bool IsUpdatingMetadata
        {
            get
            {
                return this.properties.isUpdatingMetadata;
            }
            protected set
            {
                SetProperty(ref this.properties.isUpdatingMetadata, ref value, OnIsUpdatingMetadataChanged);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public int? HeightScanned
        {
            get
            {
                return this.properties.heightScanned;
            }
            set
            {
                SetProperty(ref this.properties.heightScanned, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public long? TotalReceived
        {
            get
            {
                return this.properties.totalReceived;
            }
            set
            {
                SetProperty(ref this.properties.totalReceived, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public long? TotalSent
        {
            get
            {
                return this.properties.totalSent;
            }
            set
            {
                SetProperty(ref this.properties.totalSent, ref value);
            }
        }


        [IsMetadataProperty(MetadataLevels.Lite)]
        public long? Balance
        {
            get
            {
                return this.properties.balance;
            }
            set
            {
                SetProperty(ref this.properties.balance, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Full)]
        public DateTime? LastSyncDate
        {
            get
            {
                return this.properties.lastSyncDate;
            }
            set
            {
                SetProperty(ref this.properties.lastSyncDate, ref value);
            }
        }

        [IsMetadataProperty(MetadataLevels.Lite)]
        public ThreadSafeObservableCollection<Transfer> Transfers
        {
            get
            {
                return this.properties.transfers;
            }
            set
            {
                SetProperty(ref this.properties.transfers, ref value, OnTransfersChanged);
            }
        }

        public MappedDictionary<string, Transfer> TransferIds { get; private set; }

        #endregion

        #region constructors

        private ExplorerAddress()
            : base(ExplorerAddress.DatabasePropertyNames)
        {
            this.MetadataLevel = MetadataLevels.Default;
        }

        private ExplorerAddress(Base58Data addressId)
            : this()
        {
            this.AddressId = addressId;
            this.Transfers = new ThreadSafeObservableCollection<Transfer>();
        }

        #endregion

        #region public methods

        public override bool CanBeRemovedFromDatabase()
        {
            return !this.IsMarkedForStorage && !this.Transfers.Where(transaction => transaction.Transaction.IsMarkedForStorage).Any();
        }

        public override MetadataLevels DetermineMetadataLevelRestoredFromDatabase()
        {
            MetadataLevels metadataLevel = this.MetadataLevel;

            if (metadataLevel == MetadataLevels.Full)
            {
                if (this.Transfers.Any(transfer => !transfer.IsAllPropertiesPopulated()))
                {
                    metadataLevel = MetadataLevels.Lite;
                }
            }

            return metadataLevel;
        }

        #region public static methods

        public static ExplorerAddress[] ExplorerAddressesFromSQLiteExplorerAddresses(IEnumerable<SQLiteExplorerAddress> sqliteExplorerAddresses)
        {
            SQLiteExplorerAddress[] sqliteExplorerAddressesArray = sqliteExplorerAddresses.ToArray();
            Base58Data[] sqliteExplorerAddressIds = sqliteExplorerAddresses.Select(explorerAddress => Base58Data.GetFromBase58Data(explorerAddress.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network)).ToArray();
            ExplorerAddress[] explorerAddresses;

            if (sqliteExplorerAddressesArray.Length > 0)
            {
                Dictionary<Base58Data, ExplorerAddress> explorerAddressesByExplorerAddressId = new Dictionary<Base58Data, ExplorerAddress>();
                ExplorerAddress[] explorerAddressesFromReferences;

                lock (ExplorerAddress.references)
                {
                    explorerAddressesFromReferences = ExplorerAddress.references.GetItems(sqliteExplorerAddressIds).ToArray();

                    if (explorerAddressesFromReferences.Length > 0)
                    {
                        // keep only the addresses that weren't fetched from the references
                        sqliteExplorerAddressesArray = sqliteExplorerAddressesArray.Where(explorerAddress => (explorerAddressesFromReferences.FirstOrDefault(explorerAddressReference => explorerAddressReference.AddressId.ToWif() == explorerAddress.AddressId) == null)).ToArray();

                        // add addresses from references
                        foreach (ExplorerAddress explorerAddress in explorerAddressesFromReferences)
                        {
                            if (explorerAddress.AddressId != null)
                            {
                                explorerAddressesByExplorerAddressId[explorerAddress.AddressId] = explorerAddress;
                            }
                        }
                    }

                    // if there are still more addresses
                    if (sqliteExplorerAddressesArray.Length > 0)
                    {
                        foreach (SQLiteExplorerAddress sqliteExplorerAddress in sqliteExplorerAddressesArray)
                        {
                            ExplorerAddress explorerAddress = new ExplorerAddress(Base58Data.GetFromBase58Data(sqliteExplorerAddress.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network))
                            {
                                Balance = sqliteExplorerAddress.Balance,
                                HeightScanned = sqliteExplorerAddress.HeightScanned,
                                IsMarkedForStorage = sqliteExplorerAddress.IsMarkedForStorage,
                                LastSyncDate = sqliteExplorerAddress.LastSyncDate,
                                MetadataLevel = sqliteExplorerAddress.MetadataLevel,
                                TotalReceived = sqliteExplorerAddress.TotalReceived,
                                TotalSent = sqliteExplorerAddress.TotalSent,
                            };
                            
                            if (explorerAddress.AddressId != null)
                            {
                                ExplorerAddress.references.Add(explorerAddress);

                                explorerAddressesByExplorerAddressId[explorerAddress.AddressId] = explorerAddress;
                            }

                            // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                            explorerAddress.Transfers = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerAddress.TransferIds));

                            explorerAddress.MetadataLevel = explorerAddress.DetermineMetadataLevelRestoredFromDatabase();
                        }
                    }
                }

                explorerAddresses = sqliteExplorerAddressIds.Select(addressId => explorerAddressesByExplorerAddressId[addressId]).ToArray();
            }
            else
            {
                explorerAddresses = new ExplorerAddress[] { };
            }

            return explorerAddresses;
        }

        public static ExplorerAddress ExplorerAddressFromSQLiteExplorerAddress(SQLiteExplorerAddress sqliteExplorerAddress)
        {
            ExplorerAddress explorerAddress = null;

            if (sqliteExplorerAddress != null)
            {
                lock (ExplorerAddress.references)
                {
                    Base58Data base58AddressId = Base58Data.GetFromBase58Data(sqliteExplorerAddress.AddressId, AppServices.Instance.BitcoinExplorer.ExplorerClient.Network);

                    explorerAddress = ExplorerAddress.references.GetItem(base58AddressId);

                    if (explorerAddress == null)
                    {
                        explorerAddress = new ExplorerAddress(base58AddressId)
                        {
                            Balance = sqliteExplorerAddress.Balance,
                            HeightScanned = sqliteExplorerAddress.HeightScanned,
                            IsMarkedForStorage = sqliteExplorerAddress.IsMarkedForStorage,
                            LastSyncDate = sqliteExplorerAddress.LastSyncDate,
                            MetadataLevel = sqliteExplorerAddress.MetadataLevel,
                            TotalReceived = sqliteExplorerAddress.TotalReceived,
                            TotalSent = sqliteExplorerAddress.TotalSent,
                        };

                        ExplorerAddress.references.Add(explorerAddress);

                        // create and add object to reference before creating its references so when the refs are created they can use the new reference instead of trying to also create it in an infite loop
                        explorerAddress.Transfers = new ThreadSafeObservableCollection<Transfer>(DeserializeTransferIds(sqliteExplorerAddress.TransferIds));

                        explorerAddress.MetadataLevel = explorerAddress.DetermineMetadataLevelRestoredFromDatabase();
                    }
                }
            }

            return explorerAddress;
        }

        public static ExplorerAddress ExplorerAddressFromAddressId(Base58Data addressId)
        {
            ExplorerAddress address = null;

            if (addressId != null)
            {
                lock (ExplorerAddress.references)
                {
                    address = ExplorerAddress.references.GetItem(addressId);

                    if (address == null)
                    {
                        DatabaseRepository databaseRepository = DatabaseRepository.Instance;

                        if (databaseRepository != null)
                        {
                            address = databaseRepository.ExplorerAddressRepository.GetEntity(addressId.ToWif());
                        }

                        if (address == null)
                        {
                            address = new ExplorerAddress(addressId);
                        }

                        ExplorerAddress.references.Add(address);
                    }
                }
            }

            return address;
        }

        #endregion

        #endregion

        #region non-public methods

        protected override async Task<MetadataResponse> GetLiteMetadataAsync(CancellationToken cancellationToken)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            this.Balance = 0;
            this.HeightScanned = -1;
            this.TotalSent = 0;
            this.TotalReceived = 0;
            
            return response;
        }

        protected override async Task<MetadataResponse> GetFullMetadataAsync(CancellationToken cancellationToken, object rawResponseData)
        {
            MetadataResponse response = new MetadataResponse()
            {
                ErrorType = MetadataPageViewModelBase.DataErrorTypes.None,
                RawResponseObject = null,
            };

            Network network = AppServices.Instance.BitcoinExplorer.ExplorerClient.Network;
            GetBlockResponse blockResponse = null;

            do
            {
                try
                {
                    blockResponse = await AppServices.Instance.BitcoinExplorer.ExplorerClient.GetBlock(new BlockFeature(this.HeightScanned.Value + 1));
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"{exception.GetType().Name}: {exception.Message}.");

                    response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.FullMetadataResponseFailed;
                }

                if (blockResponse != null)
                {
                    foreach (Transaction transaction in blockResponse.Block.Transactions)
                    {
                        long balanceChange = 0;

                        if (!transaction.IsCoinBase)
                        {
                            foreach (TxIn input in transaction.Inputs)
                            {
                                string transferId = Transfer.HashTransferId(input.PrevOut.Hash, true, this.AddressId);

                                // if the output used for this transaction came from this address
                                if (this.TransferIds.ContainsKey(transferId))
                                {
                                    balanceChange -= this.TransferIds[transferId].Amount.Value;

                                    ExplorerTransaction explorerTransaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(transaction.GetHash());

                                    Transfer transfer = Transfer.TransferFromTransferId(transaction.GetHash(), false, this.AddressId);

                                    if (!transfer.IsAllPropertiesPopulated())
                                    {
                                        transfer.Address = this;
                                        transfer.Amount = this.TransferIds[transferId].Amount.Value;
                                        transfer.IsTransferIncoming = false;
                                        transfer.Transaction = explorerTransaction;
                                    }

                                    // manually add to DB because transfer is not metadataChangeObject which adds to DB on changes
                                    if (!transfer.CanBeRemovedFromDatabase())
                                    {
                                        DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                                    }

                                    if (!explorerTransaction.SpentIds.ContainsKey(transfer.TransferId))
                                    {
                                        explorerTransaction.Spent.Add(transfer);
                                    }

                                    if (!this.TransferIds.ContainsKey(transfer.TransferId))
                                    {
                                        this.Transfers.Add(transfer);
                                    }
                                }
                            }
                        }

                        foreach (TxOut output in transaction.Outputs)
                        {
                            Base58Data address = null;
                            PubKey[] pubKeys = output.ScriptPubKey.GetDestinationPublicKeys();

                            // brads todo: figure out what the meaning of multiple pub keys can be, as well as order(assuming order in the script stack can matter)
                            if (pubKeys.Length != 1)
                            {
                                if (pubKeys.Length == 0)
                                {
                                    address = Base58Data.GetFromBase58Data(output.ScriptPubKey?.GetDestination()?.GetAddress(network)?.ToWif(), network);
                                }
                                else
                                {

                                }
                            }
                            else
                            {
                                address = Base58Data.GetFromBase58Data(pubKeys[0].GetAddress(network).ToWif(), network);
                            }

                            if (address == this.AddressId)
                            {
                                balanceChange += output.Value.Satoshi;

                                ExplorerTransaction explorerTransaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(transaction.GetHash());

                                Transfer transfer = Transfer.TransferFromTransferId(transaction.GetHash(), true, this.AddressId);

                                if (!transfer.IsAllPropertiesPopulated())
                                {
                                    transfer.Address = this;
                                    transfer.Amount = output.Value.Satoshi;
                                    transfer.IsTransferIncoming = true;
                                    transfer.Transaction = ExplorerTransaction.ExplorerTransactionFromTransactionId(transaction.GetHash());
                                }

                                // manually add to DB because transfer is not metadataChangeObject which adds to DB on changes
                                if (!transfer.CanBeRemovedFromDatabase())
                                {
                                    DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                                }

                                if (!explorerTransaction.ReceivedIds.ContainsKey(transfer.TransferId))
                                {
                                    explorerTransaction.Received.Add(transfer);
                                }

                                if (!this.TransferIds.ContainsKey(transfer.TransferId))
                                {
                                    this.Transfers.Add(transfer);
                                }
                            }
                        }

                        if (balanceChange != 0)
                        {
                            if (balanceChange > 0)
                            {
                                this.TotalReceived += balanceChange;
                            }
                            else
                            {
                                this.TotalSent -= balanceChange; // note: subtracting a negative
                            }

                            this.Balance = this.TotalReceived - this.TotalSent;
                        }
                    }

                    this.HeightScanned += 1;
                }

                if (cancellationToken.IsCancellationRequested)
                {
                    response.ErrorType = MetadataPageViewModelBase.DataErrorTypes.FullMetadataResponseFailed;
                }
            }
            while (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None && blockResponse != null);

            if (response.ErrorType == MetadataPageViewModelBase.DataErrorTypes.None)
            {
                this.LastSyncDate = DateTime.Now;
            }
            else
            {
                this.LastSyncDate = null;
            }

            return response;
        }

        protected override void OnAssociatedObjectIsMarkedForStorageChanged(object databasePropertyChangeObject, EventArgs eventArgs)
        {
            bool isInDatabase = DatabaseRepository.Instance.ExplorerAddressRepository.Contains(this.AddressId.ToWif());

            if (isInDatabase && this.CanBeRemovedFromDatabase())
            {
                // update or remove
                DatabaseRepository.Instance.ExplorerAddressRepository.UpdateInDatabase(this);
            }
            else if (!isInDatabase && !this.CanBeRemovedFromDatabase())
            {   
                // adds or updates
                DatabaseRepository.Instance.ExplorerAddressRepository.EnsureIsInDatabase(this);
            }
        }

        private void OnTransfersChanged(ThreadSafeObservableCollection<Transfer> oldValue, ThreadSafeObservableCollection<Transfer> newValue)
        {
            if (oldValue != null)
            {
                oldValue.CollectionChanged -= OnCollectionChanged_Transfers;
            }

            if (newValue == null)
            {
                if (oldValue != null)
                {
                    this.TransferIds?.Clear();

                    lock (this.updateNeededMutex)
                    {
                        this.IsUpdateNeeded = true;
                    }
                }
            }
            else
            {
                newValue.CollectionChanged += OnCollectionChanged_Transfers;

                this.TransferIds = new MappedDictionary<string, Transfer>(newValue, (transfer => transfer.TransferId),
                    delegate
                    {
                        lock (this.updateNeededMutex)
                        {
                            this.IsUpdateNeeded = true;
                        }
                    }
                );
            }
        }

        private void OnCollectionChanged_Transfers(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                {
                    foreach (Transfer item in e.NewItems.OfType<Transfer>())
                    {
                        //item.Transaction.MarkedForStorageChanged += OnAssociatedObjectIsMarkedForStorageChanged;
                    }
                    
                    break;
                }
                case NotifyCollectionChangedAction.Move:
                {
                    break;
                }
                case NotifyCollectionChangedAction.Remove:
                {
                    foreach (Transfer item in e.OldItems.OfType<Transfer>())
                    {
                        //item.Transaction.MarkedForStorageChanged -= OnAssociatedObjectIsMarkedForStorageChanged;
                    }

                    break;
                }
                case NotifyCollectionChangedAction.Replace:
                {
                    foreach (Transfer item in e.OldItems.OfType<Transfer>())
                    {
                        //item.Transaction.MarkedForStorageChanged -= OnAssociatedObjectIsMarkedForStorageChanged;
                    }

                    goto case NotifyCollectionChangedAction.Add;
                }
                case NotifyCollectionChangedAction.Reset:
                {
                    foreach (Transfer item in this.Transfers)
                    {
                        //item.Transaction.MarkedForStorageChanged += OnAssociatedObjectIsMarkedForStorageChanged;
                    }

                    break;
                }
            }
        }

        private void OnAddressIdChanged(Base58Data oldValue, Base58Data newValue)
        {
        }

        private void OnIsUpdatingMetadataChanged(bool oldValue, bool newValue)
        {
            // if was updating and now isn't
            if (!newValue && oldValue)
            {
                bool anyAddedOrRemoved = false;
                bool isInDatabase = DatabaseRepository.Instance.ExplorerAddressRepository.Contains(this.AddressId.ToWif());

                if (isInDatabase && this.CanBeRemovedFromDatabase())
                {
                    // update or remove
                    anyAddedOrRemoved = DatabaseRepository.Instance.ExplorerAddressRepository.UpdateInDatabase(this);
                }
                else if (!isInDatabase && !this.CanBeRemovedFromDatabase())
                {   // adds or updates
                    anyAddedOrRemoved = DatabaseRepository.Instance.ExplorerAddressRepository.EnsureIsInDatabase(this);
                }

                if (anyAddedOrRemoved)
                {
                    foreach (Transfer transfer in this.Transfers)
                    {
                        if (transfer.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.TransferRepository.UpdateInDatabase(transfer);
                        }
                        else
                        {
                            DatabaseRepository.Instance.TransferRepository.EnsureIsInDatabase(transfer);
                        }

                        if (transfer.Transaction.CanBeRemovedFromDatabase())
                        {
                            DatabaseRepository.Instance.ExplorerTransactionRepository.UpdateInDatabase(transfer.Transaction);
                        }
                        else
                        {
                            DatabaseRepository.Instance.ExplorerTransactionRepository.EnsureIsInDatabase(transfer.Transaction);
                        }
                    }
                }
            }
        }

        private static List<Transfer> DeserializeTransferIds(byte[] blob)
        {
            List<Transfer> rebuiltData = new List<Transfer>();
            List<String> deserializedData;
            
            using (MemoryStream stream = new MemoryStream(blob))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                deserializedData = (List<String>)formatter.Deserialize(stream);
            }

            foreach (string transferId in deserializedData)
            {
               rebuiltData.Add(Transfer.TransferFromTransferId(transferId));    
            }

            return rebuiltData;
        }

        #endregion
    }
}
